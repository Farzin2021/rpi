from bluepy.btle import ADDR_TYPE_PUBLIC, ADDR_TYPE_RANDOM, ScanEntry, BluepyHelper, Scanner, Debugging, BTLEException, DefaultDelegate, UUID, Characteristic, BTLEDisconnectError, Service, BTLEGattError, Descriptor, BTLEManagementError
import pytz
from datetime import datetime, timedelta
import struct
import zmq
from fabric.api import local, settings
import sys, traceback
try:
    from zmq.core.error import ZMQError
except ImportError:
    from zmq import ZMQError
import json
import requests
import binascii
import xxtea


class Peripheral(BluepyHelper):

    # das ist nur hier, damit wir den timeout setzen koennen. sonst muessten wir bluepy.btle umschreiben

    def __init__(self, deviceAddr=None, addrType=ADDR_TYPE_PUBLIC, iface=None):
        BluepyHelper.__init__(self)
        self._serviceMap = None # Indexed by UUID
        (self.deviceAddr, self.addrType, self.iface) = (None, None, None)

        if isinstance(deviceAddr, ScanEntry):
            self._connect(deviceAddr.addr, deviceAddr.addrType, deviceAddr.iface)
        elif deviceAddr is not None:
            self._connect(deviceAddr, addrType, iface)

    def setDelegate(self, delegate_): # same as withDelegate(), deprecated
        return self.withDelegate(delegate_)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.disconnect()

    def _getResp(self, wantType, timeout=10):
        if isinstance(wantType, list) is not True:
            wantType = [wantType]

        while True:
            resp = self._waitResp(wantType + ['ntfy', 'ind'], timeout)
            if resp is None:
                return None

            respType = resp['rsp'][0]
            if respType == 'ntfy' or respType == 'ind':
                hnd = resp['hnd'][0]
                data = resp['d'][0]
                if self.delegate is not None:
                    self.delegate.handleNotification(hnd, data)
            if respType not in wantType:
                continue
            return resp

    def _connect(self, addr, addrType=ADDR_TYPE_PUBLIC, iface=None):
        if len(addr.split(":")) != 6:
            raise ValueError("Expected MAC address, got %s" % repr(addr))
        if addrType not in (ADDR_TYPE_PUBLIC, ADDR_TYPE_RANDOM):
            raise ValueError("Expected address type public or random, got {}".format(addrType))
        self._startHelper(iface)
        self.addr = addr
        self.addrType = addrType
        self.iface = iface
        if iface is not None:
            self._writeCmd("conn %s %s %s\n" % (addr, addrType, "hci"+str(iface)))
        else:
            self._writeCmd("conn %s %s\n" % (addr, addrType))
        rsp = self._getResp('stat')
        if (rsp is None or rsp['state'] is None):
            self.disconnect()
            raise BTLEDisconnectError("Failed to connect to peripheral rsp or rsp-state none %s, addr type: %s" % (addr, addrType), rsp)
        while rsp['state'][0] == 'tryconn':
            rsp = self._getResp('stat')
            if (rsp is None or rsp['state'] is None):
                self.disconnect()
                raise BTLEDisconnectError("Failed to connect to peripheral 1 rsp or rsp-state none %s, addr type: %s" % (addr, addrType), rsp)

        if rsp['state'][0] != 'conn':
            self._stopHelper()
            raise BTLEDisconnectError("Failed to connect to peripheral %s, addr type: %s" % (addr, addrType), rsp)

    def connect(self, addr, addrType=ADDR_TYPE_PUBLIC, iface=None):
        if isinstance(addr, ScanEntry):
            self._connect(addr.addr, addr.addrType, addr.iface)
        elif addr is not None:
            self._connect(addr, addrType, iface)

    def disconnect(self):
        if self._helper is None:
            return
        # Unregister the delegate first
        self.setDelegate(None)

        self._writeCmd("disc\n")
        self._getResp('stat')
        self._stopHelper()

    def discoverServices(self):
        self._writeCmd("svcs\n")
        rsp = self._getResp('find')
        starts = rsp['hstart']
        ends   = rsp['hend']
        uuids  = rsp['uuid']
        nSvcs = len(uuids)
        assert(len(starts)==nSvcs and len(ends)==nSvcs)
        self._serviceMap = {}
        for i in range(nSvcs):
            self._serviceMap[UUID(uuids[i])] = Service(self, uuids[i], starts[i], ends[i])
        return self._serviceMap

    def getState(self):
        status = self.status()
        return status['state'][0]

    @property
    def services(self):
        if self._serviceMap is None:
            self._serviceMap = self.discoverServices()
        return self._serviceMap.values()

    def getServices(self):
        return self.services

    def getServiceByUUID(self, uuidVal):
        uuid = UUID(uuidVal)
        if self._serviceMap is not None and uuid in self._serviceMap:
            return self._serviceMap[uuid]
        self._writeCmd("svcs %s\n" % uuid)
        rsp = self._getResp('find')
        if 'hstart' not in rsp:
            raise BTLEGattError("Service %s not found" % (uuid.getCommonName()), rsp)
        svc = Service(self, uuid, rsp['hstart'][0], rsp['hend'][0])

        if self._serviceMap is None:
            self._serviceMap = {}
        self._serviceMap[uuid] = svc
        return svc

    def _getIncludedServices(self, startHnd=1, endHnd=0xFFFF):
        # TODO: No working example of this yet
        self._writeCmd("incl %X %X\n" % (startHnd, endHnd))
        return self._getResp('find')

    def getCharacteristics(self, startHnd=1, endHnd=0xFFFF, uuid=None):
        cmd = 'char %X %X' % (startHnd, endHnd)
        if uuid:
            cmd += ' %s' % UUID(uuid)
        self._writeCmd(cmd + "\n")
        rsp = self._getResp('find')
        nChars = len(rsp['hnd'])
        return [Characteristic(self, rsp['uuid'][i], rsp['hnd'][i],
                               rsp['props'][i], rsp['vhnd'][i])
                for i in range(nChars)]

    def getDescriptors(self, startHnd=1, endHnd=0xFFFF):
        self._writeCmd("desc %X %X\n" % (startHnd, endHnd) )
        # Historical note:
        # Certain Bluetooth LE devices are not capable of sending back all
        # descriptors in one packet due to the limited size of MTU. So the
        # guest needs to check the response and make retries until all handles
        # are returned.
        # In bluez 5.25 and later, gatt_discover_desc() in attrib/gatt.c does the retry
        # so bluetooth_helper always returns a full list.
        # This was broken in earlier versions.
        resp = self._getResp('desc')
        ndesc = len(resp['hnd'])
        return [Descriptor(self, resp['uuid'][i], resp['hnd'][i]) for i in range(ndesc)]

    def readCharacteristic(self, handle):
        self._writeCmd("rd %X\n" % handle)
        resp = self._getResp('rd')
        return resp['d'][0]

    def _readCharacteristicByUUID(self, uuid, startHnd, endHnd):
        # Not used at present
        self._writeCmd("rdu %s %X %X\n" % (UUID(uuid), startHnd, endHnd))
        return self._getResp('rd')

    def writeCharacteristic(self, handle, val, withResponse=False):
        # Without response, a value too long for one packet will be truncated,
        # but with response, it will be sent as a queued write
        cmd = "wrr" if withResponse else "wr"
        self._writeCmd("%s %X %s\n" % (cmd, handle, binascii.b2a_hex(val).decode('utf-8')))
        return self._getResp('wr')

    def setSecurityLevel(self, level):
        self._writeCmd("secu %s\n" % level)
        return self._getResp('stat')

    def unpair(self):
        self._mgmtCmd("unpair")

    def pair(self):
        self._mgmtCmd("pair")

    def setMTU(self, mtu):
        self._writeCmd("mtu %x\n" % mtu)
        return self._getResp('stat')

    def waitForNotifications(self, timeout):
         resp = self._getResp(['ntfy','ind'], timeout)
         return (resp != None)

    def _setRemoteOOB(self, address, address_type, oob_data, iface=None):
        if self._helper is None:
            self._startHelper(iface)
        self.addr = address
        self.addrType = address_type
        self.iface = iface
        cmd = "remote_oob " + address + " " + address_type
        if oob_data['C_192'] is not None and oob_data['R_192'] is not None:
            cmd += " C_192 " + oob_data['C_192'] + " R_192 " + oob_data['R_192']
        if oob_data['C_256'] is not None and oob_data['R_256'] is not None:
            cmd += " C_256 " + oob_data['C_256'] + " R_256 " + oob_data['R_256']
        if iface is not None:
            cmd += " hci"+str(iface)
        self._writeCmd(cmd)

    def setRemoteOOB(self, address, address_type, oob_data, iface=None):
        if len(address.split(":")) != 6:
            raise ValueError("Expected MAC address, got %s" % repr(address))
        if address_type not in (ADDR_TYPE_PUBLIC, ADDR_TYPE_RANDOM):
            raise ValueError("Expected address type public or random, got {}".format(address_type))
        if isinstance(address, ScanEntry):
            return self._setOOB(address.addr, address.addrType, oob_data, address.iface)
        elif address is not None:
            return self._setRemoteOOB(address, address_type, oob_data, iface)

    def getLocalOOB(self, iface=None):
        if self._helper is None:
            self._startHelper(iface)
        self.iface = iface
        self._writeCmd("local_oob\n")
        if iface is not None:
            cmd = " hci"+str(iface)
        resp = self._getResp('oob')
        if resp is not None:
            data = resp.get('d', [''])[0]
            if data is None:
                raise BTLEManagementError(
                                "Failed to get local OOB data.")
            if struct.unpack_from('<B',data,0)[0] != 8 or struct.unpack_from('<B',data,1)[0] != 0x1b:
                raise BTLEManagementError(
                                "Malformed local OOB data (address).")
            address = data[2:8]
            address_type = data[8:9]
            if struct.unpack_from('<B',data,9)[0] != 2 or struct.unpack_from('<B',data,10)[0] != 0x1c:
                raise BTLEManagementError(
                                "Malformed local OOB data (role).")
            role = data[11:12]
            if struct.unpack_from('<B',data,12)[0] != 17 or struct.unpack_from('<B',data,13)[0] != 0x22:
                raise BTLEManagementError(
                                "Malformed local OOB data (confirm).")
            confirm = data[14:30]
            if struct.unpack_from('<B',data,30)[0] != 17 or struct.unpack_from('<B',data,31)[0] != 0x23:
                raise BTLEManagementError(
                                "Malformed local OOB data (random).")
            random = data[32:48]
            if struct.unpack_from('<B',data,48)[0] != 2 or struct.unpack_from('<B',data,49)[0] != 0x1:
                raise BTLEManagementError(
                                "Malformed local OOB data (flags).")
            flags = data[50:51]
            return {'Address' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in address]),
                    'Type' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in address_type]),
                    'Role' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in role]),
                    'C_256' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in confirm]),
                    'R_256' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in random]),
                    'Flags' : ''.join(["%02X" % struct.unpack('<B',c)[0] for c in flags]),
                    }

    def __del__(self):
        self.disconnect()


class BTLEDevice(Peripheral):
    SUPPORTED_DEVICES = (
        ("EUROtronic GmbH", "Comet Blue"),
        ("Eurotronic GmbH", "Comet Blue"),
        ("Controme GmbH", "Controme RBDG10")
    )

    def __init__(self, *args, **kwargs):
        Peripheral.__init__(self, *args, **kwargs)


class ContromeRBDG10(BTLEDevice):

    def __init__(self, dev, mac):
        self.data = {
            "manufacturer": "Controme GmbH",
            "device": "Controme RBDG10",
            "datetime": (0, 0, 0, 0, 0),
        }        
        self.m = [int(_m, 16) for _m in mac]
        self.d = [int(_d, 16) for _d in dev.split(':')]
        self.deviceAddr1 = dev
        self.t_soll = 0
        self.set_seclevel = True
        BTLEDevice.__init__(self, dev)

    def _connectContromeWT(self):
        for _ in range(5):
            try:
                self.connect(self.deviceAddr1, ADDR_TYPE_PUBLIC, 0)
                if self.set_seclevel:
                    try:
                        self.setSecurityLevel("low")
                    except BTLEException as e:
                        if str(e).startswith("Error from bluepy-helper") and "error: setsockopt(BT_SECURITY)" in str(e):
                            self.set_seclevel = False
                        else:
                            raise e
                return True
            except BTLEException as e:
                # print "_connect BTLEException %s" % str(e)
                # print traceback.format_exc()
                # return False
                pass
            except TypeError as e:
                print traceback.format_exc()
        else:
            print "_connect failed after five retries"
            print traceback.format_exc()
            # wir haben irgendein anderes problem, read_battery() crashed und update_data() setzt last_seen nicht
            return False

    
    def read_temperatures(self):
        temp = self.getCharacteristics(uuid="2A6E")[0].read()
        b0, b1 = struct.unpack('BB', temp)
        self.data['ist'] = (b1*256 + b0) / 100.0
        self.getCharacteristics(uuid="f4701152-6d28-48ca-96c5-02e5baa91dcf")[0].write('0', withResponse=True)
        temp = self.getCharacteristics(uuid="f4701152-6d28-48ca-96c5-02e5baa91dcf")[0].read()
        self.data['soll'] = temp

    def write_temperatures(self, soll):
        self.t_soll = soll
        ret = self.getCharacteristics(uuid="f4701152-6d28-48ca-96c5-02e5baa91dcf")[0].write(("10:%1.2f" % soll), withResponse=True)
        print "%s returned %s. wrote soll = %s." % (self.deviceAddr1, ret, soll)

    def update_data(self):
        if not self._connectContromeWT():
            return False
        self.read_temperatures()
        self.disconnect()
        return True

    def set_data(self, data):
        if (int(data['soll'] * 100) !=int( float(self.data.get('soll', 0.0)) * 100)) and self._connectContromeWT():
            self.write_temperatures(data['soll'])
           
            self.disconnect()

    def get_data(self):
        return self.data


class CometBlue(BTLEDevice):
    # new genius led
    # handle 3 ist geraetename
    # handle 18 ist hersteller
    # handle 27 ist datetime.
    # handle 61 ist temperature. 
    # handle 63 ist battery
    # handle 72 ist pin

    # old comet
    # handle 3 ist geraetename
    # handle 26 ist hersteller
    # handle 29 ist datetime. 1 byte jeweils fuer M H d m Y (Y ist 2000 + Y)
    # handle 63 ist temperature. 1 byte jeweils fuer IST, SOLL, THIGH, TLOW, OFFSET, WINDOWDET, WINDOWOPEN
    # handle 65 ist battery
    # handle 71 ist pin

    def __init__(self, dev, mac, isNewLed, manufacturer):
        self.data = {
            "manufacturer": manufacturer,
            "device": "Comet Blue",
            "datetime": (0, 0, 0, 0, 0),
            "isNewLed": isNewLed
        }        
        self.m = [int(_m, 16) for _m in mac]
        self.d = [int(_d, 16) for _d in dev.split(':')]
        self.deviceAddr1 = dev
        self.h_Name = 3
        if isNewLed:
            self.h_Manufacturer = 18
            self.h_Datetime = 27
            self.h_Temperature = 61
            self.h_battery = 63
            self.h_pin = 72
        else:
            self.h_Manufacturer = 26
            self.h_Datetime = 29
            self.h_Temperature = 63
            self.h_battery = 65
            self.h_pin = 71
        BTLEDevice.__init__(self, dev)

    def _connectCometBlue(self):
        pin_error = False
        for _ in range(5):
            try:
                self.connect(self.deviceAddr1, ADDR_TYPE_PUBLIC, 0)
                if not self.data['isNewLed']:
                    self.setSecurityLevel("low")
                if not self.send_pin(pin=struct.pack('i', sum([_m * _d for _m, _d in zip(self.m, self.d)]))):
                    pin_error = True
                return True
            except BTLEException as e:
                # print "_connect BTLEException %s" % str(e)
                # print traceback.format_exc()
                # return False
                pass
            except TypeError as e:
                print traceback.format_exc()
        else:
            print "_connect failed after five retries"
            print traceback.format_exc()
            if pin_error:
                # geraet vorhanden, aber falsche pin. last_seen wird gesetzt.
                return False
            else:
                # wir haben irgendein anderes problem, read_battery() crashed und update_data() setzt last_seen nicht
                return True

    def send_pin(self, pin):
        try:
            _ = self.writeCharacteristic(self.h_pin, '\x00' * 4, withResponse=True)
            return True
        except BTLEException as e:
            print "error writing pin for %s" % self.deviceAddr1
            print traceback.format_exc()
            return False

    def read_battery(self):
        # 255 ist None? 
        battery = self.readCharacteristic(self.h_battery)
        if ord(battery) == 255:
            self.data['battery'] = None
        else:
            if ord(battery) > 100:
                self.data['battery'] = 100
            else:
                self.data['battery'] = ord(battery)

    def read_datetime(self):
        dt = self.readCharacteristic(self.h_Datetime)
        mnt, hr, dy, mnth, yr = struct.unpack('bbbbb', dt)
        self.data['datetime'] = (mnt, hr, dy, mnth, yr)

    def write_datetime(self):
        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        if self.data['datetime'] != (now.minute, now.hour, now.day, now.month, now.year - 2000):
            self.writeCharacteristic(self.h_Datetime, struct.pack('bbbbb', now.minute, now.hour, now.day, now.month, now.year - 2000))

    def read_temperatures(self):
        temp = self.readCharacteristic(self.h_Temperature)
        cnt, mantemp, targetlow, targethigh, offsettemp, windowopendet, windowopenmin = struct.unpack('bbbbbbb', temp)
        self.data['ist'] = cnt / 2.0
        self.data['soll'] = mantemp / 2.0
        self.data['offset'] = offsettemp / 2.0

    def write_temperatures(self, soll, offset=0.0):
        # das muss die Temperatur in ein int *2 uebersetzen
        ret = self.writeCharacteristic(self.h_Temperature, struct.pack('bbbbbbb', -128, int(soll * 2), int(soll * 2), int(soll * 2), int(offset * 2), 12, 10), withResponse=True)
        print "%s returned %s. wrote soll = %s." % (self.deviceAddr, ret, soll)

    def set_mode(self):
        r = self.getCharacteristics(uuid="47e9ee2a-47e9-11e4-8939-164230d1df67")[0].read()
        mode, zwei, drei = struct.unpack('bbb', r)
        if not mode:
            self.getCharacteristics(uuid="47e9ee2a-47e9-11e4-8939-164230d1df67")[0].write('\x01' + r[1:])

    def update_data(self):

        if not self._connectCometBlue():
            return False

        self.set_mode()
        self.read_battery()
        self.read_datetime()
        self.read_temperatures()

        self.disconnect()

        return True

    def set_data(self, data):
        if (int(data['soll'] * 2) != int(self.data.get('soll', 0.0) * 2) or int(data['offset'] * 2) != int(self.data.get('offset', 0.0) * 2)) and self._connectCometBlue():

            self.write_datetime()
            self.write_temperatures(data['soll'], data['offset'])

            self.disconnect()

    def get_data(self):
        return self.data


class eTRVDevice(BTLEDevice):

    def __init__(self, address, secret=None, pin=None):
        self.address = address
        self.deviceAddr1 = address
        self.secret = secret
        self.pin = b'0000' if pin is None else pin
        self.data = {
            "manufacturer": 'Danfoss',
            "device": "eTRV"
        }
        BTLEDevice.__init__(self, deviceAddr=address)

    @staticmethod
    def etrv_reverse_chunks(data):
        result = bytearray()
        for i in range(0, len(data), 4):
            result += data[i:i+4][::-1]
        return result

    def _connecteTRV(self):
        for _ in range(5):
            try:
                self.connect(self.deviceAddr1, ADDR_TYPE_PUBLIC, 0)
                if self.send_pin():
                    return True
            except BTLEException as e:
                pass
            except TypeError:
                pass
        else:
            print "_connecteTRV failed after five attempts"
            return False

    def send_pin(self):
        ret = self.writeCharacteristic(0x24, b'0000', True)  # pin schicken
        try:
            if ret['rsp'] == ['wr']:
                return True
            else:
                return False
        except Exception:
            print traceback.format_exc()
            return False

    def read_temperatures(self):
        byteval = self.readCharacteristic(0x2d)
        data = self.etrv_reverse_chunks(byteval)
        data = xxtea.decrypt(bytes(data), self.secret, padding=False)
        data = self.etrv_reverse_chunks(data)
        try:
            soll, ist = struct.unpack('<BB6x', data)
            self.data['ist'] = ist / 2.0
            self.data['soll'] = soll / 2.0
            return True
        except Exception:
            print traceback.format_exc()
            return False

    def write_temperatures(self, soll, offset=0.0):
        settemp = min(56, max(10, int((soll + offset) * 2)))
        s = struct.Struct("<B7x")
        data = s.pack(settemp)
        data = self.etrv_reverse_chunks(data)
        data = xxtea.encrypt(bytes(data), self.secret, padding=False)
        data = self.etrv_reverse_chunks(data)
        ret = self.writeCharacteristic(0x2d, data, True)
        return True

    def set_data(self, data):
        if int(data['soll'] * 2) != int(self.data.get('soll', 0.0) * 2) and self._connecteTRV():

            self.write_temperatures(data['soll'])
            self.disconnect()

    def update_data(self):

        if not self._connecteTRV():
            return False

        self.read_temperatures()

        self.disconnect()

        return True


class BTLEHandler:
    def __init__(self, *args, **kwargs):
        self.devices = {}
        self.last_seen = {}
        self.update_data = {}
        self.intervall = 300
        self.discovery = False
        with open('/sys/class/net/eth0/address') as f:
            mac = f.read()
            macaddr = mac.strip().lower().split(':')
        self.mac = macaddr

        self.zmqcontext = zmq.Context()

        self.outsocket = self.zmqcontext.socket(zmq.DEALER)
        self.outsocket.connect("tcp://127.0.0.1:%s" % 5557)

        self.insocket = self.zmqcontext.socket(zmq.SUB)
        self.insocket.connect("tcp://127.0.0.1:%s" % 5558)
        self.insocket.setsockopt(zmq.SUBSCRIBE, 'btle')

        self.poller = zmq.Poller()
        self.poller.register(self.insocket, zmq.POLLIN)

    def get_device_data(self):
        if self.discovery:
            try:
                scanner = Scanner()
                for dev in scanner.scan(7.0):

                    if dev.addr in self.devices:
                        continue

                    if dev.scanData.get(9) == "Comet Blue":
                        self._instantiate_device(dev.addr, 'Genius')

                    elif dev.scanData.get(9) == "Controme RBDG10": 
                        self._instantiate_device(dev.addr, 'Controme RBDG10')

                    elif dev.scanData.get(9, '').endswith(';eTRV'):
                        self._instantiate_device(dev.addr, 'eTRV')
            except BTLEException as e:
                local("sudo hciconfig hci0 down")
                local("sudo hciconfig hci0 up")

        for addr, dev in self.devices.items():
            try:
                if dev.update_data():
                    self.last_seen[addr] = 'n'  # now
            except BTLEException as e:
                print "device %s disconnected" % addr
                dev.disconnect()
            except IOError as e:
                print "get_device_data ioerror %s for device %s" % (str(e), addr)
                del self.devices[addr]
            except TypeError:
                print "get_device_data typeerror"

    def send_device_data(self):
        data = {}
        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        for addr, dev in self.devices.items():
            data[addr] = dev.get_data()
            if self.last_seen[addr] == 'n':
                self.last_seen[addr] = now
            data[addr]['last_seen'] = self.last_seen[addr].strftime("%Y-%m-%d %H:%M")
        data['last_seen'] = now.strftime("%Y-%m-%d %H:%M")
        print "%s - sending: %s" % (datetime.now(berlin), data)

        self.outsocket.send_multipart(['btle', '', json.dumps({'btle': data})])

    def set_device_data(self): 
        for addr in self.update_data:
            if addr not in self.devices:
                print "trying to instantiate ", addr
                self._instantiate_device(addr, self.update_data[addr]['typ'], secret=self.update_data[addr].get('secret'))

            try:
                self.devices[addr].set_data(self.update_data[addr])
            except BTLEException:
                print "error setting device data"
                print traceback.format_exc()
                self.devices[addr].disconnect()
            except KeyError:
                print "error in set_device_data because of failed instantiation"

    def _instantiate_device(self, addr, typ, secret=None):
        if typ == "Comet Blue" or typ == "Genius":
            try:
                p = Peripheral(addr)
                manufacturer = p.getCharacteristics(uuid="2a29")[0].read()
                if (manufacturer, p.getCharacteristics(uuid="2a00")[0].read()) in BTLEDevice.SUPPORTED_DEVICES:
                    p.disconnect()
                    if typ == "Genius":
                        isNewLed = True
                    else:
                        isNewLed = False
                    cb = CometBlue(addr, self.mac, isNewLed, manufacturer)
                    self.devices[addr] = cb
                    self.last_seen[addr] = 'n'  # now
                    cb.disconnect()
                else:
                    p.disconnect()
            except BTLEException:
                print "error instantiating device as peripheral"
                print traceback.format_exc()
                pass
            except TypeError:
                # vermutlich nicht verbunden, deswegen scheitert .read()
                pass
        elif typ == "Controme RBDG10":
            try:
                p = Peripheral(addr)
                if ("Controme GmbH", p.getCharacteristics(uuid="2a00")[0].read()) in BTLEDevice.SUPPORTED_DEVICES:
                    p.disconnect()
                    cb = ContromeRBDG10(addr, self.mac)
                    self.devices[addr] = cb
                    self.last_seen[addr] = 'n'  # now
                    cb.disconnect()
                else:
                    p.disconnect()
            except BTLEException:
                print "error instantiating device as peripheral"
                print traceback.format_exc()
                pass
            except TypeError:
                # vermutlich nicht verbunden, deswegen scheitert .read()
                pass
        elif typ == "eTRV":
            for _ in range(10):
                try:
                    p = Peripheral(addr)
                    service = p.getServiceByUUID("10020000-2749-0001-0000-00805f9b042f")
                    if secret is None:
                        try:
                            secret = struct.unpack('<16c', service.getCharacteristics(forUUID="1002000B-2749-0001-0000-00805F9B042F")[0].read())
                        except IndexError:
                            print "indexerror"

                    if secret is not None:
                        p.disconnect()
                        etrv = eTRVDevice(address=addr, secret=''.join(list(secret)))
                        self.devices[addr] = etrv
                        etrv.disconnect()
                        return

                    p.disconnect()
                except BTLEException as e:
                    print "instantiate etrv", str(e)
                except TypeError as e:
                    print "instantiate etrv", str(e)

    def run(self):

        local("sudo hciconfig hci0 down")
        local("sudo hciconfig hci0 up")

        timer = datetime.now() - timedelta(seconds=self.intervall)
        rec_data = False  # damit doppelte nachrichten abgearbeitet werden koennen
        berlin = pytz.timezone("Europe/Berlin")
        while True:
            try:
                socks = dict(self.poller.poll(7))
                if self.insocket in socks and socks[self.insocket] == zmq.POLLIN:
                    try:
                        message = self.insocket.recv(zmq.NOBLOCK)
                        message = json.loads(message.split('btle ')[1])
                    except ZMQError, e:
                        self.update_data = {}
                        if e.errno == zmq.EAGAIN:
                            pass
                        else:
                            raise e
                    else:
                        print "%s - received message: %s" % (datetime.now(berlin), str(message))
                        rec_data = True

                    self.update_data = message.get('data', {})
                    if 'intervall' in message:
                        self.intervall = message.get('intervall')
                    if 'discovery' in message:
                        self.discovery = message.get('discovery')
                    self.set_device_data()
                else:
                    rec_data = False

                if datetime.now() - timedelta(seconds=self.intervall) > timer and not rec_data:
                    try:
                        print "INTERVALL: %s" % self.intervall
                        with settings(warn_only=True):
                            r = local("ps -C bluepy-helper -o %cpu", capture=True)
                            r = float(r.split('\n')[1])
                            if r > 90:
                                local("sudo killall bluepy-helper")
                    except Exception as e:
                        print "bluepyhelper exc %s" % str(e)
                    self.get_device_data()
                    self.send_device_data()
                    timer = datetime.now()
            
            except IOError as e:
                if str(e) == "[Errno 32] Broken pipe":
                    local("sudo hciconfig hci0 down")
                    local("sudo hciconfig hci0 up")
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    err = "exception %s / %s in run:%s" % (exc_type, exc_obj, exc_tb.tb_lineno)
                    print "%s, restarted hci0. %s" % (str(e), err)
                else:
                    exc_type, exc_obj, exc_tb = sys.exc_info()
                    err = "exception %s / %s in run:%s" % (exc_type, exc_obj, exc_tb.tb_lineno)
                    print "uncaught IOError %s" % err
                print traceback.format_exc()
            except BTLEException as e:
                if str(e) == "Device disconnected":
                    print "btleexception in run, device disconnected"
                print "btleexception bubbled up to the main loop"
                print traceback.format_exc()
            except Exception as e:
                exc_type, exc_obj, exc_tb = sys.exc_info()
                err = "exception %s / %s in run:%s" % (exc_type, exc_obj, exc_tb.tb_lineno)
                print traceback.format_exc()
                requests.post("%s/get/zwave/%s/" % ("http://controme-main.appspot.com", self.mac), data=err, timeout=10)
                print str(traceback.extract_tb(exc_tb))


if __name__ == "__main__":
    handler = BTLEHandler()
    handler.run()
