from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^m_setup/(?P<haus>\d+)/temperaturszenen/(?P<action>[a-z]+)/$', views.get_global_settings_page),
    url(r'^m_setup/(?P<haus>\d+)/temperaturszenen/(?P<action>[a-z]+)/(?P<entityid>\d+)/$', views.get_global_settings_page),
    url(r'^set/temperaturszenen/(?P<hausid>\d+)/(?P<qlink_uuid>[-\w]{36})/(?P<duration>[0-9]+)/$',views.qlink_set_mode),
    url(r'^set/temperaturszenen/(?P<hausid>\d+)/(?P<qlink_uuid>[-\w]{36})/$',views.qlink_set_mode),
]
