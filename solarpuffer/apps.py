from django.apps import AppConfig


class SolarpufferConfig(AppConfig):
    name = 'solarpuffer'
    verbose_name = 'Solarpuffer'

    def ready(self):
        from . import signals
