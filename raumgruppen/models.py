from django.db import models
from heizmanager.fields import ListField
from heizmanager.models import Raum


class RaumgruppenManager(models.Manager):
    def filter(self, *args, **kwargs):
        queryset = super(RaumgruppenManager, self).filter(*args, **kwargs)
        ret = []
        for raumgruppe in queryset:
            for raumid in raumgruppe.raeume:
                try:
                    raum = Raum.objects.get(pk=long(raumid))
                except Raum.DoesNotExist:
                    raumgruppe.raeume.remove(raumid)
            raumgruppe.save()
            ret.append(raumgruppe)
        return ret

    def get(self, *args, **kwargs):
        try:
            raumgruppe = super(RaumgruppenManager, self).get(*args, **kwargs)
            for raumid in raumgruppe.raeume:
                try:
                    raum = Raum.objects.get(pk=long(raumid))
                except Raum.DoesNotExist:
                    raumgruppe.raeume.remove(raumid)
            raumgruppe.save()
            return raumgruppe
        except Raumgruppe.DoesNotExist:
            return None


class Raumgruppe(models.Model):
    name = models.CharField(max_length=45, blank=False)
    haus = models.ForeignKey('heizmanager.Haus', related_name='raumgruppen')
    raeume = ListField(models.ForeignKey('heizmanager.Raum'), default=[])

    remote_id = models.BigIntegerField(default=0)

    objects = RaumgruppenManager()

    def get_raeume_sorted(self):
        #sortiert raeume in sortierte etagen in einer [(e,[]),...]
        allr = {}
        for raumid in self.raeume:
            raum = Raum.objects.get(pk=raumid)
            allr.setdefault(raum.etage.name, list()).append((raum.name, raum))
        ret = []
        for e in sorted(allr.keys()):
            ret.append((e, allr[e]))
        return ret

    class Meta:
        app_label = 'raumgruppen'
