from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^get/mixer/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/config/$', views.get_config),
    url(r'^set/mixer/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/status/$', views.set_status),
    url(r'^m_setup/(?P<haus>\d+)/vorlauftemperaturregelung/(?P<action>[a-z]+)/$', views.get_global_settings_page),
    url(r'^m_setup/(?P<haus>\d+)/vorlauftemperaturregelung/(?P<action>[a-z]+)/(?P<entityid>\d+)/$', views.get_global_settings_page),
    ]
