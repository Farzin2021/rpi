from django.test import TestCase, RequestFactory, Client
from django.contrib.auth.models import User
from heizmanager.models import Haus, HausProfil, Etage, Raum, Gateway, Regelung, GatewayAusgang, RPi, Sensor, CryptKeys
from rf.models import RFController, RFAktor, RFAusgang
from django.core.cache import cache as memcache
import logging
import json


class PLTest(TestCase):

    def setUp(self):
        self.usr = User.objects.create_user(username='test@test.de', email='test@test.de', password='test')
        self.usr.save()
        self.haus = Haus(name="Teststr. 1, 12345 Testhausen", eigentuemer=self.usr)
        self.haus.save()
        hausprofil = HausProfil(haus=self.haus, modules='ruecklaufregelung,zweipunktregelung,funksollregelung,pumpenlogik', module_parameters=str({}))
        hausprofil.save()
        etage = Etage(name='testetage', haus=self.haus, position=0)
        etage.save()
        self.raum = Raum(name='testraum 1', etage=etage, position=0, solltemp=21.0)
        self.raum.save()
        self.zweiterraum = Raum(name='testraum 2', etage=etage, position=1, solltemp=22.0)
        self.zweiterraum.save()
        self.gw = Gateway(name='aa-bb-cc-dd-ee-ff', haus=self.haus)
        self.gw.save()
        self.sensor = Sensor(name='11_22_33_44_55_66_77_88', gateway=self.gw, haus=self.haus, raum=self.raum, mainsensor=True)
        self.sensor.save()
        self.rreg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.rreg.save()
        self.zrreg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.zrreg.save()
        self.raum.regelung = self.rreg
        self.raum.save()
        self.zweiterraum.regelung = self.zrreg
        self.zweiterraum.save()
        self.ausgang1 = GatewayAusgang(gateway=self.gw, ausgang="1", regelung=self.rreg)
        self.ausgang1.save()
        self.ausgang2 = GatewayAusgang(gateway=self.gw, ausgang="2", regelung=self.zrreg)
        self.ausgang2.save()

        self.rpi = RPi(haus=self.haus, name="aa-bb-cc-dd-ee-ff", description='')
        self.rpi.save()
        self.rfc = RFController(name="0x12345678", haus=self.haus, rpi=self.rpi)
        self.rfc.save()
        self.rfaktor = RFAktor(controller=self.rfc, name="2")
        self.rfaktor.save()
        self.relais = RFAktor(controller=self.rfc, type="relais", name="3")
        self.relais.save()

        self.ck = CryptKeys()
        self.ck.create_key()
        self.ck.save()
        self.rpi.crypt_keys = self.ck
        self.rpi.save()

        memcache._cache.flush_all()
        self.factory = RequestFactory()
        self.client = Client()
        self.client.login(username='test@test.de', password='test')

    def testCreateAndEditGWPL(self):
        response = self.client.get('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk)
        self.assertEqual(200, response.status_code)
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        params = pl.get_parameters()
        self.assertEqual(params['input'], [(self.gw.id, self.gw.name, self.ausgang1.ausgang)])
        self.assertEqual(params['output'], [(self.gw.id, self.gw.name, '14')])

        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=%s' % (self.haus.pk, pl.pk),
                                    {'pledit': 'true', 'plid': pl.pk, 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '13'): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '12'): 'on'})
        pls = Regelung.objects.filter(regelung='pumpenlogik')
        self.assertEqual(len(pls), 1)
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        params = pl.get_parameters()
        self.assertEqual(params['input'], [(self.gw.id, self.gw.name, self.ausgang1.ausgang)])
        self.assertTrue((self.gw.id, self.gw.name, '12') in params['output'])
        self.assertTrue((self.gw.id, self.gw.name, '13') in params['output'])

    def testCreateAndDeleteGWPL(self):
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})
        self.assertEqual(len(Regelung.objects.filter(regelung='pumpenlogik')), 1)
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        response = self.client.get('/m_setup/%s/pumpenlogik/?pldel=%s' % (self.haus.pk, pl.pk))
        self.assertEqual(len(Regelung.objects.filter(regelung='pumpenlogik')), 0)

    def testCreateEditDeleteRFPL(self):
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.rfc.name, self.rfaktor.name): 'on'})
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        params = pl.get_parameters()
        self.assertEqual(params['input'], [(self.gw.id, self.gw.name, self.ausgang1.ausgang)])
        self.assertEqual(params['output'], [(self.rfc.id, self.rfc.name, self.rfaktor.name)])
        rfa = RFAusgang.objects.get(controller=self.rfc)
        self.assertEqual(rfa.aktor, [self.rfaktor.id])

        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=%s' % (self.haus.pk, pl.pk),
                                    {'pledit': 'true', 'plid': pl.pk, 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang2.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '15'): 'on'})
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        params = pl.get_parameters()
        self.assertEqual(params['input'], [(self.gw.id, self.gw.name, self.ausgang2.ausgang)])
        self.assertEqual(params['output'], [(self.gw.id, self.gw.name, '15')])
        self.assertEqual(0, len(RFAusgang.objects.all()))

        response = self.client.get('/m_setup/%s/pumpenlogik/?pldel=%s' % (self.haus.pk, pl.pk))
        self.assertEqual(0, len(Regelung.objects.filter(regelung='pumpenlogik')))

    def testCheckConsistency(self):
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo1', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})
        ausgang = GatewayAusgang.objects.get(gateway=self.gw, ausgang='14')
        pl1 = Regelung.objects.filter(regelung='pumpenlogik')[0]

        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo2', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, ausgang.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '15'): 'on'})

        self.assertEqual(2, len(Regelung.objects.filter(regelung='pumpenlogik')))

        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=%s' % (self.haus.pk, pl1.pk),
                                    {'pledit': 'true', 'plid': pl1.pk, 'plname': 'pulo1', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, '15'): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})
        pl1 = Regelung.objects.get(pk=pl1.pk)
        params = pl1.get_parameters()
        # sollte nicht gespeichert haben:
        self.assertEqual([(self.gw.id, self.gw.name, self.ausgang1.ausgang)], params['input'])
        self.assertEqual(1, len(params['input']))

    def testCheckFunctionality(self):
        # defval: on
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo1', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]

        self.client.get('/set/%s/20.00/' % self.sensor.name)
        response = self.client.get('/get/%s/1/' % self.gw.name)
        self.assertEqual("<1>", response.content)
        response = self.client.get('/get/%s/14/' % self.gw.name)
        self.assertEqual("<1>", response.content)

        response = self.client.get('/get/%s/all/' % self.gw.name)
        self.assertEqual(response.content[26], "1")

        self.client.get('/set/%s/24.00/' % self.sensor.name)
        response = self.client.get('/get/%s/1/' % self.gw.name)
        self.assertEqual("<0>", response.content)
        response = self.client.get('/get/%s/14/' % self.gw.name)
        self.assertEqual("<0>", response.content)

        self.haus.set_modules(['ruecklaufregelung','zweipunktregelung','funksollregelung'])
        self.assertTrue('pumpenlogik' not in self.haus.get_modules())
        response = self.client.get('/get/%s/14/' % self.gw.name)
        self.assertEqual("<1>", response.content)

        # defval: off
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=%s' % (self.haus.pk, pl.pk),
                                    {'pledit': 'true', 'plid': pl.pk, 'plname': 'pulo1', 'defval': 'off',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.gw.name, '14'): 'on'})

        response = self.client.get('/get/%s/14/' % self.gw.name)
        self.assertEqual("<0>", response.content)

    def testRelais(self):
        response = self.client.post('/m_setup/%s/pumpenlogik/?plid=' % self.haus.pk,
                                    {'pledit': 'true', 'plid': '', 'plname': 'pulo', 'defval': 'on',
                                     'i_%s_%s_ed' % (self.gw.name, self.ausgang1.ausgang): 'on',
                                     'o_%s_%s_ed' % (self.rfc.name, '3'): 'on'})
        pl = Regelung.objects.filter(regelung='pumpenlogik')[0]
        self.client.get('/set/%s/20.00/' % self.sensor.name)
        response = self.client.get('/get/zwave/%s/' % self.rfc.name)
        response = json.loads(self.ck.decrypt(response.content).rsplit('}', 1)[0] + '}')
        self.assertEqual(response['3']['37'][1], 1)

        self.client.get('/set/%s/24.00/' % self.sensor.name)
        response = self.client.get('/get/zwave/%s/' % self.rfc.name)
        response = json.loads(self.ck.decrypt(response.content).rsplit('}', 1)[0] + '}')
        self.assertEqual(response['3']['37'][1], 0)

