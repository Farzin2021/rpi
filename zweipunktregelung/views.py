import re

from heizmanager.models import Raum, GatewayAusgang, Gateway
import heizmanager.cache_helper as ch
import logging
from django.http import HttpResponse
from heizmanager.render import render_response, render_redirect
import pytz
from datetime import datetime, timedelta
from heizmanager.mobile.m_temp import LocalPage, get_module_offsets, get_offsets_icon
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from vsensoren.models import VSensor
import json
from heizmanager.models import sig_new_output_value
from django.views.decorators.csrf import csrf_exempt
from heizmanager.abstracts.base_mode import BaseMode
from heizmanager.getandset import calc_opening


logger = logging.getLogger('logmonitor')


def get_name():
    return u'Zweipunktregelung'


def managesoutputs():
    return False


def do_ausgang(regelung, ausgang):

    # wenns raum nicht gibt in 2punkt, sind wir fertig
    raum = regelung.get_raum()
    if not raum:
        logging.error("raum.doesnotexist")
        return HttpResponse('<0>')

    offset = 0.0
    try:
        sensor = raum.get_mainsensor()
        cnt = sensor.get_wert()[0]
        if cnt < 5:
            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf 1, Frostschutz." % ausgang))
            return HttpResponse('<1>')  # frostschutz
        try:
            mode = BaseMode.get_active_mode_raum(raum)
            raum.refresh_from_db()
        except Exception as e:
            logging.exception("zpr: exception getting mode for raum %s" % raum.id)
            mode = None
        if mode is None or mode.get('use_offsets', False):
            offset = get_module_offsets(raum, only_clear_offsets=False)
    except (TypeError, AttributeError):
        cnt = raum.solltemp + offset

    ret = _zweipunkt(cnt, (raum.solltemp or 4.0) + offset, 0.0)
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    ch.set('lastget_%s_%s' % (regelung.id, ausgang), (ret.content.translate(None, '<>!'), now, None, raum.solltemp+offset))
    ch.set('lastget_%s_%s' % (regelung.id, 0), (ret.content.translate(None, '<>!'), now, None, raum.solltemp+offset))  # fuer get_disp_temp
    logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s." % (ausgang, ret.content.translate(None, '<>!'))))

    return ret


def do_rfausgang(regelung, controllerid):
    ret = do_ausgang(regelung, 0).content
    return int(ret[1]) if ret[1] in ["0", "1"] else 0


def _zweipunkt(cnt, soll, margin):
    if cnt >= float(soll) + margin:
        return HttpResponse("<0>")
    elif cnt < float(soll) - margin:
        return HttpResponse("<1>")
    else:
        return HttpResponse("<>")


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    ret = sig_get_outputs_from_output.send(sender='zpr', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            ms = raum.get_mainsensor()
            if ms:
                belegte_ausgaenge.setdefault(gw, list())
                for name in namen:
                    belegte_ausgaenge[gw].append((name, ausgang, ms, True))

    return belegte_ausgaenge


def get_possible_outputs(haus, raumdict=dict()):

    ret = sig_get_possible_outputs_from_output.send(sender='zweipunktregelung', haus=haus)
    ctrldevices = sig_get_ctrl_devices.send(sender='zpr', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)

    if not len(raumdict):
        raumdict = {}
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id).values_list("id", "name"):
                raumdict[raum[0]] = u"%s/%s" % (etage.name, raum[1])

    for cbfunc, r2s in ret:
        for raum, sensoren in r2s.items():
            ms = raum.get_mainsensor()
            if ms:
                for sensor, ctrldevice in sensoren:
                    if ctrldevice not in ctrldevices:
                        continue
                    if ms == sensor:
                        regler[ctrldevice].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(sensor))])
                        for ctrld in ctrldevices:
                            if ctrldevice != ctrld:
                                regler[ctrld].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(sensor))])

    def uniq(lst):
        last = object()
        for item in lst:
            if item == last:
                continue
            yield item
            last = item

    for ctrld, outs in regler.items():
        regler[ctrld] = list(uniq(sorted(outs)))

    return regler


def get_config(request, macaddr):
    
    if request.method == 'GET':

        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)

        ret = {}
        now = datetime.now()
        params = {'zpr': True}
        # haus = gw.haus
        for ausgang in GatewayAusgang.objects.filter(gateway=gw):
            reg = ausgang.regelung
            if reg.regelung != 'zweipunktregelung':
                continue
            for ausgangno in [int(_o) for _o in reg.gatewayausgang.ausgang.split(',')]:
                try:
                    val = do_ausgang(reg, ausgangno).content.translate(None, "<>")
                    val = calc_opening(val, reg.get_raum().get_betriebsart(), gw.version, ausgangno, reg.get_raum(), gw.haus.profil.get().get_gateway_invertouts(macaddr.lower()))
                except Exception as e:
                    logging.exception("exc")
                    logger.warning("%s|%s" % (reg.raum.id, u"Fehler: Ausgang an %s konnte nicht gesetzt werden, steht auf 0." % macaddr))
                else:
                    ret.update({str(ausgangno): val})
                    lastget = ch.get("lastget_%s_%s" % (reg.id, ausgangno))
                    sig_new_output_value.send(sender='rlr_get_config', name='%s_%s' % (macaddr, ausgangno), value=val, ziel=lastget[3] if lastget is not None and isinstance(lastget, tuple) and len(lastget) == 4 else None, parameters=params, timestamp=now, modules=None)

        ch.set("zpr_%s" % macaddr, ret)
        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):
    if request.method == "POST":
        data = json.loads(request.body)
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            pass
        else:
            if 'error' in data:
                ausgaenge = GatewayAusgang.objects.filter(gateway=gw)
                for ausgang in ausgaenge:
                    if ausgang.regelung and ausgang.regelung.regelung == "zweipunktregelung":
                        logger.warning("%s|%s" % (ausgang.regelung.id, data['error']))

    return HttpResponse()


def get_global_settings_link(request, haus):
    pass


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link


def get_global_settings_page(request, haus):
    pass


def get_local_link(regelung, request, raum):
    pass


def get_local_page(regelung, request, raum):

    if request.method == 'GET':

        if 'gets' in request.GET:

            sensoren = raum.get_sensor_overview()

            return render_response(request, 'm_raum_sensoren.html', {'raum': raum, 'sensoren': sensoren})

        else:

            lines = []
            import importlib
            mods = list()
            haus = raum.etage.haus
            modules = haus.get_modules()
            for modulename in sorted(modules):
                try:
                    module = importlib.import_module('heizmanager.modules.' + modulename)
                except ImportError:
                    try:
                        module = importlib.import_module(modulename.strip() + '.views')
                    except ImportError:
                        continue
                try:
                    link = module.get_local_settings_link(request, raum)
                    if link:
                        mods.append(link)
                except (AttributeError, KeyError):
                    pass
            api_raum = ch.get("api_roomoffset_%s" % raum.id) or {}
            api_haus = ch.get("api_houseoffset_%s" % haus.id) or {}
            for mod in set(api_raum.keys() + api_haus.keys()):
                offset = api_raum.get(mod, 0.0) + api_haus.get(mod, 0.0)
                mods.append((mod, offset, 75, "#"))

            ms = raum.get_mainsensor()
            if ms:

                last = ms.get_wert()
                ch.delete("%s_roffsets_dict" % raum.id)
                try:
                    mode = BaseMode.get_active_mode_raum(raum)
                    raum.refresh_from_db()
                except Exception as e:
                    logging.exception("zpr: exception getting mode for raum %s" % raum.id)
                    mode = None
                if mode is None or mode.get('use_offsets', False):
                    clear_offsets = get_module_offsets(raum, only_clear_offsets=True)
                else:
                    clear_offsets = 0.0

                backup = False
                if last is None and isinstance(ms, VSensor):
                    try:
                        last = ms.backup_sensor.get_wert()
                    except AttributeError:
                        last = None
                    backup = True
                if last is None:
                    return LocalPage(lines=lines, mods=mods, offset="-")

                if backup:
                    temp = "<i>%.2f</i>" % last[0]
                else:
                    temp = "%.2f" % last[0]

                lines.append(('Raumtemperatur', "<small>%s</small> / <b>%.2f</b> " % (temp, raum.solltemp if raum.solltemp else 4.0),
                              112, ""))

                return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

            else:
                return LocalPage(lines=lines, mods=mods, offset="-")

    elif request.method == 'POST' and Raum.objects.get_for_user(request.user, pk=raum.id):
        # keine cache invalidation hier, das macht get state
        try:
            soll = float(request.POST['slidernumber'].replace(',', '.'))
            raum.solltemp = soll
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt (Z1)" % raum.solltemp))
            ausgang = regelung.get_ausgang()
            if ausgang:
                ch.set_gateway_update(ausgang.gateway.name)
            raum.save()
        except ValueError:  # wilde Eingabe
            pass
        return render_redirect(request, '/m_raum/%s/' % raum.id)


def get_local_html(regelung, request, raum):
    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("rlr: exception getting mode for raum %s" % raum.id)
        mode = None

    haus = raum.etage.haus
    hmodules = haus.get_modules()
    hparams = haus.get_module_parameters()
    if mode is None:
        offsets, icon = get_offsets_icon(raum)
    else:
        icon = mode['icon']
        if mode.get('use_offsets', False):
            offsets, icon = get_offsets_icon(raum)
        else:
            offsets = 0.0

    max_offset_module_html = ""
    if icon:
        max_offset_module_html = """<span class="module-iconn"><img src="/static/icons/%s.png" alt="icon"/></span>""" % icon

    ms = raum.get_mainsensor()
    temp = None
    if ms:
        last = ms.get_wert()
        if last is not None:
            temp = last[0]

    if temp is None:
        temp = '-'
    else:
        temp = '%.2f' % temp
    soll = raum.solltemp or 21.0

    if offsets < 0:
        pom = "-"''
        offset_str = ' <span class="heat5_room_offset offstr blue-str" {color}>%05.2f</span>' % abs(offsets)
    else:
        pom = "+"
        offset_str = ' <span class="heat5_room_offset offstr red-str" {color}>%05.2f</span>' % offsets

    soll_offset_color = ""
    hand_time = "&nbsp"
    ziel_color = "#779A45"
    hand_picture = "<img src='/static/icons/icon-clock.png' width='30' height='30'>"
    mode_id = -99
    if mode is not None:
        hand_time = mode['activated_mode_text']
        mode_id = mode['activated_mode_id']
        if 'heizprogramm' not in hmodules:
            hand_picture = "&nbsp"
        else:
            hand_picture = "<img src='/static/icons/%s.png' width='30' height='30'>" % mode['icon']

        soll = mode.get('soll', 21.0)
        if mode_id == -1:
            soll_offset_color = " style='color: gray;'"
            ziel_color = "#d79a2d"

    else:
        if 'heizprogramm' not in hmodules or not hparams.get('is_switchingmode_enabled_%s' % haus.id, True):
            hand_picture = "&nbsp"

    ziel = float(soll) + float(offsets)

    try:
        if float(temp) > float(ziel):
            sign = "<"
        else:
            sign = ">"
    except:
        sign = "-"

    is_hide = ""
    from benutzerverwaltung.decorators import show_pro_visualbar
    if not show_pro_visualbar(request):
        is_hide = "hide"

    bis = ""
    if mode_id == -1:
        bis = "-"

    if len(hand_time) > 10:
        hand_time = hand_time[0:7] + "..."

    html = u'''<a data-role="none" href="/m_raum/{id}/" class="ui-link-inherit">
            <div class="main" data-id={room_id}>
              <div class="left_ui">
                  <div class="ui_details">
                    <span class="room_icon"><img src="/static/icons/{icon}.png" alt="icon"/></span>
                    <span class="room_name">{name}</span></div>
                    <input type='hidden' id="room_row_id" value={room_id}>
                    {html_slider}
                  <div class="ui_details"><div class="beam_place">{html_beam}</div></div>
              </div>
              <div class="right_ui">
                <div class="ui_details temperature">
                    <div class="details_left"><span id="{room_id}" class="soll" {color}>{soll}</span></div>
                    <div class="details_sign signed" id="pom-sign">{pom}</div>
                    <div class="details_right {color}" id="offset">{offset_str}</div>
                    <div class="details_leaf"></div>
                </div>
                <div class="ui_details temperature">
                    <div class="details_left"><span id="{room_id}" class="ziel" style="color:{ziel_color};">{ziel}</span></div>
                    <div class="details_sign"><f class="sign">{sign}</f></div>
                    <div class="details_right"><span class="temp bold underline">{temp}</span></div>
                    <div class="details_leaf">{max_offset_module}</span></div>
                </div>
                <div class="ui_details hand_click">
                    <div class="details_left hand" id="mode_icon">{hand_picture}</div>
                    <div class="details_sign" id="sign_bis">{bis}</div>
                    <div class="details_right hand_time" id="mode_name" data-id="{mode_id}">{hand_time}</div>
                    <div class="details_leaf"></div>
                </div>
              </div>
            </div>
         '''
    if ziel < 10 or ziel > 35:
        html_slider = '''
            <div class="ui_details {hide}">
              <div class="range"><span>10</span></div>
              <div class="visualbar_slider">
                <div class="visual-bar">
                  <div class="slider-place">
                    <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                      <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                      <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                      {arrow_bar}
                    </div>
                  </div>
                </div>
              </div>
              <div class="range"><span>35</span></div>
            </div>
             '''
    else:
        html_slider = '''
                    <div class="ui_details {hide}">
                      <div class="range"><span>10</span></div>
                      <div class="visualbar_slider">
                        <div class="visual-bar">
                          <div class="slider-place">
                            <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                              <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                              <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                              <span id="vbar_ziel_s2"  class="ziel" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {ziel_converted}%; background-color: {ziel_color};"></span>
                              {arrow_bar}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="range"><span>35</span></div>
                    </div>
                     '''

    html_beam = ""
    ausgang = regelung.get_ausgang()
    value = 0
    color = ''
    frame_color = '#969597'  # grey as default
    betriebsart = raum.get_betriebsart()
    if not int(betriebsart):
        html_beam += '<div class="beam" style="border-color: #019BB0;"></div>'
    else:
        if ausgang is not None:
            gw = ausgang.gateway
            gw_inverted = haus.profil.get().get_gateway_invertouts(gw.name.lower())
            for a in ausgang.ausgang.split(','):
                a = a.strip()

                sget = ch.get('lastout_%s_%s' % (regelung.id, a))
                if sget is None:
                    sget = do_ausgang(regelung, a).content
                    try:
                        value = calc_opening(sget, raum.get_betriebsart(), gw.version, a, raum, gw_inverted, nolog=True)
                    except ValueError:
                        value = 0
                else:
                    value = int(sget[0])

                if not ("6.00" > gw.version >= "5.00"):
                    value = 0 if value == 0 else 100

                if gw_inverted:  # re-invert for displaying NO out beams correctly
                    value = 100 - value

                color = "#019BB0" if int(betriebsart) == 1 else "#e61873"
                frame_color = color if 'betriebsarten' in haus.get_modules() else "#969597"  # grey
                html_beam += '''
                                                       <div class="visualbar_slider">
                                                        <div class="visual-bar">
                                                          <div class="slider-place">
                                                            <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:5px;color: {frame_color};">
                                                              <div class="ui-slider-bg" style="width: {value}%; background-color: {background_color}"></div>
                                                              <input type="hidden" class="beam-width-value" value="{value}">
                                                            </div>
                                                          </div>
                                                        </div>
                                                       </div>
                                                    '''.format(frame_color=frame_color, value=int(value), background_color=color)

    html += "</a>"
    visual_temp = 0
    try:
        visual_temp = float(temp)
        # convert to %
        visual_temp = min(((visual_temp-10) * 100) / 25, 100)
        if visual_temp < 0:
            visual_temp = 0
    except:
        pass

    soll = float(soll)
    ziel = float(ziel)
    offset_converted = (abs(float(offsets)) * 100) / 25

    overflow = True
    if soll <= 10:
        soll_converted = 0

    elif soll >= 35:
        soll_converted = 100
    else:
        soll_converted = (float(soll - 10) * 100) / 25
        overflow = False

    if ziel <= 10:
        ziel_converted = 0
    elif ziel >= 35:
        ziel_converted = 100
    else:
        ziel_converted = (float(ziel - 10) * 100) / 25
        overflow = False

    arrow_bar = ""
    delta = 100 - soll_converted
    if not overflow:
        if offsets <= -0.1 and ziel < 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{soll_converted}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets <= -0.1 and ziel > 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{offset_converted}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel < 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{offset_converted}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel > 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{delta}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'

    html = html.format(temp=temp, offset_str=offset_str, soll='%.2f' % soll, sign=sign,
                       ziel="%.2f" % ziel, pom=pom, max_offset_module=max_offset_module_html, id=raum.id,
                       icon=raum.icon, ziel_color=ziel_color, delta=delta,
                       name=raum.name, html_beam=html_beam, html_slider=html_slider, mode_id=mode_id, hand_time=hand_time
                       , room_id=raum.id, hand_picture=hand_picture,
                       color=soll_offset_color, bis=bis)

    html = html.format(color=soll_offset_color,
                       arrow_bar=arrow_bar, soll_converted=soll_converted,
                       visual_temp=visual_temp, delta=delta,
                       ziel_converted=ziel_converted, hide=is_hide, ziel_color=ziel_color, value=int(value), background_color=color)
    html = html.format(delta=delta, ziel_converted=ziel_converted, soll_converted=soll_converted, offset_converted=offset_converted)
    return html
