from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^get/zpr/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/config/$', views.get_config),
    ]
