from django.apps import AppConfig


class DifferenzregelungConfig(AppConfig):
    name = 'differenzregelung'
    verbose_name = 'Differenzregelung'

    def ready(self):
        from . import signals
