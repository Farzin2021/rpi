from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^m_setup/(?P<hausid>\d+)/users/$', views.get_global_settings_page),
    url(r'^accounts/resetpw/$', views.reset_password),
    url(r'^accounts/m_login/$', views.loginview),
    url(r'^accounts/register/$', views.register),
    url(r'^accounts/profile/$', views.index),
    url(r'^accounts/logout/$', views.logout_view),  # django default
    ]
