from django.conf.urls import url
from . import views
urlpatterns = [ 
    url(r'^set/geo/(?P<hausid>\d+)/(?P<device_uuid>[-\w]{36})/(?P<offset>-?\+?\d+((\.|,)\d+)?)/$', views.set_offset),
    url(r'^set/geo/(?P<hausid>\d+)/(?P<device_uuid>[-\w]{36})/(?P<objid>[rvd]{1}\d+)/(?P<offset>-?\+?\d+((\.|,)\d+)?)/$', views.set_offset),
    ]
