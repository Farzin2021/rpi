# coding=utf-8
from heizmanager.render import render_response, render_redirect
from django.shortcuts import HttpResponse
from django.http import JsonResponse
from heizmanager.models import Haus, Raum, Regelung
from models import GeoDevice, GeoOffset
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
import pytz
from django.contrib.contenttypes.models import ContentType
import logging


def get_name():
    return 'Geolocation'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def get_offset(haus, raum=None):
    dev, offset = GeoDevice.objects.get_active_offset(haus, raum)
    return offset or 0.0


def get_offset_regelung(haus, regelungname, objid):
    paramskey = 'mid' if regelungname == 'vorlauftemperaturregelung' else 'dreg'
    for reg in Regelung.objects.filter(regelung=regelungname):
        if str(objid) == reg.get_parameters().get(paramskey):
            break
    else:
        return 0.0

    maxdev, maxoffset = GeoDevice.objects.get_active_offset(haus, reg)
    return maxoffset or 0.0


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/geolocation/'>Geolocation</a>" % haus.id


def get_global_description_link():
    desc = u"Die Heizung wird herunter geregelt, wenn alle Bewohner das Haus verlassen haben."
    desc_link = "http://support.controme.com/geolocation/"
    return desc, desc_link


def get_global_settings_page(request, haus):

    if request.method == "GET":
        if 'add' in request.GET:
            device = GeoDevice()
            return render_response(request, "m_settings_geolocation_edit.html", {"device": device, "haus": haus})

        elif 'edit' in request.GET:
            try:
                device = GeoDevice.objects.get(pk=long(request.GET['edit']))
            except:
                return render_redirect(request, "/m_setup/%s/geolocation/" % haus.id)

            return render_response(request, "m_settings_geolocation_edit.html", {"device": device, "haus": haus})

        elif 'gddel' in request.GET:
            try:
                device = GeoDevice.objects.get(pk=long(request.GET['gddel']))
                device.delete()
            except:
                pass

            return render_redirect(request, "/m_setup/%s/geolocation/" % haus.id)

        else:
            devices = GeoDevice.objects.filter(haus=haus)
            params = haus.get_module_parameters()
            context = dict()
            context['haus'] = haus
            context['devices'] = devices
            context['r_m_active'] = params.get('geolocation_right_menu', True)
            return render_response(request, 'm_settings_geolocation.html', context)

    elif request.method == "POST":

        if 'right_menu_ajax' in request.POST:
            params = haus.get_module_parameters()
            params['geolocation_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        deviceid = request.POST.get('deviceid')
        name = request.POST.get('name', '')
        if deviceid:
            try:
                device = GeoDevice.objects.get(pk=long(deviceid))
                device.name = name
            except:
                device = GeoDevice(name=name, haus=haus)
        else:
            device = GeoDevice(name=name, haus=haus)
        device.save()

        return render_redirect(request, "/m_setup/%s/geolocation/" % device.haus_id)


def get_local_settings_link(request, raum):

    haus = raum.etage.haus
    activate(raum)

    rdev, roffset = GeoDevice.objects.get_active_offset(haus, raum)
    hdev, hoffset = GeoDevice.objects.get_active_offset(haus, None)
    offset = (roffset or 0.0) + (hoffset or 0.0)

    if offset != 0.0 and offset is not None:
        return "Geolocation", "Geolocation", "%.2f" % offset, 75, "/m_raum/%s/geolocation/" % raum.id
    else:
        return "Geolocation", "Geolocation", "deaktiviert", 75, "#"


def get_local_settings_page(request, raum=None):
    if raum is None:
        return render_redirect(request, "/")
    haus = raum.etage.haus

    ct = ContentType.objects.get_for_model(Raum)
    devices = GeoDevice.objects.filter(haus=haus, content_type=ct, object_id=raum.id).values_list("id")
    offsets = GeoOffset.objects.filter(haus=haus, device_id__in=devices).order_by('device', 'created')

    timezone.activate(pytz.timezone("Europe/Berlin"))
    rdev, roffset = GeoDevice.objects.get_active_offset(haus, raum)
    hdev, hoffset = GeoDevice.objects.get_active_offset(haus, None)
    return render_response(request, "m_raum_geolocation.html",
                           {"haus": haus, "raum": raum, "offsets": offsets, "rdev": rdev,
                            "roffset": roffset, "hdev": hdev, "hoffset": hoffset})


def activate(hausoderraum):
    if 'geolocation' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['geolocation'])


def deactivate(hausoderraum):
    modules = hausoderraum.get_modules()
    try:
        modules.remove('geolocation')
        hausoderraum.set_modules(modules)
    except ValueError:  # nicht in modules
        pass


def get_local_settings_page_haus(request, haus, action=None, objid=None):

    if action == "delete":
        try:
            offset = GeoOffset.objects.get(id=long(objid))
            offset.delete()
        except:
            pass
        return render_redirect(request, "/m_geolocation/%s/" % haus.id)

    devices = GeoDevice.objects.filter(haus=haus, content_type=None).values_list("id")
    offsets = GeoOffset.objects.filter(haus=haus, device_id__in=devices).order_by('device', 'created')

    timezone.activate(pytz.timezone("Europe/Berlin"))
    maxdev, maxoffset = GeoDevice.objects.get_active_offset(haus, None)

    return render_response(request, "m_geolocation.html",
                           {"haus": haus, 'lastoffsets': offsets, "max_device": maxdev, 'max_offset': maxoffset})


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return get_name(), 'geolocation', offset, 75, '/m_%s/%s/show/%s/geolocation/' % (modulename, haus.id, objid)


def get_local_settings_page_regelung(request, haus, modulename, objid, params):
    paramskey = 'mid' if modulename == 'vorlauftemperaturregelung' else 'dreg'
    for reg in Regelung.objects.filter(regelung=modulename):
        if str(objid) == reg.get_parameters().get(paramskey):
            ct = ContentType.objects.get_for_model(Regelung)
            devices = GeoDevice.objects.filter(haus=haus, content_type=ct, object_id=reg.id).values_list("id")
            offsets = GeoOffset.objects.filter(haus=haus, device_id__in=devices).order_by('device', 'created')
            maxdev, maxoffset = GeoDevice.objects.get_active_offset(haus, reg)
            break
    else:
        offsets = []
        maxdev, maxoffset = None, None

    timezone.activate(pytz.timezone("Europe/Berlin"))

    return render_response(request, "m_regelung_geolocation.html",
                           {"haus": haus, "modulename": modulename, "objid": objid, "lastoffsets": offsets,
                            "max_device": maxdev, "max_offset": maxoffset})


@csrf_exempt
def set_offset(request, hausid, device_uuid, objid=None, offset=None):
    haus = Haus.objects.get(id=long(hausid))

    if objid is None:
        for etage in haus.etagen.all():
            for raum in etage.raeume.all().only("modules"):
                activate(raum)
        ct = None
        objid = 0

    else:
        model_prefix = objid[0]
        objid = int(objid[1:])
        if model_prefix == 'r':
            mdl = Raum
            try:
                raum = Raum.objects.get(pk=objid)
                activate(raum)
            except Raum.DoesNotExist:
                return HttpResponse(status=404)
        elif model_prefix in {'d', 'v'}:
            mdl = Regelung
            regname = 'vorlauftemperaturregelung' if model_prefix == 'v' else 'differenzregelung'
            paramskey = 'mid' if model_prefix == 'v' else 'dreg'
            for reg in Regelung.objects.filter(regelung=regname):
                if str(objid) == reg.get_parameters().get(paramskey):
                    objid = reg.id
                    break
            else:
                return HttpResponse(status=404)
        else:
            return HttpResponse(status=404)
        ct = ContentType.objects.get_for_model(mdl)

    try:
        device = GeoDevice.objects.get(uuid=device_uuid, content_type=ct, haus=haus, object_id=objid)
    except GeoDevice.DoesNotExist:
        if ct is not None and objid > 0:
            # geodevice for this r/d/v hasn't been created yet
            try:
                master_dev = GeoDevice.objects.get(uuid=device_uuid, content_type=None, haus=haus)
            except GeoDevice.DoesNotExist:
                return HttpResponse(status=404)
            device = GeoDevice(haus=haus, uuid=device_uuid, content_type=ct, object_id=objid, name=master_dev.name)
            device.save()
        else:
            return HttpResponse(status=404)

    GeoOffset(haus=haus, device=device, offset=float(offset.replace(',', '.'))).save()

    return HttpResponse("done")
