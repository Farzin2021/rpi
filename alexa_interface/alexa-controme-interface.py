import configparser
import os
import json
import urllib.request
import urllib.parse
import time
import logging
import sys
import sqlite3
import ast

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(message)s')
#logging.basicConfig(level=logging.WARNING, format='%(asctime)s %(message)s')
logging.debug('alexa-interface started')

config_filename = os.path.dirname(os.path.realpath(__file__)) + '/alexa_config.ini'

# seconds to wait between calls (add this to reaction time of local mini-server and alexa-endpoint)
delay = 30

logging.debug('reading config file "%s"', config_filename)
config = configparser.ConfigParser()
config.read(config_filename)
config_section = config['alexa_config']

endpoint = config_section['endpoint']
#login_user = config_section['login_user']
#login_pass = config_section['login_pass']
local_server = config_section['local_server']
home_id = config_section['home_id']
error_log_file = config_section['error_log_file']

mac_id = open('/sys/class/net/eth0/address').readline().strip().lower()

logging.debug('endpoint = ' + endpoint)
#logging.debug('login_user = ' + login_user)
#logging.debug('login_pass = ' + login_pass)
logging.debug('mac_id = ' + mac_id)
logging.debug('local_server = ' + local_server)
logging.debug('home_id = ' + home_id)
logging.debug('error_log_file = ' + error_log_file)


def get_alexa_id():
    """
    :return: mac address if the user has not provided alexa id in: m_setup/1/alexa_interface/changeid/
    """
    conn = sqlite3.connect("/home/pi/rpi/db.sqlite3")
    c = conn.cursor()
    c.execute("select module_parameters from heizmanager_hausprofil where haus_id=%s" % home_id)
    h_params = c.fetchone()
    h_params = ast.literal_eval(h_params[0]) if h_params is not None else dict()
    return h_params.get('alexa_interface', {}).get('alexa_id', mac_id)


def get_json_v1(request_type):
    global home_id
    try:
        result = urllib.request.urlopen(local_server + '/get/json/v1/' + home_id + '/' + request_type + '/')
    except urllib.error.HTTPError as e:
        if e.code == 404:  # wrong home_id
            home_id = "2" if home_id == "1" else "1"
        else:  # temperaturszenen probably not activated, would return code 400
            pass
        json_response = "{}"
    else:
        json_response = result.read()
        json_response = json_response.decode("utf-8")
    # logging.debug('got json: ' + str(json_response))
    return json.loads(json_response)


def set_json_v1(url_ending, form_data={}):
    #form_data['user'] = login_user
    #form_data['password'] = login_pass
    form_data['duration'] ="default"
    encoded_args = urllib.parse.urlencode(form_data).encode('utf-8')
    url = local_server + '/set/json/v1/' + home_id + '/' + url_ending
    urllib.request.urlopen(url, encoded_args)


def set_soll(room, temp):
    logging.info('setting room %s to %s degrees', room, temp)
    data = {
        'ziel': temp
    }
    set_json_v1('ziel/' + room + '/', data)


def set_scene(scene):
    logging.info('setting scene to %s', scene)
    set_json_v1('temperaturszenen/' + scene + '/')


def get_temps():
    logging.debug('getting temps and setup from local mini-server')

    setup = {}
    temperatures = {}

    decoded = get_json_v1('temps')

    for floor in decoded:
        for room in floor['raeume']:
            room_id = room['id']

            setup[room_id] = {
                'name': room['name'],
                'soll': room['solltemperatur']
            }

            temperatures[room_id] = room['temperatur']

    return {
                'setup': {
                    'rooms': setup
                },
                'temps': temperatures
    }


def get_scenes():
    logging.debug('getting scenes from local mini-server')

    scenes = {}

    decoded = get_json_v1('temperaturszenen')

    for scene in decoded:
        scene_id = scene['id']
        scene_name = scene['Name']
        scene_is_active = False
        for room in scene['raeume']:
            if room['active']:
                scene_is_active = True
        scenes[scene_id] = {
            'name': scene_name,
            'active': scene_is_active
        }

    return scenes


baseline = {}
while True is True:
    logging.debug('updating...')

    if not os.path.exists("/home/pi/rpi/alexa_interface/run.pid"):
        sys.exit(0)

    try:
        temps = get_temps()
        temps['setup']['scenes'] = get_scenes()
        temps['hardwareid'] = get_alexa_id()

        if temps['setup'] != baseline:
            baseline = temps['setup']
        else:
            del temps['setup']

        send_data = json.dumps(temps).encode('utf8')
        #logging.debug('send to alexa-endpoint: ' + str(send_data))
        request = urllib.request.Request(endpoint, data=send_data, headers={'content-type': 'application/json'})
        response = urllib.request.urlopen(request)

        response_data = json.loads(response.read().decode("utf-8"))

        set_data = response_data['set'] if response_data else None
        if set_data:
            logging.debug('command from alexa-endpoint: ' + str(response_data))

            scene_to_set = set_data['scene']
            if scene_to_set:
                set_scene(scene_to_set)

            for soll in set_data['soll']:
                room_to_set = soll['room_id']
                temp_to_set = soll['temp']
                set_soll(room_to_set, temp_to_set)

        else:
            logging.debug('got no response from endpoint')

    except Exception as e:
        logging.error(e, exc_info=True)

    time.sleep(delay)
