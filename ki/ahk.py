from fabric.api import local
from heizmanager.models import Raum, Haus
from logger.models import SimpleSensorLog, SimpleOutputLog
from django.db.models import Q
from datetime import datetime, timedelta
import pytz
import ast
import numpy as np
import csv
import logging
import os
from itertools import ifilter
import math
from celery.decorators import task
import heizmanager.cache_helper as ch

try:
    import pandas as pd
except ImportError:
    try:
        dv = local("cat /etc/debian_version", capture=True)
        if dv.startswith("10."):
            local("sudo pip uninstall -y numpy pandas scikit-learn")
            local("sudo apt-get update")
            local("sudo apt-get install -y python-pandas python-numpy python-sklearn")
        else:
            local("""sudo su -c 'echo "CONF_SWAPSIZE=1024" > /etc/dphys-swapfile'""")
            try:
                local("sudo dphys-swapfile setup")
            except:
                local("sudo pip install cython==0.29.15 pandas==0.20.3")  # proserver
            else:
                local("sudo dphys-swapfile swapon")
                local("sudo pip install cython==0.29.15")
                local("nohup sh -c 'sudo pip install pandas==0.20.3 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat' &")
    except:
        raise ImportError("exc running pandas install")

try:
    from sklearn import linear_model
    from sklearn.model_selection import KFold
    from sklearn import neighbors
    from sklearn import tree
    from sklearn.svm import SVR
except ImportError:
    try:
        dv = local("cat /etc/debian_version", capture=True)
        if dv.startswith("10."):
            local("sudo pip uninstall -y numpy pandas scikit-learn")
            local("sudo apt-get update")
            local("sudo apt-get install -y python-pandas python-numpy python-sklearn")
        else:
            local("sudo apt-get update")
            local("sudo apt-get install -y gfortran libopenblas-dev liblapack-dev")
            local("nohup sh -c 'sudo pip install -U numpy==1.16.6 && sudo apt-get install -y python-scipy && sudo pip install scikit-learn==0.20.4 && sudo supervisorctl -c /etc/supervisor/supervisord.conf restart uwsgi celery celerybeat' &")
    except:
        raise ImportError("exc running sklearn install")

# try:
#     import matplotlib as mpl
#     mpl.use('Agg')
#     import matplotlib.pyplot as plt
#     import matplotlib.dates as mdates
# except ImportError:
#     plt = mdates = None


class RoomAnalysis:

    # todo stuetzstellen fuer jeden ruecklauf in einem raum einzeln berechnen
    # todo mehr at loggen in calc_hftemps
    # todo nicht immer alle logs lesen (zwei jahre zurueck; nur x datenpunkte; nur innerhalb schwellen)
    # todo adjust_curve sollte irgendwie von diesem solltemp change signal ausgeloest und in taskqueue geworfen werden
    # todo wie setzt man target_rz das optimal?

    def __init__(self, raum, start_ts):
        self.raum = raum
        self.start_ts = start_ts

        self.ms = self.raum.get_mainsensor()
        if self.ms is None:
            raise ValueError("\traum hat keine mainsensor")
        self.regelung = self.raum.get_regelung()
        if self.regelung.regelung != 'ruecklaufregelung':
            raise ValueError("\tregelung ist keine ruecklaufregelung")
        self.ausgang = self.regelung.get_ausgang()
        if self.ausgang is None:
            raise ValueError("\traum hat keinen ausgang")
        # try:
        #     if not ("6.00" > self.ausgang.gateway.version >= "5.00"):
        #         raise ValueError("\tgwausgang im raum ist nicht an progw")
        # except Exception:
        #     raise ValueError("\traum hat keinen gwausgang")

        self.resample_intervall = "30min"
        self.window_lag = 30  # sinnvollerweise gleich lang wie das resample intervall

        self.rlzielwerte = pd.DataFrame()
        self.df_rlzielwerte = pd.DataFrame()
        self.df_ms = pd.DataFrame()
        self.df_at = pd.DataFrame()
        self.df_rlsensorlog = pd.DataFrame()
        self.grouped_outs = {}  # {rlsensorid: {out: groups}}
        self.windows = {}
        self.target_rz = None

        self.regressor = None
        self.model = None
        self.score = 0

    def _get_rlzielwerte(self):
        self.rlzielwerte = SimpleOutputLog.objects.filter(name="hfo_params_raum_%s" % self.raum.id, timestamp__gte=self.start_ts).values_list("timestamp", "name", "ziel", "parameters")  # sol.ziel hat das rlziel
        # if len(self.rlzielwerte):
        #     # originalparameter
        #     params = ast.literal_eval(self.rlzielwerte[0][3])
        #     von = params['hf_temp_minustwentyat']
        #     bis = params['hf_temp_plustwentyat']
        #     print [int(von - i*(von-bis)/41) for i in range(41)]
        self.df_rlzielwerte = pd.DataFrame(list(self.rlzielwerte), columns=["timestamp", "name", "ziel", "parameters"])
        self.df_rlzielwerte['ziel'] = self.df_rlzielwerte['ziel'].replace(to_replace=0, method='ffill')
        self.df_rlzielwerte['timestamp'] = pd.to_datetime(self.df_rlzielwerte['timestamp'], unit="ms")

    def _get_mslog(self):
        mslog = SimpleSensorLog.objects.filter(name=self.ms.get_identifier(), timestamp__gte=self.start_ts).values_list("timestamp", "value")
        self.df_ms = pd.DataFrame(list(mslog), columns=["timestamp", "value"])
        self.df_ms['timestamp'] = pd.to_datetime(self.df_ms['timestamp'], unit="ms")

    def _get_atlog(self):
        atlog = SimpleSensorLog.objects.filter(name="cnttemp_raum_%s" % self.raum.id, timestamp__gte=self.start_ts).values_list("timestamp", "value")
        self.df_at = pd.DataFrame(list(atlog), columns=["timestamp", "value"])
        self.df_at['timestamp'] = pd.to_datetime(self.df_at["timestamp"], unit="ms")

    def gather_logs(self):
        try:
            self._get_rlzielwerte()
            self._get_mslog()
            self._get_atlog()
        except Exception:
            logging.exception("asdf")
        else:

            if len(self.df_rlzielwerte) and len(self.df_ms) and len(self.df_at):
                regparams = self.regelung.get_parameters()
                rlsensorssn = regparams.get('rlsensorssn', {})
                gw = self.regelung.get_ausgang().gateway

                for sensorid, outs in rlsensorssn.items():
                    rlsensorlog = SimpleSensorLog.objects.filter(name=sensorid, timestamp__gte=self.start_ts).values_list("timestamp", "value")  # name dazu?
                    self.df_rlsensorlog = pd.DataFrame(list(rlsensorlog), columns=["timestamp", "value"])
                    self.df_rlsensorlog['timestamp'] = pd.to_datetime(self.df_rlsensorlog['timestamp'], unit="ms")

                    for out in outs:
                        out_logs = SimpleOutputLog.objects.filter(Q(name="%s_%s" % (gw.name, out)), Q(timestamp__gte=self.rlzielwerte[0][0]) & Q(timestamp__gte=self.start_ts)).values_list("timestamp", "name", "value", "ziel")  # hier haben wir das mit .ziel das raumziel, und mit .value, ob der ausgang offen ist (aber nur 0/1)
                        df_sol_out = pd.DataFrame(list(out_logs), columns=["timestamp", "name", "value", "ziel"])
                        df_sol_out['timestamp'] = pd.to_datetime(df_sol_out['timestamp'], unit="ms")
                        df_sol_out.sort_index(inplace=True)
                        df_sol_out.reset_index(inplace=True)
                        df_sol_out_bool = df_sol_out['value'] != df_sol_out['value'].shift()
                        df_sol_out_cumsum = df_sol_out_bool.cumsum()
                        groups = df_sol_out.groupby(df_sol_out_cumsum)
                        self.grouped_outs.setdefault(sensorid, dict())
                        self.grouped_outs[sensorid][out] = groups

    def extract_windows(self):

        for sensorid, outs in self.grouped_outs.items():
            for out, groups in outs.items():

                total_df = []

                for idx, gdata in groups:
                    if gdata.iloc[0].value and (gdata.iloc[-1].timestamp - gdata.iloc[0].timestamp).total_seconds()/60 > 20:  # es wurde geheizt und es wurde fuer mehr als 20 minuten geheizt

                        tminuslagts = gdata.iloc[-1].timestamp

                        def _extract_intervall(_df):
                            ret = _df.loc[(_df.timestamp >= gdata.iloc[0].timestamp) & (_df.timestamp <= gdata.iloc[-1].timestamp + timedelta(minutes=self.window_lag))]
                            ret = ret.set_index(ret.timestamp)
                            return ret

                        _df_ms = _extract_intervall(self.df_ms)
                        _df_rlsensor = _extract_intervall(self.df_rlsensorlog)
                        _df_at = _extract_intervall(self.df_at)
                        _df_rlziel = _extract_intervall(self.df_rlzielwerte)

                        for i in range(self.window_lag / 5):  # auffuellen, damit man rt/ist gegeneinander vergleichen kann
                            gdata = gdata.append({'timestamp': gdata.iloc[-1].timestamp+timedelta(minutes=(i+1)*5), 'ziel': gdata.iloc[-1].ziel}, ignore_index=True)
                        gdata = gdata.set_index(gdata.timestamp)

                        atresample = _df_at[_df_at.timestamp <= tminuslagts].resample(self.resample_intervall, base=gdata.iloc[0].timestamp.minute).mean()
                        raumzielresample = gdata.ziel.resample(self.resample_intervall, base=gdata.iloc[0].timestamp.minute).mean()
                        msresample = _df_ms[_df_ms.timestamp <= tminuslagts].resample(self.resample_intervall, base=gdata.iloc[0].timestamp.minute).mean()
                        rlresample = _df_rlsensor[_df_rlsensor.timestamp <= tminuslagts].resample(self.resample_intervall, base=gdata.iloc[0].timestamp.minute).mean()

                        try:
                            hfo_params = ast.literal_eval(_df_rlziel.iloc[0].parameters)
                        except IndexError:
                            continue
                        else:
                            # above = hfo_params['activity_threshold_above_target']
                            below = hfo_params['activity_threshold_below_target']

                        try:
                            df = pd.concat(
                                [
                                    atresample.rename(columns={"value": "atr"}),
                                    raumzielresample.rename(columns={"ziel": "rz"}),
                                    msresample.rename(columns={"value": "rt"}),
                                    rlresample.rename(columns={"value": "rlr"})
                                ],
                                axis=1)
                        except IndexError:
                            # leeres resample
                            continue
                        df['rt_shift'] = df['rt'].shift(-1)
                        df = df.dropna()
                        df['below'] = pd.Series([below]*len(df), index=df.index)
                        df.rename(columns={0: 'rz'}, inplace=True)  # weil das oben aus irgendeinem grund nicht passiert

                        if not len(total_df):
                            total_df = df
                        else:
                            total_df = pd.concat([total_df, df])

                self.windows.setdefault(sensorid, dict())
                self.windows[sensorid][out] = total_df

    def read_data_from_csv(self):
        logpath = "/home/pi/rpi/ki/logs/"
        csvfiles = [f for f in os.listdir(logpath) if os.path.isfile(os.path.join(logpath, f))]
        for cf in csvfiles:
            with open(os.path.join(logpath, cf), 'rb') as f:
                for row in ifilter(lambda x: x[0].startswith("%s_" % self.raum.id), csv.reader(f, delimiter=',')):
                    _id, sensorid, out = row[0].split('_')
                    self.windows.setdefault(sensorid, dict())
                    self.windows[sensorid].setdefault(out, pd.DataFrame(columns=['atr', 'rt', 'rt_shift', 'rlr', 'rz', 'below', 'ts']))
                    dfrow = pd.DataFrame([[float(row[5]), float(row[2]), float(row[3]), float(row[4]), float(row[6]), float(row[7]), row[1]]], columns=['atr', 'rt', 'rt_shift', 'rlr', 'rz', 'below', 'ts'])
                    self.windows[sensorid][out] = pd.concat([self.windows[sensorid][out], dfrow])

    def write_data_to_csv(self):
        logfile = '/home/pi/rpi/ki/logs/log_%s_%s.csv' % (self.start_ts.year, self.start_ts.month)
        if not os.path.isdir("/home/pi/rpi/ki/logs/"):
            local("mkdir /home/pi/rpi/ki/logs/")
        if not os.path.isfile(logfile):
            with open(logfile, 'w') as f:
                f.write('id,ts,rt,rt_shift,rlr,atr,rz,below\n')

        with open(logfile, "a") as csvout:
            writer = csv.writer(csvout)
            for sensorid, outs in self.windows.items():
                for out, data in outs.items():
                    for idx, d in data.iterrows():
                        _id = "%s_%s_%s" % (self.raum.id, sensorid, out)
                        writer.writerow((_id, idx, round(d.rt, 2), round(d.rt_shift, 2), round(d.rlr, 2), round(d.atr, 2), round(d.rz, 2), d.below))

    def evaluate_regressors(self):

        def get_splits(_data):
            kf = KFold(n_splits=5)
            X = _data[['rt', 'rlr', 'atr']]
            y = _data.rt_shift
            for train_idx, test_idx in kf.split(X, y):
                X_train, X_test = X.iloc[train_idx], X.iloc[test_idx]
                y_train, y_test = y.iloc[train_idx], y.iloc[test_idx]
                yield X_train, X_test, y_train, y_test

        def get_regressors():
            for _reg in [  # linear_model.Ridge(),
                        linear_model.BayesianRidge(),
                        linear_model.LinearRegression(),
                        neighbors.KNeighborsRegressor(5, weights='uniform'),
                        # tree.DecisionTreeRegressor(max_depth=1),
                        # SVR(),
                        # linear_model.HuberRegressor(epsilon=1.1)  # predicted immer den momentanen wert?
                    ]:
                yield _reg

        def predict_trend(model, X_test, y_test):
            evaldf = pd.DataFrame({'prediction': model.predict(X_test), 'actual': y_test, 'previous': X_test.rt})
            evaldf.prediction = evaldf.prediction.round(2)
            evaldf.actual = evaldf.actual.round(2)
            evaldf.previous = evaldf.previous.round(2)
            evaldf['predprev'] = (evaldf.prediction - evaldf.previous).abs()
            evaldf['actprev'] = (evaldf.actual - evaldf.previous).abs()

            evaldf['eval'] = np.where(((evaldf.prediction > evaldf.previous) & (evaldf.actual > evaldf.previous)) |
                                      ((evaldf.prediction < evaldf.previous) & (evaldf.actual < evaldf.previous)) |
                                      # ((evaldf.prediction == evaldf.previous) & (evaldf.actual == evaldf.previous)) |
                                      ((evaldf.predprev < 0.03) & (evaldf.actprev == 0)),
                                      '+', '-')

            evaldf = evaldf.groupby('eval').count()
            try:
                return round(evaldf['actual'][0]/float(evaldf['actual'][0]+evaldf['actual'][1]), 2)
            except:
                if len(evaldf['actual']) == 1:  # todo ist das ok? oder besser None zurueck?
                    return 1.0
                else:
                    return "no prediction"

        def eval_models(_data):
            # print "\tevaluating %s data points" % len(data)
            for _reg in get_regressors():
                ret = []
                for X_train, X_test, y_train, y_test in get_splits(_data):
                    model = _reg.fit(X_train, y_train)
                    ret.append(float(predict_trend(model, X_test, y_test)))
                # print "\t{0:25} {1} {2}".format(type(_reg).__name__, ret, sum(ret)/len(ret))
                yield _reg, ret, sum(ret)/len(ret)

        all_data = pd.DataFrame(columns=['atr', 'rt', 'rt_shift', 'rlr', 'rz', 'below', 'ts'])
        for sensorid, outs in self.windows.items():
            for out, data in outs.items():
                if not len(data):
                    continue
                data = data.dropna()
                if not len(data):
                    continue
                all_data = pd.concat([all_data, data])
                # if len(data) < 10:
                #     continue
                # print eval_models(data)  # einzelnen ausgang evaluieren

        if not len(all_data) or len(all_data) < 10:
            return

        self.target_rz = all_data.rz.median()

        avg = 0
        reg = None
        for regressor, cv_prediction_accuracies, avg_accuracy in eval_models(all_data):
            if avg_accuracy > avg:
                avg = avg_accuracy
                reg = regressor

        if avg > 0.5:  # todo oder noch mehr kriterien?
            self.regressor = reg
            self.model = reg.fit(all_data[['rt', 'rlr', 'atr']], all_data.rt_shift)
            self.score = avg

        # print "\tselected regressor:", type(self.regressor).__name__, avg

    def adjust_curve(self):

        if self.model is None:
            # print "\tno model created"
            return
        if self.target_rz is None:
            # print "\tno target_rz set"
            return

        ats = [at-20 for at in range(0, 41)]
        # rzs = [20.5, 21, 21.5]  # die ergebnisse von predict schwanken teils massiv, wenn man rz um 1K variiert

        points = []
        for at in ats:
            mindiff = 100
            minrlr = 0
            for rlr in xrange(20, 55):
                _y = self.model.predict(np.array([[self.target_rz, rlr, at]]))[0]
                if math.fabs(self.target_rz - _y) < mindiff:
                    mindiff = math.fabs(self.target_rz - _y)
                    minrlr = rlr
            if minrlr > 0:
                # print "{0:5}: {1} -> {2}".format(at, minrlr, mindiff)
                points.append(minrlr)

        if len(points) == 41:
            # wir speichern die stuetzstellen und interpolieren manuell anstatt ein polynom zu fitten
            # print "\tcalculated", points
            hfo_params = self.raum.get_hfo_params()
            hfo_params['points'] = points
            hfo_params['score'] = self.score
            hfo_params['points_for'] = self.target_rz
            self.raum.set_spec_module_params('heizflaechenoptimierung', hfo_params)
            # if hfo_params.get('ki_mode') == "proki":
            ch.set_usage_val("proki_score", {self.raum.id: round(self.score, 2)})
            ch.set_usage_val("active_ki", {self.raum.id: hfo_params.get('ki_mode')})


def analyze_logs():
    utc = pytz.timezone("UTC")
    start_ts = datetime.now(utc) - timedelta(days=1)  # needs to be timezoneaware! utc?
    for raum in Raum.objects.all():
        try:
            ra = RoomAnalysis(raum, start_ts)
            ra.gather_logs()
            ra.extract_windows()
            ra.write_data_to_csv()
        except Exception as e:
            logging.exception("raum %s" % raum.id)


def adjust_curves():
    utc = pytz.timezone("UTC")
    start_ts = datetime.now(utc) - timedelta(days=1)  # needs to be timezoneaware! utc?

    for haus in Haus.objects.all():
        hparams = haus.get_module_parameters()
        wp_active = hparams.get('wetter_pro', {}).get('is_active', False)
        for etage in haus.etagen.all():
            for raum in etage.raeume.all():
                hmods = haus.get_modules()
                mods = raum.get_modules()
                if "ki" in hmods and wp_active and "ki" not in mods:
                    raum.set_modules(mods + ['ki'])
                elif ("ki" not in hmods or not wp_active) and "ki" in mods:
                    mods.remove("ki")
                    raum.set_modules(mods)
                    continue

                try:
                    ra = RoomAnalysis(raum, start_ts)
                    ra.read_data_from_csv()
                    ra.evaluate_regressors()
                    ra.adjust_curve()
                except Exception as e:
                    logging.exception("raum %s" % raum.id)


@task(name="adjust_ondemand")
def adjust_ondemand(raum, target_rz):
    utc = pytz.timezone("UTC")
    start_ts = datetime.now(utc) - timedelta(days=1)  # needs to be timezoneaware! utc?

    try:
        ra = RoomAnalysis(raum, start_ts)
        ra.read_data_from_csv()
        ra.evaluate_regressors()
        ra.target_rz = target_rz
        ra.adjust_curve()
    except Exception as e:
        logging.exception("raum %s" % raum.id)


def run():

    utc = pytz.timezone("UTC")
    start_ts = datetime(year=2020, month=2, day=1, tzinfo=utc)  # needs to be timezoneaware! utc?

    for raum in Raum.objects.all():
        try:
            ra = RoomAnalysis(raum, start_ts)
        except Exception as e:
            print raum.id, str(e), '\n'
        else:
            print raum.id, raum.name
            # ra.gather_logs()
            # ra.extract_windows()
            # ra.write_data_to_csv()

            # ra.read_data_from_csv()
            # ra.analyse_logs()

            ra.gather_logs()
            ra.extract_windows()
            ra.evaluate_regressors()
            ra.adjust_curve()


if __name__ == "__main__":
    run()
