# -*- coding: utf-8 -*-

from heizmanager.render import render_response
from heizmanager.models import Haus
from django.http import HttpResponse
import sqlite3
from datetime import datetime, timedelta
import csv
import ast


def get_name():
    return u'Ki'


def get_global_settings_link(request, haus):
    # 	# return u"<a href='/m_setup/%s/ki/'>Deep Learning für Fußbodenheizung</a>" % haus.id
    # return u"Deep Learning für Fußbodenheizung"
    return None


def get_global_description_link():
    desc = u"Die KI (künstliche Intelligenz) lernt die optimale Fußbodentemperatur."
    desc_link = "https://support.controme.com/fussbodentemperatur-ki/ "
    return desc, desc_link


def get_global_settings_page(request, haus):
    return u'KI'


# This method calculates the starting point of the average calculation depending on end and t_interval.
def calc_start(end, t_intervall):
    # String end to datetime object
    datetime_str = end[2:]
    datetime_end = datetime.strptime(datetime_str, '%y-%m-%d %H:%M:%S.%f')
    datetime_start = datetime_end - timedelta(days=t_intervall)
    year = datetime_start.year

    # datetime object to String
    start = datetime_start.strftime('%y-%m-%d %H:%M:%S.%f')
    start = str(year) + start[2:]
    return start


# This method calculates the associated room and the sensors for given raumid and hausid.
def calc_sensoren(hausid, raumid):
    haus = Haus.objects.get(pk=long(hausid))
    etagen = haus.etagen.all()
    raum = None
    for etage in etagen:
        raeume = etage.raeume.all()
        for room in raeume:
            if room.id == raumid:
                raum = room
                break

    it_sensor = raum.get_mainsensor()
    it_identifier = it_sensor.get_identifier()

    rlt_sensor = None
    sensoren = raum.get_sensoren()
    for sensor in sensoren:
        if (sensor is not it_sensor):
            rlt_sensor = sensor
            break
    rlt_identifier = rlt_sensor.get_identifier()

    return it_identifier, rlt_identifier, raum


def calc_intervals(start, end, actuator):
    '''
    This method calculates the intervals in which the actuator was open or closed.
    :param start: start time of the average interval
    :param end: end time of the average interval
    :param actuator: values of 0 or 1
    :return: array with all intervals of 0 and 1
    '''

    # Get all values of actautor an timestamp
    conn = sqlite3.connect("usbmount/db_log.db")
    c = conn.cursor()
    out = []
    statement = "SELECT TIMESTAMP, VALUE FROM logger_simpleoutputlog WHERE name = " + "'" + actuator + "'" \
                + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    c.execute(statement)
    result = c.fetchall()
    for r in result:
        out.append(r, )

    intervals = []
    pointer = 1
    bedingung = True

    while bedingung:
        start_val = out[pointer][1]
        start_time = out[pointer][0]
        value = start_val
        i = 1
        while value == start_val:
            if pointer + i >= len(out):
                bedingung = False
                break
            value = out[pointer + i][1]
            i += 1

        # wenn in dem Intervall der Stellantrieb geschlossen war
        if start_val == 0:
            end_time = out[pointer + (i - 1)][0]
            start_time = out[pointer - 1][0]

        # wenn in dem Intervall der Stellantrieb geöffnet war
        else:
            end_time = out[pointer + (i - 2)][0]

        intervals.append([start_time, end_time, start_val])
        pointer = pointer + (i - 1)
        if pointer > len(out):
            bedingung = False
    c.close()
    conn.close()
    return intervals


def calc_new_rlt(intervals, rlt_identifier, it_identifier):
    """
        This method calculates the improved RLT for zero-intervals. First it takes the last correct value in red interval.
        Afterwards it takes the first correct value after der blue interval in a red interval. Then it calculates the mean.
        :param intervals: Calculated Intervals of actuator (calc in method calc_intervals)
        :param rlt_identifier: name of associated rlt-sensor in database
        :param it_identifier: name of associated it-sensor in database
        :return: The average value of rlt in the average interval with Correction.
    """

    # Array for storing rlt
    rlt = []

    conn = sqlite3.connect("usbmount/db_log.db")
    c = conn.cursor()

    for i in range(1, len(intervals) - 1):
        interval = intervals[i]
        interval_value = interval[2]
        interval_start = interval[0]
        interval_end = interval[1]

        # If value is 1, so actuator is open, add the temps in this value to rlt array
        if interval_value == 1:
            query_in = "SELECT VALUE FROM logger_simplesensorlog WHERE name=" + "'" + str(rlt_identifier) + "'" \
                       + "and TIMESTAMP >= " + "'" + interval_start + "'" + "and TIMESTAMP <=" + "'" + interval_end + "'"
            for row in c.execute(query_in):
                rlt.append(row[0])
            continue

        # If it is an closed interval
        elif interval_value == 0:
            # calculate the last valid value before the zero interval
            query_before = "SELECT TIMESTAMP, VALUE FROM logger_simplesensorlog WHERE name=" + \
                           "'" + str(rlt_identifier) + "'" + "and TIMESTAMP <= " + "'" + interval_start + "'"
            last = []
            for r in c.execute(query_before):
                last.append(r)
            value_before = last[len(last) - 1][1]
            # timestamp_before = last[len(last)-1][0]

            # calculate the first valid value after the zero interval
            query_next = "SELECT TIMESTAMP, VALUE FROM logger_simplesensorlog WHERE name=" + \
                         "'" + str(rlt_identifier) + "'" + "and TIMESTAMP >= " + "'" + interval_end + "'"
            next = c.execute(query_next).fetchone()
            # timestamp_next = next[0]
            value_next = next[1]

            # if the average temperature in the zero interval is lower than the first valid value, replace it
            query_it = "SELECT AVG(VALUE) FROM logger_simplesensorlog WHERE name=" + "'" + str(it_identifier) + "'" \
                       + "and TIMESTAMP >=" + "'" + interval_start + "'" + "and TIMESTAMP <= " + "'" + interval_end + "'"
            it_mean, = c.execute(query_it).fetchone()
            if it_mean > value_next:
                value_next = it_mean

            mittelwert = (value_next + value_before) / 2

            query_in = "SELECT TIMESTAMP, VALUE FROM logger_simplesensorlog WHERE name=" + "'" + str(
                rlt_identifier) + "'" \
                       + "and TIMESTAMP >= " + "'" + interval_start + "'" + "and TIMESTAMP <=" + "'" + interval_end + "'"
            for row in c.execute(query_in):
                rlt.append(mittelwert)
    c.close()
    conn.close()
    avg_rlt = sum(rlt) / len(rlt)
    return avg_rlt


def calc_avg_rlt(start, end, actuator, rlt_identifier, it_identifier):
    intervals = calc_intervals(start, end, actuator)
    avg_rlt = calc_new_rlt(intervals, rlt_identifier, it_identifier)
    return avg_rlt, intervals


def check_sensor(start, ende, sensor):
    conn = sqlite3.connect("usbmount/db_log.db")
    c = conn.cursor()
    minvalues = 4 * 60 * 60  # Definiere wie groß der Abstand maximal sein darf
    sql_statement_timestamp = "SELECT TIMESTAMP FROM logger_simplesensorlog WHERE name=" + "'" + str(sensor) + "'" \
                              + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + ende + "'"
    # values aus der Datenbank in ein Array
    values = c.execute(sql_statement_timestamp).fetchall()
    time_before = values[0][0]
    time_before = time_before[2:]
    time_before = datetime.strptime(time_before, '%y-%m-%d %H:%M:%S.%f')
    for time in values:
        time = time[0][2:]
        time = datetime.strptime(time, '%y-%m-%d %H:%M:%S.%f')
        delta = (time - time_before).total_seconds()
        if delta > minvalues:
            return False
        time_before = time
    c.close()
    conn.close()
    return True


def check_database(ende, intervall_start, t_intervall, rlt_identifier, it_identifier):
    '''
    This method proves if the database includes a every hour from start to end a value for rlt and it.
    This ensures that no data points exist twice in kilog_rated.
    This ensures that no data points are overestimated.
    '''
    # Hole die benötigten Daten aus der Datenbank
    conn = sqlite3.connect("usbmount/db_log.db")
    c = conn.cursor()
    start, = c.execute("SELECT MIN(TIMESTAMP) FROM logger_simplesensorlog").fetchone()

    c.close()
    conn.close()

    # start und ende in Zeiten umrechnen
    start_t = start[2:]
    ende_t = ende[2:]
    start_time = datetime.strptime(start_t, '%y-%m-%d %H:%M:%S.%f')
    end_time = datetime.strptime(ende_t, '%y-%m-%d %H:%M:%S.%f')
    length_database = (end_time - start_time).days

    # 1. Überprüfung: ist die Datenpank kürzer als das abgefragte Intervall?
    if length_database < t_intervall:
        return False

    # 2. Überpüfung: liegen Werte für jede Stunde vor?
    check_rlt = check_sensor(intervall_start, ende, rlt_identifier)
    if not check_rlt:
        return False
    check_it = check_sensor(intervall_start, ende, it_identifier)
    if not check_it:
        return False

    return True


def calc_datapoint(hausid, raumid, t_intervall):
    """
    This method first calculates the associated sensors for the given raumid.
    The average temperature values for the given time interval t_interval are being calculated.

    """
    try:
        it_identifier, rlt_identifier, raum = calc_sensoren(hausid, raumid)
    except:
        return

    dict_point = {'IT': None, 'AT': None, 'RLT': None, 'OUT': None}

    # Connect to Logging database
    conn = sqlite3.connect("usbmount/db_log.db")
    c = conn.cursor()

    # Take the last entry from the current logging db
    end, = c.execute("SELECT MAX(TIMESTAMP) FROM logger_simplesensorlog").fetchone()

    # Calculate the starting time depending on the given t_interval
    start = calc_start(end, t_intervall)

    # Überprüfe die Länge der Datenbank und die Anzahl der vorhandenen Werte
    try:
        valide = check_database(end, start, t_intervall, rlt_identifier, it_identifier)
        if not valide:
            return
    except:
        return

    # Calculation of the average inside temperature, outside temperature and return temperature
    sql_statement_it = "SELECT AVG(VALUE) FROM logger_simplesensorlog WHERE name=" + "'" + str(it_identifier) + "'" \
                       + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    it, = c.execute(sql_statement_it).fetchone()
    if it is None:
        return

    dict_point['IT'] = it

    sql_statement_at = "SELECT AVG(VALUE) FROM logger_simplesensorlog WHERE name=" + "'" + "averagetemp_raum_" + str(
        raumid) + "'" \
                       + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    at, = c.execute(sql_statement_at).fetchone()
    if at is None:
        return
    dict_point['AT'] = at

    # for test purposes only
    sql_statement_rlt = "SELECT AVG(VALUE) FROM logger_simplesensorlog WHERE name=" + "'" + str(rlt_identifier) + "'" \
                        + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    rlt, = c.execute(sql_statement_rlt).fetchone()
    rlt_alt = rlt

    # Calculate gateway and outputs of the room
    try:
        out = raum.regelung.get_ausgang()
        gateway_name = out.gateway.name
        ausgang = out.ausgang.split(',')
        ausgang = ausgang[0]
        actuator = str(gateway_name) + "_" + str(ausgang)
    except:
        return

    # Berechnung des Durchschnitts der korrigierten RLT
    try:
        rlt, intervals = calc_avg_rlt(start, end, actuator, rlt_identifier, it_identifier)
        dict_point['RLT'] = rlt
    except:
        dict_point['RLT'] = rlt_alt

    # Calculate the number in percent of how long the actuator was open in the time interval.
    statement_sum = "SELECT SUM(VALUE) FROM logger_simpleoutputlog WHERE name = " + "'" + actuator + "'" \
                    + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    sum, = c.execute(statement_sum).fetchone()

    statement_count = "SELECT COUNT(VALUE) FROM logger_simpleoutputlog WHERE name = " + "'" + actuator + "'" \
                      + "and TIMESTAMP >= " + "'" + start + "'" + " and TIMESTAMP <=" + "'" + end + "'"
    count, = c.execute(statement_count).fetchone()
    percent = float(sum) / float(count) * 100 if count != 0 else 0.0
    dict_point['OUT'] = percent

    c.close()
    conn.close()
    return dict_point, it_identifier, rlt_identifier, actuator, rlt_alt


def linear_regression(hausid, raumid):
    # Step 1: take the values from the kilog_rated.csv
    raumid = str(raumid)
    hausid = str(hausid)
    ergebnis = []
    try:
        with open('usbmount/kilog_rated_final.csv') as csvdatei:
            csv_reader_object = csv.reader(csvdatei, delimiter=",")
            for i, row in enumerate(csv_reader_object):
                if row[1] == str(hausid) and row[3] == str(raumid):
                    ergebnis.append(row[6])
    except:
        return 0.33, None

    # Step 2: calculate the points in this form: [[1,2], [2,3]]
    points = []
    for dict in ergebnis:
        dict = ast.literal_eval(dict)
        itat = dict["IT"] - dict["AT"]
        rltit = dict["RLT"] - dict["IT"]
        point = [itat, rltit]
        points.append(point)

    # Step 3: Calculate the straight line of origin with regression (y = mx)
    xy = [points[i][0] * points[i][1] for i in range(len(points))]
    sum_xy = sum(xy)
    x2 = [points[i][0] ** 2 for i in range(len(points))]
    sum_x2 = sum(x2)
    try:
        m = sum_xy / sum_x2
    except:
        m = 0.33
    return m


'''
def calc_rlsoll(raum, zielit, avg_at):
	"""
		This method calculates the "Rücklaufsolltemperatur" depending on the Zielinnentemperatur and the average "Außentemperatur"
		:param raum: room for which the "Rücklaufsolltemperatur" should be calculated
		:param zielit: the aim of the inside temperature of the room raum
		:param avg_at: average temperature outside
		:return: rlsoll
	"""
	hausid = raum.etage.haus.id
	raumid = raum.id
	m, points = linear_regression(hausid, raumid)
	# y = mx
	itminusat = zielit - avg_at
	rltminusit = m * itminusat
	rlsoll = rltminusit + zielit
	return rlsoll
'''


def valuation_ki(hausid, raumid):
    """
    This method calculates the advance of the KI for every room. The green line is the average
    ranking value. The blue line says something about the adaptation of the ki to the specific space.
    :param hausid: id of the house
    :param raumid:id of the room
    :return: length of the bar for the progress of the Ki
    """
    raumid = str(raumid)
    hausid = str(hausid)
    room_points = 0
    ranking_ges = 0
    anzahl_datenpunkte = 0
    try:
        with open('usbmount/kilog_rated_final.csv') as csvdatei:
            csv_reader_object = csv.reader(csvdatei, delimiter=",")
            for i, row in enumerate(csv_reader_object):
                if row[1] == str(hausid) and row[3] == str(raumid):
                    anzahl_datenpunkte = anzahl_datenpunkte + 1
                    ranking_ges += float(row[7])
                    if row[4] == str(raumid): #Achtung mit den Indizes
                        room_points += 1
    except:
        return 0, 0
    try:
        blue = room_points / anzahl_datenpunkte
    except:
        blue = 0
    blue_length = 0.25 * blue  # normal: 0.5 * blue
    try:
        avg_ranking = ranking_ges / anzahl_datenpunkte
    except:
        avg_ranking = 0
    green = avg_ranking + anzahl_datenpunkte
    green_length = (0.75 / 24) * green  # normal: 0.5 * blue

    return blue_length, green_length


def get_local_settings_page_haus(request, haus, action=None):
    if not isinstance(haus, Haus):
        try:
            haus = Haus.objects.get(pk=long(haus))
        except Haus.DoesNotExist:
            return HttpResponse("Error", status=400)

    if action == "plot":
        button = u""
        t_intervall = None
        raumid = None
        context = {'haus': haus, 'button': button, 't_intervall': t_intervall, 'raumid': raumid, 'plot_values': None,
                   'regression': None}
        if request.method == "GET":
            return render_response(request, 'm_ki_plot.html', context)

        elif request.method == "POST":
            # to test/plot linear_regression
            raum = request.POST['raum']
            haus = request.POST['haus']
            raum = int(raum)
            haus = int(haus)
            slope, plot_values = linear_regression(haus, raum)
            context['plot_values'] = plot_values
            context['regression_points'] = [[0, 0], [28, slope * 28]]  # den letzten Punkt nehmen
            context['plot'] = u"" + "Ergebniss: " + str(slope)

            # to test calc_datapoint
            t_intervall = request.POST['t_intervall']
            t_intervall = int(t_intervall)
            dict_point, it_identifier, rlt_identifier, actuator, rlt_alt = calc_datapoint(haus, raum, t_intervall)

            context['button'] = u"" + "Ergebniss: " + str(dict_point) + "  ,IT-Sensor: " + str(it_identifier) + \
                                "  ,RLT-Sensor: " + str(rlt_identifier) + "  ,Gateway/Ausgang: " + str(actuator) \
                                + "  ,RLT-ohne Korrektur: " + str(rlt_alt)
            return render_response(request, 'm_ki_plot.html', context)
