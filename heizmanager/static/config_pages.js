
let call_ajax = function(url, method, data,success_cb, err_cb){
    $.ajax({
        url: url,
        data: data,
        type: method,
        success: success_cb,
        error: err_cb
    });
}

let change_right_menu_indication = function(){
    let token = $('input[name=csrfmiddlewaretoken]').val();
    let url = window.location.path;

    let right_menu = 0;
    if ($('#right-menu-ch').attr("checked")){
        right_menu = 1;
    }

    let data = {
        'csrfmiddlewaretoken': token,
        'right_menu_ajax': right_menu
    }

    call_ajax(url, 'post', data,function () {}, function () {})

}

$(document).ready(function() {

    // right menu indication changing by chbox in module config main-pages
    $('.main-config-page #right-menu-ch').change(function(){
        change_right_menu_indication();
    })

})