# -*- coding: utf-8 -*-

from heizmanager.render import render_redirect, render_response
import importlib
import json

try:
    from heizmanager.modules.visible_modules import visible_modules
except ImportError:
    visible_modules = None

# define planning module for upcoming offsets
planning_modules = ['timer', 'jahreskalender', 'heizprogramm']


def get_mods_list(haus, just_activated=False, setting_link=False):
    mods = [('gatewaymonitor', 'Gateway Monitor'), ('betriebsarten', 'Betriebsarten'), ('wetter', 'Wetter'),
            ('gcal', 'Google Kalender'), ('raumgruppen', 'Raumgruppen'),
            ('zeitschalter', 'Zeitschalter'), ('hochtarifoptimierung', 'Hochtarifoptimierung'),
            ('ruecklaufregelung', u'Rücklaufregelung'), ('solarpuffer', 'Solarpuffer'),
            ('vsensoren', 'Virtuelle Sensoren'), ('pumpenlogik', 'Raumanforderung'),
            ('praesenzregelung', u'Präsenzregelung'), ('wochenkalender', 'Wochenkalender'),
            ('logger', 'Logging'), ('jahreskalender', 'Kalender'),
            ('vorlauftemperaturregelung', 'Vorlauftemperaturregelung'),
            ('differenzregelung', 'Differenzregelung'), ('luftfeuchte', 'Luftfeuchte'),
            ('geolocation', 'Geolocation'), ('aussentemperaturkorrektur', 'Aussentemperaturkorrektur'),
            ('backupcontrol', 'Backupcontrol'), ('timer', 'Timer'), ('logmonitor', 'Logmonitor'),
            ('heizflaechenoptimierung', u'Heizflächenoptimierung'), ('fps', 'FPS'),
            ('vorlauftemperaturkorrektur', 'Vorlauftemperaturkorrektur'), ('telegrambot', 'Chat-Bot'),
            ('temperaturszenen', 'Temperaturszenen'), ('kontaktabsenkung', 'Kontaktabsenkung'),
            ('heizprogramm', 'Heizprogramm'), ('fernzugriff', 'Fernzugriff'), ('wetter_pro', 'Wetter-Pro'),
            ('alexa_interface', 'Amazon Alexa'), ('homebridge', 'Apple Homekit'),
            ('aha', 'Automatischer hydraulischer Abgleich'),
            ('ki', u'Automatische Heizkurvenanpassung für Fußbodenheizung'),
            ('kaminofen', u'Kaminofen / zusätzliche Wärmequelle'),
            ('fenster_offen', u'Fenster-offen-Erkennung'), ('offsets', 'Offsets')]

    active = haus.get_modules()

    if just_activated:
        modset = set([m[0] for m in mods if m[0] in active])
    else:
        modset = set([m[0] for m in mods])

    if visible_modules is not None:
        modset &= set(visible_modules.split(','))

    swlinks = []
    for mod, modname in mods:
        if mod not in modset: continue
        try:
            module = importlib.import_module(mod.strip() + '.views')
            try:
                description, desc_link = module.get_global_description_link()
                link = None
                if setting_link:
                    link = module.get_global_settings_link(None, haus)
            except AttributeError:
                description = ''
                desc_link = None
            if (setting_link and link) or not setting_link:
                swlinks.append((mod, modname, description, desc_link, link))
        except KeyError:
            pass
        except ImportError:
            pass
        
    category_dict = {
        'kalender_und_time': {'name': u'Kalender und Timer', 'color': '#773F9B', 'order': 1,
                              'mod_list': [
                                  {'modname': 'gcal', 'activated': 'gcal' in active},
                                  {'modname': 'heizprogramm', 'r_menu': True,
                                   'activated': 'heizprogramm' in active},

                                  {'modname': 'jahreskalender', 'r_menu': True,
                                   'activated': 'jahreskalender' in active},

                                  {'modname': 'temperaturszenen', 'r_menu': True,
                                   'activated': 'temperaturszenen' in active},

                                  {'modname': 'timer', 'r_menu': True,
                                   'perm': ['A'], 'activated': 'timer' in active}
                              ]},

        'fussbodenheizung_optimierung': {'name': u'Fußbodenheizung-Optimierung', 'color': '#779A45', 'order': 2,
                                         'mod_list': [
                                             {'modname': 'aha', 'activated': 'aha' in active},
                                             {'modname': 'heizflaechenoptimierung', 'r_menu': True,
                                              'activated': 'heizflaechenoptimierung' in active},

                                             {'modname': 'wetter', 'r_menu': True, 'activated': 'wetter' in active},
                                             {'modname': 'kaminofen', 'activated': 'wetter' in active}
                                         ]},

        'support_und_nachvollziehbarkeit': {'name': u'Support und Nachvollziehbarkeit', 'color': '#A37512', 'order': 3,
                                            'mod_list': [
                                                {'modname': 'backupcontrol',
                                                 'activated': 'backupcontrol' in active},
                                                {'modname': 'gatewaymonitor',
                                                 'activated': 'gatewaymonitor' in active},
                                                {'modname': 'logmonitor', 'r_menu': True,
                                                 'activated': 'logmonitor' in active},
                                            ]},

        'korrektur_module': {'name': u'Korrektur-Module', 'color': '#CB0963', 'order': 4,
                             'mod_list': [
                                 {'modname': 'aussentemperaturkorrektur', 'r_menu': True,
                                  'activated': 'aussentemperaturkorrektur' in active},

                                 {'modname': 'geolocation', 'r_menu': True, 'activated': 'geolocation' in active},
                                 {'modname': 'kontaktabsenkung', 'activated': 'kontaktabsenkung' in active},
                                 {'modname': 'luftfeuchte', 'r_menu': True, 'activated': 'luftfeuchte' in active},
                                 {'modname': 'praesenzregelung', 'activated': 'praesenzregelung' in active},
                                 {'modname': 'offsets', 'activated': 'offsets' in active},
                                 {'modname': 'fenster_offen', 'activated': 'fenster-offen' in active}
                             ]},

        'spezialanwendungen': {'name': u'Spezialanwendungen', 'color': '#D783FF', 'order': 5,
                               'mod_list': [
                                   {'modname': 'betriebsarten', 'r_menu': True,
                                    'check_show_link': True,
                                    'activated': 'betriebsarten' in active},

                                   {'modname': 'hochtarifoptimierung',
                                    'activated': 'hochtarifoptimierung' in active},
                                   {'modname': 'raumgruppen', 'activated': 'raumgruppen' in active},
                                   {'modname': 'solarpuffer', 'activated': 'solarpuffer' in active},
                                   {'modname': 'wetter_pro', 'activated': 'wetter_pro' in active},
                                   {'modname': 'vsensoren', 'activated': 'vsensoren' in active}
                               ]},

        'zentrale_heiztechnik': {'name': u'Zentrale Heiztechnik', 'color': '#F9B233', 'order': 6,
                                 'mod_list': [
                                     {'modname': 'differenzregelung', 'activated': 'differenzregelung' in active},
                                     {'modname': 'fps', 'activated': 'fps' in active},
                                     {'modname': 'pumpenlogik', 'activated': 'pumpenlogik' in active},
                                     {'modname': 'vorlauftemperaturkorrektur', 'r_menu': True,
                                      'activated': 'vorlauftemperaturkorrektur' in active},

                                     {'modname': 'vorlauftemperaturregelung',
                                      'activated': 'vorlauftemperaturregelung' in active}
                                 ]},



        'visualisierung_und_system': {'name': u'Visualisierung und System', 'color': '#2489CE', 'order': 7,
                                      'mod_list': [
                                          {'modname': 'alexa_interface', 'activated': 'alexa_interface' in active},
                                          {'modname': 'homebridge', 'activated': 'homebridge' in active},
                                          {'modname': 'fernzugriff', 'activated': 'fernzugriff' in active},
                                          {'modname': 'logger', 'r_menu': True, 'activated': 'logger' in active},
                                          {'modname': 'telegrambot', 'activated': 'telegrambot' in active}
                                      ]},
    }

    return sorted(swlinks, key=lambda x: x[1]), category_dict


def get_global_settings_page(request, haus):
    if request.method == "GET":

        # nicht alle von unseren Modulen sind (de-)aktivierbar. Bei denen gehts dann nur um die Anzeige.
        active = haus.get_modules()
        swlinks, category_dict = get_mods_list(haus)
        phase_out = ['wochenkalender', 'zeitschalter', 'ruecklaufregelung']
        return render_response(request, "m_modules.html",
                               {"haus": haus, "active": active, "mods": swlinks,
                                "category_dict": category_dict.items(), 'cat': json.dumps(category_dict), "phaseout": phase_out})

    elif request.method == "POST":

        # was passiert, wenn ein modul deaktiviert wird?
        #  wetter, kalender, hto, rlr, sp: standard-deaktivierungsverhalten, module greifen nicht mehr in regelung ein
        #  pl: rückgabe von default wert
        #  vsensoren: nichts. [alternative: rueckgabe von backup-wert]
        #  2punkt: wird nur versteckt, hat sonst keinen einfluss.
        #  rg: nichts.

        active_mods = []
        for k, v in request.POST.items():
            if v == 'on':
                active_mods.append(k)
        haus.set_modules(active_mods)

        return render_redirect(request, "/config")
