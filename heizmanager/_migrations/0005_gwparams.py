# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import caching.base


class Migration(migrations.Migration):

    dependencies = [
        ('heizmanager', '0004_luftfeuchtigkeit'),
    ]

    operations = [
            migrations.AlterField(
                model_name='gateway',
                name='parameters',
                field=models.TextField(blank=True),
            ),
    ]
