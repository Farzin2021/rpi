from heizmanager.render import render_response
import logging
import requests
import json
import sys, traceback
from rpi.aeshelper import aes_encrypt
from fabric.api import local
from datetime import datetime
from celery import Task
import heizmanager.network_helper as nh


class CeleryErrorLoggingTask(Task):
    def on_failure(self, exc, task_id, args, kwargs, einfo):
        try:
            typ, value, trace = sys.exc_info()
            branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
            res = local("git log -1 --date=raw", capture=True)
            luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
            lu = datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
            ret = {
                'exc_type': typ.__name__,
                'exc_msg': 'celery_exc',
                'exc_value': str(value).translate(None, "'").translate(None, '"'),
                'exc_trace': traceback.extract_tb(trace),
                'branch': branch,
                'rev': lu
            }
            macaddr = nh.get_mac()
            requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr,
                          data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=5)
        except Exception:
            pass

        super(CeleryErrorLoggingTask, self).on_failure(exc, task_id, args, kwargs, einfo)


def error500(request):

    try:
        typ, value, trace = sys.exc_info()
        branch = local("git rev-parse --abbrev-ref HEAD", capture=True)
        res = local("git log -1 --date=raw", capture=True)
        luiso = [r[8:-6] for r in res.split('\n') if r.startswith("Date: ")][0]
        lu = datetime.utcfromtimestamp(long(luiso)).strftime('%d.%m.%Y %H:%M:%S')
        ret = {
            'get': request.GET,
            'post': request.POST,
            'path': request.path,
            'exc_type': typ.__name__,
            'exc_msg': typ.__doc__,
            'exc_value': str(value).translate(None, "'").translate(None, '"'),
            'exc_trace': traceback.extract_tb(trace),
            'branch': branch,
            'rev': lu
        }
        macaddr = nh.get_mac()
        requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)), timeout=5)

        if ret['exc_value'] == "database is locked":
            try:
                # nochmal checken, ob wirklich was da ist, damit wir nicht ganz wahllos abschiessen
                fuser = local("fuser /home/pi/rpi/db.sqlite3", capture=True)
            except:
                pass  # leeres resultat
            else:
                local("fuser -k /home/pi/rpi/db.sqlite3")

        elif ret['exc_value'].startswith("no such column: knx"):
            try:
                local("rm /home/pi/rpi/knx/migrations/*")
            except:
                try:
                    local("mkdir /home/pi/rpi/knx/migrations/")
                except:
                    pass
            try:
                local("cp /home/pi/rpi/knx/_migrations/* /home/pi/rpi/knx/migrations/")
                local("python /home/pi/rpi/manage.py migrate knx")
            except: 
                pass
    except:
        pass

    return render_response(request, "m_error.html", {'e': 'Ein unvorhergesehenes Problem ist aufgetreten.'})


def error404(request):

    # try:
    #     macaddr = '-'.join(("%012x" % getnode())[i:i+2] for i in range(0, 12, 2))
    #     ret = {
    #         'post': request.POST, 'get': request.GET, 'path': request.path
    #     }
    #     requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)))
    # except:
    #     pass

    return render_response(request, "m_error.html", {'e': 'Seite konnte nicht gefunden werden.'})
