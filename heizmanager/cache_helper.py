from django.core.cache import cache as memcache
import pytz
from datetime import datetime, timedelta
import __builtin__


def get(key):
    return memcache.get(key)


def set(key, value, time=86000):
    return memcache.set(key, value, timeout=time)


def delete(key):
    return memcache.delete(key)


def update_gateway(macaddr):
    if memcache.get('upd%s' % macaddr.lower()):
        return True
    return False


def del_gateway_update(macaddr):
    memcache.delete('upd%s' % macaddr.lower())
    return True


def set_gateway_update(macaddr):
    set('upd%s' % macaddr.lower(), 1)
    return True


def update_db(hausid):
    if memcache.get('upddb%s' % hausid):
        return True
    return False


def del_update_db(hausid):
    memcache.delete('upddb%s' % hausid)
    return True


def set_update_db(hausid):
    set('upddb%s' % hausid, 1)
    return True


def update_db_user(hausid):
    if memcache.get('upddbuser%s' % hausid):
        return True
    return False


def del_update_db_user(hausid):
    memcache.delete('upddbuser%s' % hausid)
    return True


def set_update_db_user(hausid):
    set('upddbuser%s' % hausid, 1)
    return True


def set_new_temp(ssn, ist, hausid, sensor_offset=0.0, update_time=True):
    berlin = pytz.timezone('Europe/Berlin')
    tstamp = datetime.now(berlin)

    last = get_last_temp(ssn)
    if update_time:
        ltstamp = None
        if last:
            try:
                ltstamp = last[1]
            except (TypeError, KeyError):
                pass
    else:
        if last:
            try:
                tstamp = last[1]
                ltstamp = last[3]
            except TypeError:
                ltstamp = None
        else:
            ltstamp = None

    # sensor_offset wird immer auf IST draufgerechnet. kann mit last[4] wieder zurueckgerechnet werden.
    try:
        set(key=ssn.lower(), value=(float(ist)+sensor_offset, tstamp, hausid, ltstamp, sensor_offset))
    except TypeError:
        # kann auftreten aus uwsgi fork von tset raus
        return False
    return True


def get_last_temp(ssn):
    if ssn[:2] == "00":
        try:
            val = float(ssn[-5:].replace('_', '.'))
            if ssn[-8:-6].lower() == 'ff':
                val = -val
        except:
            val = None
        berlin = pytz.timezone("Europe/Berlin")
        return val, datetime.now(berlin)-timedelta(seconds=10), 1, datetime.now(berlin)-timedelta(seconds=60), 0.0
    return memcache.get(ssn.lower().strip())


def set_new_vals(ssn, vals, hausid):
    berlin = pytz.timezone('Europe/Berlin')
    tstamp = datetime.now(berlin)

    last = get_last_vals(ssn)
    ltstamp = None
    if last:
        try:
            ltstamp = last[1]
        except TypeError:
            ltstamp = None

    set(key=ssn.lower(), value=(vals, tstamp, hausid, ltstamp))
    return True


def get_last_vals(ssn):
    return memcache.get(ssn.lower())


def set_gw_ping(mac):
    berlin = pytz.timezone('Europe/Berlin')
    tstamp = datetime.now(berlin)

    last = get_gw_ping(mac)
    try:
        ltstamp = last[0]
    except (TypeError, KeyError):
        ltstamp = None

    set(key='getping_'+mac.lower(), value=(tstamp, ltstamp))
    return True


def get_gw_ping(mac):
    return memcache.get('getping_'+mac.lower())


def set_module_offset_raum(module, hausid, raumid, offset, ttl=600):
    set('%s_offset_%s_%s' % (module, str(hausid), str(raumid)), offset, ttl)
    return True


def get_module_offset_raum(module, hausid, raumid):
    return memcache.get('%s_offset_%s_%s' % (module, str(hausid), str(raumid)))


def set_module_offset_regelung(module, hausid, regelung, regelung_id, offset, ttl=600):
    set("%s_offset_%s_%s_%s" % (module, str(hausid), regelung, str(regelung_id)), offset, ttl)
    return True


def get_module_offset_regelung(module, hausid, regelung, regelung_id):
    return memcache.get("%s_offset_%s_%s_%s" % (module, str(hausid), regelung, str(regelung_id)))


def delete_module_offset_raum(module, hausid, raumid):
    memcache.delete('%s_offset_%s_%s' % (module, str(hausid), str(raumid)))
    return True  # fehlerbehandlung?


def set_cache_upcoming_events(hausid, module, entity_id, entity_type, offset, ttl=64800):
    # default of ttl is 12 lookahead + 6h longer
    set("%s_upcoming_offsets_%s_%s_%s" % (module, hausid, entity_id, entity_type), offset, ttl)
    return True


def get_cache_upcoming_events(hausid, module, entity_id, entity_type):
    return memcache.get("%s_upcoming_offsets_%s_%s_%s" % (module, hausid, entity_id, entity_type))


def delete_cache_upcoming_events(hausid, module, entity_id, entity_type):
    memcache.delete("%s_upcoming_offsets_%s_%s_%s" % (module, hausid, entity_id, entity_type))
    memcache.delete("%s_upcoming_lookedahead_%s_%s" % (hausid, entity_type, entity_id))
    return True


def set_module_offset_haus(module, hausid, offset, ttl=600):
    set('%s_offset_%s' % (module, str(hausid)), offset, ttl)
    return True


def get_module_offset_haus(module, hausid):
    return memcache.get('%s_offset_%s' % (module, str(hausid)))


def delete_module_offset_haus(module, hausid):
    memcache.delete('%s_offset_%s' % (module, str(hausid)))
    return True  # fehlerbehandlung?


def get_room_logs(raumid):
    return memcache.get('roomlog%s' % str(raumid))


def set_room_logs(raumid, logs):
    set('roomlog%s' % str(raumid), logs)
    return True


def set_nonce(mac, nonce):
    n = memcache.get("nonce_%s" % mac)
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    if n:
        for _n, _t in n:
            if (now-_t).seconds > 600:
                n.remove((_n, _t))
        n.append((nonce, now))
    else:
        n = [(nonce, now)]
    set("nonce_%s" % mac, n)
    return True


def verify_nonce(mac, nonce):
    n = memcache.get("nonce_%s" % mac)
    if n:
        for _n, _t in n:
            if _n == nonce:
                n.remove((_n, _t))
                set("nonce_%s" % mac, n)
                return True
        else:
            return False
    else:
        return False


def set_usage_val(key, val):

    vals = memcache.get("usage_data")
    if vals is None or not isinstance(vals, dict):
        vals = {}

    if key in ['raum_get', 'raum_post']:
        vals.setdefault(key, {val})
        vals[key].add(val)

    elif key in ['sensorenuebersicht', 'raumliste']:
        vals[key] = True

    elif key in ['raumliste_quick_szs', 'raumliste_quick_srt', 'raumliste_quick_sat', 'raumliste_quick']:
        vals.setdefault(key, 0)
        vals[key] += 1

    elif key in ['glsph_get', 'glsph_post']:
        vals.setdefault(key, {val})
        vals[key].add(val)

    elif key.startswith('glspr_'):
        vals.setdefault(key, {val})
        vals[key].add(val)

    elif key in ['offsets']:
        vals.setdefault(key, dict())
        for mod, offset in val.items():
            vals[key].setdefault(mod, __builtin__.set())
            vals[key][mod].add(offset)

    elif key in ['screensize']:
        vals.setdefault(key, {val})
        vals[key].add(val)

    elif key in ['render_response', 'render_redirect']:
        vals.setdefault('crumbs', list())
        _v = "%s %s %s" % (key[7:], val[0], val[1])
        if _v not in vals['crumbs']:
            vals['crumbs'].append(_v)

    elif key in {'proki_score', 'ki_score', 'active_ki'}:
        vals.setdefault(key, val)
        vals[key].update(val)

    memcache.set("usage_data", vals, 600)
    return True


def set_module_tasks(hausid, module_name, tasks_list, tm=60 * 60 * 24):
    memcache.set("tasks_%s_%s" % (hausid, module_name), tasks_list, tm)
    return True


def get_module_tasks(hausid, module_name):
    tasks_list = get("tasks_%s_%s" % (hausid, module_name))
    return tasks_list if tasks_list is not None else list()


def delete_module_tasks(hausid, module_name):
    delete("tasks_%s_%s" % (hausid, module_name))
