# -*- coding: utf-8 -*-

from django.db import models
from fields import ListField
from django.contrib.auth.models import User
from django.db.models.signals import post_init, post_delete, post_save, pre_save
import django.dispatch as dispatch
import cache_helper as ch
import network_helper as nh
from datetime import datetime, timedelta
import pytz
import re, ast
import logging
from caching.base import CachingManager, CachingMixin
import json
import traceback
from rpi.aeshelper import aes_encrypt
import requests
from benutzerverwaltung.decorators import perm_req
import math


try:
    from Crypto.PublicKey import RSA
    from Crypto.Cipher import PKCS1_OAEP, AES
    from Crypto import Random
except ImportError:
    logging.error("couldn't import pycrypto")


class Haus(CachingMixin, models.Model):
    name = models.CharField(max_length=60, blank=False)
    eigentuemer = models.ForeignKey(User, null=True)  # eigentuemer soll auch in besitzer sein
    besitzer = ListField(models.ForeignKey(User), default=[])
    
    objects = CachingManager()

    def save(self, *args, **kwargs):
        #if not self.pk:
        #    self.besitzer = []
        if self.eigentuemer_id and self.eigentuemer_id not in self.besitzer:
            self.besitzer.append(self.eigentuemer_id)
        super(Haus, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        for etage in Etage.objects.filter(haus=self.pk):
            etage.delete()
        for gateway in Gateway.objects.filter(haus=self.pk):
            for sensor in gateway.sensoren.all():
                sensor.delete()
            gateway.delete()
        HausProfil.objects.get(haus=self).delete()
        Haus.objects.filter(pk=self.pk).delete()
    
    def get_modules(self):
        return [m.strip() for m in HausProfil.objects.get(haus=self).modules.split(',') if len(m)]
    
    def set_modules(self, modules):
        profil = HausProfil.objects.get(haus=self)
        profil.modules = ','.join(modules)
        profil.save()
        return
    
    def get_module_parameters(self):
        return ast.literal_eval(HausProfil.objects.get(haus=self).module_parameters)
    
    def set_module_parameters(self, params):
        profil = HausProfil.objects.get(haus=self)
        profil.module_parameters = str(params)
        profil.save()
        return

    def get_spec_module_params(self, module_name):
        trans_name = {
            'heizprogramm': 'switchingmode',
        }
        h_params = self.get_module_parameters()
        module_params = h_params.get(trans_name.get(module_name, module_name), dict())
        return module_params

    def set_spec_module_params(self, module_name, params):
        profile = HausProfil.objects.get(haus=self)
        h_params = ast.literal_eval(profile.module_parameters)
        h_params[module_name] = params
        profile.module_parameters = str(h_params)
        profile.save()
        return True

    def show_config(self):
        params = self.get_module_parameters()
        return params.get('show_config', 1)

    class Meta:
        app_label = 'heizmanager'


class HausProfil(CachingMixin, models.Model):
    haus = models.ForeignKey(Haus, related_name='profil')
    install_mode = models.BooleanField(default=False)
    
    modules = models.TextField(blank=True)
    module_parameters = models.TextField(blank=True)

    objects = CachingManager()

    def save(self, *args, **kwargs):
        super(HausProfil, self).save(*args, **kwargs)

    def get_modules(self):
        return self.modules.split(',')
    
    def set_modules(self, modules):
        self.modules = ','.join(modules)
        self.save()
        return

    def get_module_parameters(self):
        if self.module_parameters:
            return ast.literal_eval(self.module_parameters)
        else:
            return {}

    def set_module_parameters(self, params):
        self.module_parameters = str(params)
        self.save()
        return

    def get_gateway_delay(self):
        params = ast.literal_eval(self.module_parameters)
        if 'gwdelay' in params:
            return params['gwdelay']
        else:
            self.set_gateway_delay(90)
            return 90

    def set_gateway_delay(self, delay):
        params = ast.literal_eval(self.module_parameters)
        params['gwdelay'] = delay
        self.module_parameters = str(params)
        self.save()
        return True

    def get_gateway_invertouts(self, macaddr):
        params = ast.literal_eval(self.module_parameters)
        # if 'invertouts' in params:
        #     return params['invertouts'].get(macaddr, False)
        # else:
        #     return False
        return params.get('invertouts', dict()).get(macaddr, False)

    def set_gateway_invertouts(self, macaddr, invert):
        params = ast.literal_eval(self.module_parameters)
        params.setdefault('invertouts', dict())
        params['invertouts'][macaddr] = invert
        self.module_parameters = str(params)
        self.save()
        return True

    def get_safe_mode(self):
        # setzt, ob (Schalt-)GW Noncen verifizieren sollen
        params = ast.literal_eval(self.module_parameters)
        safe_mode = params.get("safe_mode", False)
        return safe_mode

    def set_safe_mode(self, safe_mode):
        if isinstance(safe_mode, bool):
            params = ast.literal_eval(self.module_parameters)
            params['safe_mode'] = safe_mode
            self.set_module_parameters(params)
        return

    def get_sensortest(self):
        params = ast.literal_eval(self.module_parameters)
        return params.get("sensortest", False)

    def set_sensortest(self, active):
        if isinstance(active, bool):
            params = ast.literal_eval(self.module_parameters)
            params['sensortest'] = active
            self.set_module_parameters(params)

    def get_address(self):
        params = self.get_module_parameters()
        str = params.get('address', dict()).get('str', '')
        nr = params.get('address', dict()).get('nr', '')
        plz = params.get('address', dict()).get('plz', '')
        ort = params.get('address', dict()).get('ort', '')
        return str, nr, plz, ort

    def set_address(self, str, nr, plz, ort):
        params = self.get_module_parameters()
        params.setdefault('address', dict())
        params['address']['str'] = str
        params['address']['nr'] = nr
        params['address']['plz'] = plz
        params['address']['ort'] = ort
        self.set_module_parameters(params)
        return True

    def get_vorlauftemperaturkorrektur_params(self):
        params = self.get_module_parameters()
        try:
            return float(params['vorlauftemperaturkorrektur']['increase']), float(params['vorlauftemperaturkorrektur']['decrease'])
        except KeyError:
            # params nicht gesetzt
            return 1, 1

    class Meta:
        app_label = 'heizmanager'


class Wetter(CachingMixin, models.Model):
    haus = models.ForeignKey(Haus)
    data = models.TextField()
    timestamp = models.DateField(auto_now=True)
    
    objects = CachingManager()

    def get_data(self):
        return ast.literal_eval(self.data)

    def set_data(self, data):
        if data is None:
            data = []
        self.data = str(data)
        self.save()
        return True

    class Meta:
        app_label = 'heizmanager'


class CryptKeys(CachingMixin, models.Model):
    aeskey = models.CharField(blank=True, null=True, max_length=32)
    
    objects = CachingManager()

    def create_key(self):
        import base64

        try:
            key = Random.new().read(32)
        except NameError:  # Random import oben fehlgeschlagen
            from os import urandom
            key = urandom(32)

        self.aeskey = base64.b64encode(key)
        self.save()

    def encrypt(self, data):
        import base64

        iv = Random.new().read(AES.block_size)
        aes = AES.new(base64.b64decode(self.aeskey), AES.MODE_CBC, iv)
        out = iv + aes.encrypt(data + Random.new().read(-len(data) % 16).encode('hex')[::2])

        return '~' + base64.b64encode(out)

    def decrypt(self, data):
        import base64
        data = base64.b64decode(data)
        iv = data[:16]

        aes = AES.new(base64.b64decode(self.aeskey), AES.MODE_CBC, iv)
        data = aes.decrypt(data[16:])

        return data

    class Meta:
        app_label = 'heizmanager'


class AbstractDevice(CachingMixin, models.Model):

    haus = models.ForeignKey(Haus, related_name="%(class)s_related", null=True)
    name = models.CharField(max_length=64)
    description = models.CharField(max_length=64)
    
    class Meta:
        app_label = 'heizmanager'
        abstract = True


class RPi(AbstractDevice):
    crypt_keys = models.ForeignKey(CryptKeys, null=True)
    verification_code = models.CharField(max_length=16)
    upgrade_version = models.CharField(max_length=128, blank=True)  # ein git sha1

    local_address = models.CharField(max_length=64)  # rpi_xy.local
    global_address = models.CharField(max_length=64)  # dyndns
    local_ip = models.GenericIPAddressField(null=True)

    autark = models.BooleanField(default=False)

    objects = CachingManager()

    def do_upgrade(self):
        if len(self.upgrade_version):
            return True
        else:
            return False

    def get_identifier(self):
        return "%s*%s" % (type(self).__name__, self.id)

    def get_marker(self):
        berlin = pytz.timezone('Europe/Berlin')
        now_tstamp = datetime.now(berlin)
        last = ch.get_gw_ping(self.name.lower())
        marker = 'red'

        if last and last[0] > (now_tstamp - timedelta(minutes=30)):
            marker = 'green'

        return marker

    class Meta:
        app_label = 'heizmanager'


class Etage(CachingMixin, models.Model):
    name = models.CharField(max_length=30, blank=False)
    haus = models.ForeignKey(Haus, related_name='etagen')
    position = models.IntegerField()

    objects = CachingManager()

    def __unicode__(self):
        return self.name

    def delete(self, *args, **kwargs):
        for raum in self.raeume.all():
            raum.delete()
        for etage in self.haus.etagen.all():
            if etage.position > self.position:
                etage.position -= 1
                etage.save()
        Etage.objects.filter(pk=self.pk).delete()

    def save(self, *args, **kwargs):
        super(Etage, self).save(*args, **kwargs)

    class Meta:
        app_label = 'heizmanager'
        ordering = ['position']


class RegelungManager(CachingManager):

    @perm_req('get')
    def get_for_user(self, *args, **kwargs):
        pass

    @perm_req('all')
    def all_for_user(self, *args, **kwargs):
        # return self.get_queryset()
        pass

    @perm_req('filter')
    def filter_for_user(self, *args, **kwargs):
        # qs = self.get_queryset()
        # return qs.filter(**kwargs)
        pass

    def get_queryset(self):
        # import inspect
        # logging.warning(inspect.stack()[1])
        return super(RegelungManager, self).get_queryset()


class Regelung(CachingMixin, models.Model):
    regelung = models.CharField(max_length=63)
    parameter = models.TextField(blank=True)
    
    objects = RegelungManager()

    def __unicode__(self):
        return self.regelung

    def _get_reg(self):
        import importlib
        reg = None
        try:
            reg = importlib.import_module('heizmanager.modules.'+self.regelung)
        except ImportError:
            pass
        try:
            reg = importlib.import_module(self.regelung + '.views')
        except ImportError:
            pass
        if not reg:
            return None
        return reg

    def do_ausgang(self, ausgang):
        reg = self._get_reg()
        if not reg:
            return None
        return reg.do_ausgang(self, ausgang)

    def do_rfausgang(self, controllerid):
        reg = self._get_reg()
        if not reg:
            return None
        return reg.do_rfausgang(self, controllerid)

    def get_local_link(self, request, raum):
        reg = self._get_reg()
        if not reg:
            return None
        return reg.get_local_link(self, request, raum)

    def get_local_page(self, request, raum):
        reg = self._get_reg()
        if not reg:
            return None
        return reg.get_local_page(self, request, raum)

    def get_local_html(self, request, raum):
        import importlib
        reg = importlib.import_module(self.regelung + '.views')
        return reg.get_local_html(self, request, raum)

    def get_raum(self):
        try:
            raumid = self.raum.id
            raum = Raum.objects.get_for_user(User.objects.get(pk=self.raum.etage.haus.eigentuemer_id), pk=raumid)  # because fuck you, that's why
            return raum
        except:
            return None

    def get_ausgang(self):
        try:
            ausgangid = self.gatewayausgang.id
            ausgang = GatewayAusgang.objects.get(pk=ausgangid)
        except (GatewayAusgang.DoesNotExist):
            return None
        return ausgang

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameter)
        except (ValueError, SyntaxError):
            return {}

    def set_parameters(self, params):
        self.parameter = str(params)
        r = self.get_raum()
        if r:
            ch.delete("%s_roffsets_dict" % r.id)
        self.save()

    def get_wetter_params(self, haus):
        objid = self.get_parameters().values()[0]
        params = haus.get_module_parameters().get(self.regelung, {}).get(objid,
                                                    {"0": {}}).get("module_params", {"module_params": {}})
        return params.get('wetter',
                          {'warm': 5, 'kalt': 5})

    def get_atkorrektur_params(self, haus):
        objid = self.get_parameters().values()[0]
        params = haus.get_module_parameters().get(self.regelung, {}).get(objid,
                    {"0": {}}).get("module_params", {"module_params": {}})

        return params.get('aussentemperaturkorrektur',
                          {'start_time': 0, 'end_time': 5, 'heating_element_end_1': 0, 'heating_element_end_2': 0})

    def save(self, *args, **kwargs):
        ## wenn self.raum und self.ausgang nicht existieren, loeschen.
        ## das gleiche auch bei raum.save() und ausgang.save()?
        super(Regelung, self).save(*args, **kwargs)
    
    class Meta:
        app_label = 'heizmanager'


class RaumManager(CachingManager):

    @perm_req('get')
    def get_for_user(self, *args, **kwargs):
        pass

    @perm_req('all')
    def all_for_user(self, *args, **kwargs):
        # return self.get_queryset()
        pass

    @perm_req('filter')
    def filter_for_user(self, *args, **kwargs):
        # qs = self.get_queryset()
        # return qs.filter(**kwargs)
        pass

    def get_queryset(self):
        # import inspect
        # logging.warning(inspect.stack()[1])
        return super(RaumManager, self).get_queryset()

    #def all(self, *args, **kwargs):
    #    return self.none()

    # def filter(self, *args, **kwargs):
    #     return self.none()

    # def get(self, *args, **kwargs):
    #     return self.none()


class Raum(CachingMixin, models.Model):
    name = models.CharField(max_length=63, blank=False)
    etage = models.ForeignKey(Etage, related_name='raeume')
    solltemp = models.FloatField(null=True)
    isttemp = models.FloatField(null=True)
    position = models.IntegerField()
    icon = models.CharField(max_length=30, blank=True)
    
    regelung = models.OneToOneField(Regelung, null=True, related_name='raum')
    
    modules = models.TextField(blank=True)
    module_parameters = models.TextField(blank=True)

    # objects = CachingManager()
    objects = RaumManager()

    def __init__(self, *args, **kwargs):
        self._sensoren = list()
        super(Raum, self).__init__(*args, **kwargs)
        self._original_soll = self.solltemp

    def __unicode__(self):
        return self.name

    def delete(self, *args, **kwargs):
        for sensor in self.get_sensoren():
            sensor.raum = None
            sensor.mainsensor = False
            sensor.save()
        for raum in self.etage.raeume.all():
            if raum.position > self.position:
                raum.position -= 1
                raum.save()
        try:
            self.regelung.gatewayausgang.delete()
            self.regelung.delete()
        except GatewayAusgang.DoesNotExist:
            self.regelung.delete()
        except Regelung.DoesNotExist:  # warum auch immer
            pass
        Raum.objects.filter_for_user(User.objects.get(pk=self.etage.haus.eigentuemer_id), pk=self.pk).delete()

    def save(self, *args, **kwargs):
        try:
            self.solltemp = float(self.solltemp)
            if math.isnan(self.solltemp):
                raise Exception("nan")
        except:
            return
        if self.solltemp != self._original_soll and self.pk:
            # das hier erzeugt quasi eine race condition, wenn es in einem sig_solltemp_change einen empfaenger gibt, der auf die db zugreift und dadurch das ungespeicherte objekt (lazy) neu geladen wird
            # ggf diesen check hier nach unten schieb, *nachdem* gespeichert wurde
            import copy
            _r = copy.deepcopy(self)  # damit self nicht im db cache ueberschrieben wird
            sig_solltemp_change.send(sender=Raum, haus=self.etage.haus, raum=_r)
            ch.set("%s_roffsets_dict" % self.id, None)
            ch.set("010v_hfo_timer_raum_%s" % self.id, None)
        super(Raum, self).save(*args, **kwargs)
        self._original_soll = self.solltemp

    def get_sensoren(self):
        return self._sensoren

    def get_sensor_overview(self):
        sensoren = []
        from django.utils import timezone
        berlin = timezone.get_current_timezone()
        for sensor in self.get_sensoren():
            last = sensor.get_wert()
            if last:
                last = [l for l in last]
                last[1] = last[1].replace(tzinfo=berlin)
            marker = sensor.get_marker()
            sensoren.append((sensor, last, marker))
        return sensoren

    def get_humidity(self):
        sensoren = sorted(self.get_sensoren())
        has_humidity = False
        for s in sensoren:
            if s.get_type() in {'RFSensor', 'Luftfeuchtigkeitssensor'}:
                wert = s.get_wert(val="Relative Humidity")
                if wert:
                    return wert
                else:
                    has_humidity = True
        if has_humidity:
            return (-1,)
        return None

    def get_humidity_params(self):
        params = self.get_module_parameters()
        return params.get('luftfeuchte', {'initial': 50, 'intensity': 0.01})

    def set_humidity_params(self, initial=None, intensity=None):
        params = self.get_module_parameters()
        params.setdefault('luftfeuchte', {'initial': 50, 'intensity': 0.01})
        if initial is not None:
            params['luftfeuchte']['initial'] = initial
        if intensity is not None:
            params['luftfeuchte']['intensity'] = intensity
        self.set_module_parameters(params)

    def get_wetter_params(self):
        params = self.get_module_parameters()
        try:
            return params['wetter']['wetter_params'].split(',')[0], params['wetter']['wetter_params'].split(',')[1]
        except KeyError:
            # params nicht gesetzt
            return 5, 5

    def get_betriebsart(self):
        if 'betriebsarten' not in self.etage.haus.get_modules():
            return '2'
        try:
            params = self.get_module_parameters()
            return params['betriebsarten']["%s" % self.id]
        except Exception:       
            return '2' # heat

    def set_betriebsart(self, mode):    # 0 = off, 1 = cool, 2 = heat
        try:
            params = self.get_module_parameters()
            params.setdefault('betriebsarten', dict())
            params['betriebsarten'][str(self.id)] = mode
            self.set_module_parameters(params)
            return 1
        except Exception as e:
            return 0

    def get_atkorrektur_params(self):
        params = self.get_module_parameters()
        return params.get('aussentemperaturkorrektur',
                          {'start_time': 0, 'end_time': 5, 'heating_element_end_1': 0, 'heating_element_end_2': 0})

    def get_hfo_params(self):
        params = self.get_module_parameters()
        return params.get('heizflaechenoptimierung',
                          {'time_start': 0, 'time_end': 5, 'heating_element_start': 22, 'heating_element_end': 40,
                           'before': -0.5, 'after': 0.5, 'flushing_interval_start': 60, 'flushing_interval_end': 300,
                           'correction_heating_element': 3})

    def get_vorlauftemperaturkorrektur_params(self):
        params = self.get_module_parameters()
        try:
            return float(params['vorlauftemperaturkorrektur']['increase']), float(params['vorlauftemperaturkorrektur']['decrease'])
        except KeyError:
            # params nicht gesetzt
            return 1, 0

    def get_alarm_setting(self):
        params = self.get_module_parameters()
        return params.get('alarm', {'notify': False})

    def set_alarm_setting(self, alarmparams):
        params = self.get_module_parameters()
        params['alarm'] = alarmparams
        self.set_module_parameters(params)
        return

    def get_mainsensor(self):
        s = None
        for sensor in self.get_sensoren():
            if sensor.mainsensor:
                s = sensor
        return s

    def set_mainsensor(self, sensor):
        # setzt alle anderen Sensoren ausser sensor auf .mainsensor=None.
        # Falls sensor=None, kein Hauptsensor fuer diesen Raum
        for _sensor in self.get_sensoren():
            if sensor is None or (_sensor.id != sensor.id and _sensor.mainsensor):
                _sensor.mainsensor = False
                _sensor.save()
        if sensor:
            sensor.mainsensor = True
            sensor.save()
        params = self.get_module_parameters()
        params['ms'] = None if sensor is None else sensor.get_identifier()
        self.set_module_parameters(params)

    def get_modules(self):
        return self.modules.split(',')
    
    def set_modules(self, modules):
        self.modules = ','.join(modules)
        self.save()
        ch.delete("%s_roffsets_dict" % self.id)
        return
    
    def get_module_parameters(self):
        return ast.literal_eval(self.module_parameters)
    
    def set_module_parameters(self, params):
        self.module_parameters = str(params)
        self.save()
        return

    def get_spec_module_params(self, module_name):
        r_params = self.get_module_parameters()
        module_params = r_params.get(module_name, dict())
        return module_params

    def set_spec_module_params(self, module_name, params):
        r_params = self.get_module_parameters()
        r_params[module_name] = params
        self.set_module_parameters(r_params)
        return True

    def get_regelung(self):
        return self.regelung

    class Meta:
        app_label = 'heizmanager'
        ordering = ['position']


class AbstractOutput(CachingMixin, models.Model):

    regelung = models.OneToOneField(Regelung, null=True, related_name='%(class)s')

    def get_ctrldevice(self):
        raise NotImplementedError()

    class Meta:
        app_label = 'heizmanager'
        abstract = True


class Gateway(AbstractDevice):
    version = models.CharField(max_length=8, default="nix")

    rpi = models.ForeignKey(RPi, null=True)
    crypt_keys = models.ForeignKey(CryptKeys, null=True)

    parameters = models.TextField(blank=True)

    objects = CachingManager()

    def __init__(self, *args, **kwargs):
        self._sensoren = list()
        super(Gateway, self).__init__(*args, **kwargs)

    def __unicode__(self):
        return self.description or self.name

    def save(self, *args, **kwargs):
        m = re.match(r'^([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2})$', self.name)
        if not m:
            raise ValueError('Ung&uuml;ltige MAC Adresse.')
        gateways = Gateway.objects.filter(name=self.name)
        for gateway in gateways:
            if gateway.id != self.pk:
                raise ValueError('MAC Adresse bereits in Verwendung.')
        super(Gateway, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        for sensor in Sensor.objects.filter(gateway=self.pk):  # das ist etwas aergerlich
            if "ui" in sensor.name:
                sensor.delete()  # kann in fps einen fehler produzieren
            sensor.gateway = None
            sensor.save()
        for aausgang in GatewayAusgang.objects.filter(gateway=self.pk):
            aausgang.delete()
        Gateway.objects.filter(pk=self.pk).delete()

    def get_sensoren(self):
        if not len(self._sensoren):
            _set_gateway_sensoren(sender='Gateway', instance=self)
        return self._sensoren

    def get_unassigned_outputs(self):
        assigned = self.get_assigned_outputs()
        return [i for i in range(1, 16) if i not in assigned]

    def get_assigned_outputs(self):
        ret = list()
        for ausgang in self.ausgaenge.all():
            ret += [int(a.strip()) for a in ausgang.ausgang.split(',')]
        return sorted(ret)

    def get_marker(self):
        last = ch.get_gw_ping(self.name)
        marker = 'red'
        if last:
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            gwdelay = HausProfil.objects.get(haus=self.haus).get_gateway_delay()
            if int(gwdelay) == 1:
                gwdelay = 90
            if len(last) == 2 and last[0] and last[1] and \
                    (last[0]-last[1]).seconds < gwdelay*2 and (now-last[0]).seconds < gwdelay*2:
                marker = 'green'
            elif len(last) and (now-last[0]).days < 1:
                marker = 'yellow'
        return marker

    def create_cryptkey(self):
        import json
        import requests
        # try:
        #     r = requests.post("http://controme-main.appspot.com/get/%s/gwkey/" % self.rpi.name, data="data=%s" % self.rpi.crypt_keys.encrypt(json.dumps({'gw': self.name})))
        # except:
        #     return "Bitte stellen Sie zur Einrichtung sicher, dass der Miniserver mit dem Internet verbunden ist."
        #
        # try:
        #     key = self.rpi.crypt_keys.decrypt(r.text.replace(' ', '+'))
        #     key = json.loads(key.rsplit('}', 1)[0] + '}')
        #     key = key['key']
        # except:
        #     return "Ein Fehler ist aufgetreten. Haben Sie die MAC Adresse korrekt eingegeben?"

        key = "Trej9poUTnj43t5qDIOxwxoRX7sYOwyMAI/wKl9pzpM="

        try:
            ck = CryptKeys()
            ck.aeskey = key
            ck.save()
            self.crypt_keys = ck
            self.save()
            return None
        except:
            return "Ein Fehler ist aufgetreten. Versuchen Sie es bitte erneut."

    def get_parameters(self):
        try:
            return ast.literal_eval(self.parameters)
        except (SyntaxError, ValueError):
            return {}

    def set_parameters(self, params):
        self.parameters = str(params)
        self.save()
        return True

    def get_type(self):
        return type(self).__name__

    def is_heizraumgw(self):
        if self.name.startswith(('b8-27-eb', "dc-a6-32", "e4-5f-01")):
            return True
        return False

    def get_identifier(self):
        return "%s*%s" % (type(self).__name__, self.id)

    @property
    def ctrl_device(self):
        return self.gateway

    class Meta:
        app_label = 'heizmanager'


class AbstractSensor(CachingMixin, models.Model):
    raum = models.ForeignKey(Raum, related_name="%(class)sen", null=True)
    mainsensor = models.BooleanField(default=False)

    def get_identifier(self):
        return "%s*%s" % (type(self).__name__, self.id)

    @staticmethod
    def get_sensor(identifier):
        if not identifier or identifier.endswith("*None") or '*' not in identifier:
            return None
        clsname = identifier.split('*')[0]
        dbid = identifier.split('*')[1]

        try:
            scls = globals()[clsname]
        except KeyError:
            import importlib
            try:
                sfile = importlib.import_module('%s.models' % clsname.lower().split('sensor')[0])
            except ImportError:
                pass
            try:
                sfile = importlib.import_module("%s.models" % clsname.lower().split('aktor')[0])
            except ImportError:
                pass
            try:
                sfile = importlib.import_module('%s.models' % clsname.lower())
            except ImportError:
                pass
            try:
                sfile = importlib.import_module('%sen.models' % clsname.lower())
            except ImportError:
                pass

            try:
                scls = getattr(sfile, clsname)
            except (UnboundLocalError, AttributeError):
                return None
        try:
            s = ch.get(identifier)
            if type(s) == scls:
                return s
            s = scls.objects.get(pk=long(dbid))
            ch.set(identifier, s)
            return s
        except scls.DoesNotExist:
            return None

    @staticmethod
    def get_all_sensors(exclude=[], sort_by=['ctrl_device', 'name']):
        haus = Haus.objects.all()[0]
        cb2ctrldevs = sig_get_ctrl_devices.send(sender="hardware", haus=haus)
        ctrldevs = [c for ctrldev in [cd[1] for cd in cb2ctrldevs] for c in ctrldev]
        all_sensors = []
        for ctrldev in ctrldevs:
            if type(ctrldev).__name__ in exclude:
                continue
            sensors = ctrldev.get_sensoren()
            all_sensors.append(sensors)
        all_sensors = [sensor for sensors in all_sensors for sensor in sensors]
        all_sensors = sorted(all_sensors, key=lambda x: tuple(getattr(x, s) for s in sort_by))
        return all_sensors

    @staticmethod
    def get_all_sensors_info(sensors=None, exclude=[], sort_by=['ctrl_device', 'name']):
        if sensors is None:
            sensors = AbstractSensor.get_all_sensors(exclude=exclude, sort_by=sort_by)
        retlist = []
        haus = Haus.objects.all()[0]
        hparams = haus.get_module_parameters()
        global_atsensor = hparams.get('ruecklaufregelung', dict()).get('atsensor', -1)
        for s in sensors:
            ret = {}
            sname = s.name
            ret['sensor_id'] = s.get_identifier()
            ret['sensor_name'] = sname
            ret['sensortyp'] = ''
            ret['raum_name'] = ''
            ret['description'] = s.description or ''
            atsensor = False
            rlsensor = False
            if global_atsensor == s.get_identifier():
                atsensor = True
                ret['sensortyp'] = 'at' # Außentemperatursensor
            if s.raum:
                ret['raum_name'] = u"%s / %s" % (s.raum.etage.name, s.raum.name)
                if s.mainsensor:
                    ret['sensortyp'] = 'rt'  # Raumtemperatursensor
                elif s.raum.regelung.regelung == "ruecklaufregelung" and \
                                            s.get_identifier() in s.raum.regelung.get_parameters()['rlsensorssn']:
                    rlsensor = True
                    ret['sensortyp'] = 'rl'  # Rücklaufsensor

            if hasattr(s, 'gateway'):
                if s.gateway:
                    try:
                        if s.gateway.is_heizraumgw():
                            if not atsensor and not rlsensor and not s.mainsensor:
                                ret['sensortyp'] = 'hr'  # Heizraumsensor
                    except AttributeError:
                        # zb knxgw
                        pass

            if sname[:3] == '26_':
                ret['sensortyp'] = 'lf'  # Luftfeuchtigkeit
            retlist.append(ret)
        return retlist

    def get_type(self):
        return type(self).__name__

    @property
    def verbose_raum_name(self):
        if self.raum:
            return "%s / %s" % (self.raum.etage.name, self.raum.name)
        else:
            return ""

    class Meta:
        app_label = 'heizmanager'
        abstract = True


class Sensor(AbstractDevice, AbstractSensor):
    wert = models.FloatField(null=True)
    gateway = models.ForeignKey(Gateway, related_name='sensoren', null=True)
    offset = models.FloatField(blank=True, null=True)
    linie = models.IntegerField(default=0)
    
    objects = CachingManager()

    def __unicode__(self):
        return self.description if len(self.description) else self.name

    def delete(self, *args, **kwargs):
        try:
            if self.mainsensor:
                self.raum.regelung.gatewayausgang.delete()
        except (Raum.DoesNotExist, GatewayAusgang.DoesNotExist, AttributeError):  # attributeerror fuer raum=None
            pass
        Sensor.objects.filter(pk=self.pk).delete()

    def save(self, *args, **kwargs):
        s = re.match(r'^(00|10|28)_([0-9A-Fa-f]{2}[_]){6}([0-9A-Fa-f]{2})$', self.name)
        if not self.name.endswith("pt1000") and not self.name.endswith("ui10") and not self.name.endswith("ui20") and not s:
            raise ValueError('Ung&uuml;ltige Seriennummer.')
        sensoren = Sensor.objects.filter(name=self.name.lower())
        for sensor in sensoren:
            if sensor.id != self.pk and not self.name.endswith("pt1000") and not self.name.endswith("ui10") and not self.name.endswith("ui20"):
                raise ValueError('Sensorseriennummer bereits in Verwendung.')
        self.name = self.name.lower()
        super(Sensor, self).save(*args, **kwargs)
        last = ch.get_last_temp(self.name)
        if last:
            soffset = 0.0
            if len(last) == 5:
                soffset = last[4]
            offset = self.offset
            if offset is None:
                offset = 0.0
            ch.set_new_temp(self.name, (last[0]-soffset), self.haus_id, sensor_offset=offset, update_time=False)

    def get_wert(self):
        last = ch.get_last_temp(self.name)
        return last

    def get_marker(self):
        marker = 'red'
        last = self.get_wert()
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        if last:
            try:
                gwdelay = HausProfil.objects.get(haus_id=last[2]).get_gateway_delay()
            except HausProfil.DoesNotExist:
                profile = HausProfil.objects.all()
                if len(profile) == 1:
                    gwdelay = profile[0].get_gateway_delay()
                else:
                    gwdelay = 90  # weil default
            if int(gwdelay) == 1:
                gwdelay = 90
            if len(last) >= 4 and last[1] and last[3] and \
                    (last[1]-last[3]).seconds < gwdelay*1.5 and (now-last[1]).seconds < gwdelay*1.5:
                marker = 'green'
            elif len(last) >= 2 and last[1] and (now-last[1]).days < 1:
                marker = 'yellow'
        return marker

    def get_name_for_gw(self):
        return self.name

    def get_link(self):
        return "<a style='color:#cb0963; white-space:normal;' target='_blank' href='/m_setup/" + str(self.haus_id) + \
               "/hardware/sedit/" + str(self.id) + "/'>%s</a>"

    def get_name(self):
        if self.name.endswith('pt1000'):
            return self.name.split('#')[0] + 'PT1000'
        return self.name

    @property
    def ctrl_device(self):
        return self.gateway

    class Meta:
        app_label = 'heizmanager'


class Luftfeuchtigkeitssensor(AbstractDevice, AbstractSensor):
    wert = models.FloatField(null=True)
    gateway = models.ForeignKey(Gateway, related_name='lfsensoren', null=True)
    offset = models.FloatField(blank=True, null=True)
    linie = models.IntegerField(default=0)

    objects = CachingManager()

    def __unicode__(self):
        return self.description if len(self.description) else self.name

    def delete(self, *args, **kwargs):
        Luftfeuchtigkeitssensor.objects.filter(pk=self.pk).delete()

    def save(self, *args, **kwargs):
        s = re.match(r'^26_([0-9A-Fa-f]{2}[_]){6}([0-9A-Fa-f]{2})$', self.name)
        if not s:
            raise ValueError('Ung&uuml;ltige Seriennummer.')
        sensoren = Luftfeuchtigkeitssensor.objects.filter(name=self.name.lower())
        for sensor in sensoren:
            if sensor.id != self.pk:
                raise ValueError('Sensorseriennummer bereits in Verwendung.')
        self.name = self.name.lower()
        super(Luftfeuchtigkeitssensor, self).save(*args, **kwargs)

    def get_wert(self, val='Relative Humidity'):
        last = ch.get_last_temp(self.name)
        if last:
            berlin = pytz.timezone('Europe/Berlin')
            now = datetime.now(berlin)
            gwdelay = HausProfil.objects.get(pk=long(last[2])).get_gateway_delay()
            if (now-last[1]).total_seconds() > gwdelay*15:
                return None
        return last

    def get_marker(self):
        marker = 'red'
        last = self.get_wert()
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
        if last:
            gwdelay = HausProfil.objects.get(pk=long(last[2])).get_gateway_delay()
            if len(last) >= 4 and last[1] and last[3] and \
                    (last[1]-last[3]).total_seconds() < gwdelay*1.5 and (now-last[1]).total_seconds() < gwdelay*1.5:
                marker = 'green'
            elif len(last) >= 2 and last[1] and (now-last[1]).days < 1:
                marker = 'yellow'
        return marker

    class Meta:
        app_label = 'heizmanager'


class GatewayAusgang(AbstractOutput):
    STELLANTRIEB_CHOICES = (
        ('NO', 'normal offen'),
        ('NC', 'normal geschlossen'),
    )
    ausgang = models.CharField(max_length=45, blank=True, default='')
    gateway = models.ForeignKey(Gateway, related_name='ausgaenge')
    stellantrieb = models.CharField(max_length=2, choices=STELLANTRIEB_CHOICES)

    objects = CachingManager()

    def save(self, *args, **kwargs):
        tb = traceback.extract_stack()
        tb_files = '/'.join([t[0] for t in tb])  # damit der ganze pfad unten nicht angegeben sein muss
        tb_methods = [t[2] for t in tb]

        if 'ausgaenge_einrichtung' in tb_methods or 'raum_editieren' in tb_methods or 'gateway_editieren' in tb_methods\
                or '/pumpenlogik/views.py' in tb_files\
                or '/solarpuffer/views.py' in tb_files:
            super(GatewayAusgang, self).save(*args, **kwargs)
            ch.update_gateway(self.gateway.name.lower())

        else:
            macaddr = nh.get_mac()
            ret = {
                'tb': tb
            }
            try:
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)))
            except:
                pass

    def delete(self, *args, **kwargs):
        # delete ist unten auch noch als signal
        tb = traceback.extract_stack()
        tb_files = [t[0] for t in tb]
        tb_methods = [t[2] for t in tb]

        if 'ausgaenge_einrichtung' in tb_methods or 'raum_editieren' in tb_methods or 'etage_editieren' in tb_methods \
                or 'gateway_editieren' in tb_methods or 'sensor_editieren' in tb_methods\
                or './rf/views.py' in tb_files\
                or './pumpenlogik/views.py' in tb_files\
                or './solarpuffer/views.py' in tb_files\
                or './vsensoren/models.py' in tb_files\
                or './vsensoren/views.py' in tb_files\
                or './vorlauftemperaturregelung/views.py' in tb_files\
                or './differenzregelung/views.py' in tb_files\
                or './fps/views.py' in tb_files:
            super(GatewayAusgang, self).delete(*args, **kwargs)

        else:
            macaddr = nh.get_mac()
            ret = {
                'tb': tb
            }
            try:
                requests.post("http://controme-main.appspot.com/set/rpilog/%s/" % macaddr, data="data=%s" % aes_encrypt(json.dumps(ret)))
            except:
                pass

    def get_regelung(self):
        if self.regelung_id:
            regelung = self.regelung
            return regelung
        
        return None

    def get_ctrldevice(self):
        return self.gateway

    class Meta:
        app_label = 'heizmanager'


###################
# Signal Receiver #
###################

sig_get_raum_sensoren = dispatch.Signal(providing_args=['raum'])
sig_solltemp_change = dispatch.Signal(providing_args=['haus', 'raum'])


@dispatch.receiver(post_delete, sender=GatewayAusgang)
def _ausgang_delete(sender, instance, **kwargs):
    ch.update_gateway(instance.gateway.name.lower())


@dispatch.receiver(post_init, sender=Raum)
def _set_raum_sensoren(sender, instance, **kwargs):
    sensoren = Sensor.objects.filter(raum=instance)
    instance._sensoren += list(sensoren)
    lfsensoren = Luftfeuchtigkeitssensor.objects.filter(raum=instance)
    instance._sensoren += list(lfsensoren)


@dispatch.receiver(post_init, sender=Gateway)
def _set_gateway_sensoren(sender, instance, **kwargs):
    sensoren = Sensor.objects.filter(gateway=instance)
    for s in sensoren:
        instance._sensoren.append(s)


@dispatch.receiver(post_delete, sender=Sensor)
@dispatch.receiver(post_save, sender=Sensor)
def _invalidate_sensor_cache(sender, instance, **kwargs):
    ch.delete(instance.get_identifier())

sig_get_outputs_from_regelung = dispatch.Signal(providing_args=['haus', 'raum', 'regelung'])
sig_get_possible_outputs_from_regelung = dispatch.Signal(providing_args=['haus', 'raumdict'])

sig_get_outputs_from_output = dispatch.Signal(providing_args=['raum', 'regelung'])
sig_get_all_outputs_from_output = dispatch.Signal(providing_args=['haus'])
sig_get_possible_outputs_from_output = dispatch.Signal(providing_args=['haus'])

sig_get_all_outputs = dispatch.Signal(providing_args=['haus'])
sig_get_ctrl_devices = dispatch.Signal(providing_args=['haus'])

sig_create_output = dispatch.Signal(providing_args=['haus', 'ctrldevice', 'regelung', 'sensor', 'outs'])
sig_create_output_regelung = dispatch.Signal(providing_args=['haus', 'ctrldevice', 'regelung', 'sensor', 'outs'])

sig_create_regelung = dispatch.Signal(providing_args=['regelung', 'operation', 'sensor', 'sensortyp', 'outs', 'changed_sensor'])


@dispatch.receiver(sig_get_outputs_from_output)
def gw_get_outputs_from_output(sender, **kwargs):
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    ausgaenge = GatewayAusgang.objects.filter(regelung_id=regelung.pk).select_related('gateway', 'regelung')
    return [(a.gateway, raum, regelung, a, [int(out) for out in a.ausgang.split(', ')]) for a in ausgaenge]


@dispatch.receiver(sig_get_all_outputs_from_output)
def gw_get_all_outputs_from_output(sender, **kwargs):
    ausgaenge = GatewayAusgang.objects.select_related('gateway', 'regelung').all()

    haus = kwargs['haus']

    def get_raum(reg_id):
        try:
            return Raum.objects.get_for_user(haus.eigentuemer, regelung_id=reg_id)
        except Raum.DoesNotExist:
            return None

    # return [(a.gateway, a.get_regelung().get_raum(), a.get_regelung(), a, [int(out) for out in a.ausgang.split(', ')]) for a in ausgaenge]
    # return [(a.gateway, Raum.objects.get(regelung_id=a.regelung_id), a.regelung, a, [int(out) for out in a.ausgang.split(', ')]) for a in ausgaenge]
    return [(a.gateway, get_raum(a.regelung_id), a.regelung, a, [int(out) for out in a.ausgang.split(', ')]) for a in ausgaenge]


@dispatch.receiver(sig_get_possible_outputs_from_output)
def gw_get_possible_outputs_from_output(sender, **kwargs):
    haus = kwargs['haus']
    ret = {}
    gws = Gateway.objects.all()
    gwdict = dict((gw.id, gw) for gw in gws)  # if not gw.is_heizraumgw())
    for raum in Raum.objects.select_related('regelung').filter(regelung__regelung=sender):
        ret.setdefault(raum, list())
        for sensor in raum.get_sensoren():
            try:
                ret[raum].append((sensor, gwdict[sensor.gateway_id]))
            except (Gateway.DoesNotExist, AttributeError, KeyError):
                pass
    return ret


@dispatch.receiver(sig_get_all_outputs)
def gw_get_all_outputs(sender, **kwargs):
    ret_gws = dict()
    gws = Gateway.objects.all()
    klemmendict = {1: 'Klemme 11N - 12L', 2: 'Klemme 13N - 14L', 3: 'Klemme 15N - 16L', 4: 'Klemme 17N - 18L',
                   5: 'Klemme 19N - 20L', 6: 'Klemme 21N - 22L', 7: 'Klemme 23N - 24L', 8: 'Klemme 25N - 26L',
                   9: 'Klemme 41L - 42N', 10: 'Klemme 43L - 44N', 11: 'Klemme 45L - 46N', 12: 'Klemme 47L - 48N',
                   13: 'Klemme 49L - 50N', 14: 'Klemme 51L - 52N', 15: '6no - 7center - 8nc'}

    klemmendict010v = {1: "Klemme 11 (12-, 13+)", 2: "Klemme 14 (12-, 13+)", 3: "Klemme 15 (16-, 17+)",
                       4: "Klemme 18 (16-, 17+)", 5: "Klemme 19 (20-, 21+)", 6: "Klemme 22 (20-, 21+)",
                       7: "Klemme 23 (24-, 25+)", 8: "Klemme 26 (24-, 25+)", 9: "Klemme 41 (42-, 43+)",
                       10: "Klemme 44 (42-, 43+)", 11: "Klemme 45 (46-, 47+)", 12: "Klemme 48 (46-, 47+)",
                       13: "Klemme 49 (50-, 51+)", 14: "Klemme 52 (50-, 51+)", 15: "6no - 7center - 8nc"}

    klemmendict_hr = {1: 'Relais 1 (6no - 7c - 8nc)', 2: 'Relais 2 (9no - 10c)', 3: 'Relais 3 (11no - 12c)',
                      4: 'Relais 4 (13no - 14c)', 5: 'Relais 5 (15no - 16c)', 6: 'Relais 6 (17no - 18c)',
                      7: 'Relais 7 (19no - 20c)', 8: 'Relais 8 (21no - 22c)', 9: 'Relais 9 (23no - 24c)',
                      10: 'Relais 10 (25no - 26c)', 11: 'SSR1 (47/48)', 12: 'SSR2 (49/50)', 13: 'Analog 1 (35/36)',
                      14: 'Analog 2 (37/38)'}

    klemmendictmini = {1: "Relais 1 (7c - 8no)", 2: "Relais 2 (9nc - 10c - 11no)"}

    for gateway in gws:
        if gateway.is_heizraumgw():
            ret_gws[gateway] = (
                str(gateway.id) + '##' + str(gateway) + '##' + gateway.get_type(),
                dict((i+1, [klemmendict_hr[i+1]]) for i in range(14))
            )
        elif "6.00" > gateway.version >= "5.00":
            ret_gws[gateway] = (
                str(gateway.id) + '##' + str(gateway) + '##' + gateway.get_type(),
                dict((i+1, [klemmendict010v[i+1]]) for i in range(15))
            )
        elif "7.00" > gateway.version >= "6.00":
            ret_gws[gateway] = (
                str(gateway.id) + '##' + str(gateway) + '##' + gateway.get_type(),
                dict((i+1, [klemmendictmini[i+1]]) for i in range(2))
            )
        else:
            ret_gws[gateway] = (
                str(gateway.id) + '##' + str(gateway) + '##' + gateway.get_type(),
                dict((i+1, [klemmendict[i+1]]) for i in range(15))
            )

    return ret_gws


@dispatch.receiver(sig_get_ctrl_devices)
def gw_get_all_gws(sender, **kwargs):
    haus = kwargs['haus']
    return [gw for gw in haus.gateway_related.all() if not gw.is_heizraumgw()]


@dispatch.receiver(sig_get_ctrl_devices)
def hrgw_get_all_hrgws(sender, **kwargs):
    # if sender in {'fsr', 'rlr', 'zpr'}:
    #     return []
    haus = kwargs['haus']
    return [hrgw for hrgw in haus.gateway_related.all() if hrgw.is_heizraumgw()]


@dispatch.receiver(sig_create_output)
def gw_create_output(sender, **kwargs):
    haus = kwargs['haus']
    ctrldev = kwargs['ctrldevice']
    regelung = kwargs['regelung']
    sensor = kwargs['sensor']
    outs = kwargs['outs']

    if type(ctrldev).__name__ in globals():
        try:
            ausgang = regelung.gatewayausgang
        except (AttributeError, GatewayAusgang.DoesNotExist):
            ausgang = GatewayAusgang(regelung=regelung, gateway=ctrldev, stellantrieb='NC')

        if sender == 'create':
            ausgang.ausgang = ', '.join(outs)
        elif sender == 'update':
            a = [s.strip() for s in set(ausgang.ausgang.split(', ') + outs) if len(s.strip())]
            ausgang.ausgang = ', '.join(a)

        if not sender == 'delete':
            ausgang.save()

        if haus is not None:  # wegen rlr_create_regelung
            sig_create_output_regelung.send(sender=sender, ctrldevice=ctrldev, regelung=regelung, sensor=sensor, outs=outs)


sig_new_temp_value = dispatch.Signal(providing_args=['name', 'value', 'modules', 'timestamp'])
sig_new_output_value = dispatch.Signal(providing_args=['name', 'value', 'ziel', 'parameters', 'timestamp', 'modules'])


@dispatch.receiver(post_save, sender=Regelung)
def _invalidate_regelung_cache(sender, instance, **kwargs):
    regs = Regelung.objects.filter(regelung__in=['differenzregelung', 'vorlauftemperaturregelung'])
    from caching.invalidation import Invalidator
    invalidator = Invalidator()
    invalidator.invalidate_objects(regs)
