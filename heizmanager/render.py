from django.shortcuts import redirect, render
from django.views.decorators.csrf import csrf_protect
import heizmanager.cache_helper as ch


@csrf_protect
def render_response(req, *args, **kwargs):
    if not req.path.startswith('/accounts/'):
        args[1]['user'] = req.user
    response = render(req, *args, **kwargs)
    response['Cache-Control'] = 'no-cache must-revalidate proxy-revalidate'
    response['Pragma'] = 'no-cache'
    getdata = ('?%s' % req.GET.urlencode()) if len(req.GET.urlencode()) else ""
    ch.set_usage_val("render_response", (req.path+getdata, req.method))
    return response


def render_redirect(req, *args, **kwargs):
    response = redirect(*args, **kwargs)
    response['Cache-Control'] = 'no-cache must-revalidate proxy-revalidate'
    response['Pragma'] = 'no-cache'
    getdata = ('?%s' % req.GET.urlencode()) if len(req.GET.urlencode()) else ""
    ch.set_usage_val("render_redirect", (req.path+getdata, req.method))
    return response
