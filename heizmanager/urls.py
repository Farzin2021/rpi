#from django.conf.urls.defaults import *
from django.conf.urls import url
import heizmanager.getandset as getandset
import heizmanager.mobile as mobile
import heizmanager.modules as modules
from heizmanager.modules import hardware
from heizmanager.modules import setup_rpi


urlpatterns = [ 

        # temperatur
        url(r'^set/(?P<sensorssn>([0-9A-Fa-f]{2}[_]){7}([0-9A-Fa-f]{2}))/(?P<ist>[-]*\d+\.\d{2})/$', getandset.tset),
        url(r'^set/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/all/', getandset.tset_all),
        url(r'^set/local/(?P<raumid>\d+)/(?P<soll>\d+\.\d{2})/$', getandset.set_gateways_soll),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<ausgang>\d+)/$', getandset.get),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/$', getandset.get_backup),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/(?P<version>\d+\.\d{2})/$', getandset.get_backup),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/all/$', getandset.get_all),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/setting/(?P<version>\d+\.\d{2})/$', getandset.get_setting),
        url(r'^get/(?P<macaddr>([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2}))/nonce/$', getandset.get_nonce),

        url(r'^$', mobile.m_temp.index),
        url(r'^get_main_menus_ajax$', mobile.m_temp.get_main_menus_ajax),
        url(r'^menu_ajax$', mobile.m_temp.get_main_menus_ajax),
        url(r'^sensorenuebersicht$', mobile.m_temp.sensorenuebersicht),
        url(r'^sensorenuebersicht_hrgw/$', mobile.m_temp.sensorenuebersicht_hrgw),
        url(r'^m_houseprofile_edit/$', mobile.m_temp.houseprofile_edit),
        url(r'^no-access/$', mobile.m_temp.no_access),

        url(r'^m_raum_temp_html/(?P<raumid>\d+)/$', mobile.m_temp.get_raum_state_from_reg),
        url(r'^config/$', mobile.m_setup.config),
        url(r'^config/(?P<hausid>\d+)/$', mobile.m_setup.config),
        url(r'^m_raum/(?P<raumid>\d+)/$', mobile.m_temp.show_raum),
        url(r'^m_raum/(?P<raumid>\d+)/(?P<modulename>[a-z]+)/$', mobile.m_temp.get_module_settings),

        url(r'^m_setup/(?P<hausid>\d+)/hardware/$', modules.hardware.index),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/er/$', modules.hardware.raeume_einrichtung),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/erc/$', modules.hardware.raeume_anzeigen),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/gw/$', modules.hardware.gateway_einrichtung),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/gwc/$', modules.hardware.gateways_anzeigen),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/s/$', modules.hardware.sensoren_einrichtung),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/sc/$', modules.hardware.sensoren_anzeigen),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/a/$', modules.hardware.ausgaenge_einrichtung),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/ac/$', modules.hardware.ausgaenge_anzeigen),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/acs/$', modules.hardware.ausgaenge_anzeigen_sensoren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/redit/$', modules.hardware.raum_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/eedit/$', modules.hardware.etage_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/gwedit/$', modules.hardware.gateway_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/sedit/$', modules.hardware.sensor_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/redit/(?P<raumid>\d+)/$', modules.hardware.raum_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/eedit/(?P<etagenid>\d+)/$', modules.hardware.etage_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/gwedit/(?P<gwid>\d+)/$', modules.hardware.gateway_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/sedit/(?P<sid>(([PT10_an]+|[Digitaleingang_{1,2}_an]+)([0-9A-Fa-f]{2}[-]){5}([0-9A-Fa-f]{2})))/$', modules.hardware.sensor_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/sedit/(?P<sid>([0-9A-Fa-f]{2}[_]){7}([0-9A-Fa-f]{2}))/$', modules.hardware.sensor_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/smult/$', modules.hardware.sensoren_editieren),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/e/(?P<etagenid>\d+)/(?P<dir>[a-z]+)/$', modules.hardware.edit_etage_move),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/(?P<raumid>\d+)/(?P<dir>[a-z]+)/$', modules.hardware.edit_raum_move),
        url(r'^m_setup/(?P<hausid>\d+)/hardware/setup_rpi/$', setup_rpi.get_global_settings_page),

        url(r'^m_setup/(?P<hausid>\d+)/(?P<modulename>[a-z_]+)/$', mobile.m_setup.get_module_settings),
        url(r'^m_setup/(?P<hausid>\d+)/(?P<modulename>[a-z_]+)/hilfe/$', mobile.m_setup.get_module_settings_help),

        url(r'^m_(?P<modulename>[a-z]+)/(?P<entityid>\d+)/$', mobile.m_setup.get_module_page_for_entity),
        url(r'^m_(?P<modulename>[a-z]+)/(?P<entityid>\d+)/(?P<action>[a-z]+)/$', mobile.m_setup.get_module_page_for_entity),
        url(r'^m_(?P<modulename>[a-z]+)/(?P<entityid>\d+)/(?P<action>[a-z]+)/(?P<objid>\d+)/$', mobile.m_setup.get_module_page_for_entity),
        url(r'^m_(?P<modulename>[a-z]+)/(?P<entityid>\d+)/(?P<action>[a-z]+)/(?P<objid>\d+)/(?P<regmodule>[a-z]+)/$', mobile.m_setup.get_module_page_for_entity),

        url(r'^dbdl/$', getandset.get_db),
        url(r'^dbul/$', getandset.set_db),
        url(r'^logdl/$', getandset.get_log),
        url(r'^logdl(?P<day>\d+)/$', getandset.get_log),
]
