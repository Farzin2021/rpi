from django.apps import AppConfig


class HeizmanagerConfig(AppConfig):
    name = 'heizmanager'
    verbose_name = 'controme heizmanager'

    def ready(self):
        pass
