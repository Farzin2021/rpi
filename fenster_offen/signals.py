from django.dispatch import receiver
import heizmanager.cache_helper as ch
from heizmanager.models import sig_new_temp_value


@receiver(sig_new_temp_value)
def keep_sensor_values(sender, **kwargs):
    if sender != 'tset':
        return

    sensor_id = kwargs['name'].replace('*', '_')
    cache_key = "open_window_values_%s" % sensor_id
    sensor_val = kwargs['value']

    sensor_values = ch.get(cache_key)
    sensor_values = sensor_values if sensor_values is not None else list()
    sensor_values.append(sensor_val)
    # keeps only 4 last records with ttl of one hour
    ch.set(cache_key, sensor_values[-4:], 60*60)

    is_activated = ch.get('open_window_%s' % sensor_id)
    if is_activated:
        coldest = ch.get('open_window_coldest_%s' % sensor_id)
        coldest = min(coldest, sensor_val) if coldest else sensor_val
        ch.set('open_window_coldest_%s' % sensor_id, coldest, 60*60)


