# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
import pytz
import heizmanager.cache_helper as ch
from heizmanager.models import Haus, Raum
from logger.models import SimpleSensorLog
import logging
from heizmanager.helpers import activate, deactivate
from datetime import datetime, timedelta


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return True


def check_active_condition(values, temperature, active_val):
    ret = filter(lambda x: x - temperature > active_val, values)
    return len(ret) > 0


def get_offset(haus, raum=None):
    if raum is None:
        return 0.0

    # return offset if betriebsarted is not activated or is heizen
    if raum.get_betriebsart() != '2':
        return 0.0

    ms = raum.get_mainsensor()
    if not ms:
        return 0.0

    sensor_id = ms.get_identifier().replace('*', '_')
    values = ch.get("open_window_values_%s" % sensor_id)
    if values is None:
        return 0.0

    # get parameters
    mod_params = haus.get_spec_module_params('fenster_offen')
    rooms = mod_params.get('rooms', list())

    # default all rooms checks window open, it's also possible, user selects rooms
    if len(rooms) and str(raum.id) not in rooms:
        return 0.0

    activation_value = mod_params.get('activation_value', 1.0)
    deactivation_value = mod_params.get('deactivation_value', 1.0)

    # last value used as current temperature
    current_temperature = values[-1]
    # past values
    last_values = values[:-1]
    window_open = ch.get('open_window_%s' % sensor_id)
    # temperature
    if window_open is None:
        if current_temperature < 20.0 and check_active_condition(last_values, current_temperature, activation_value):
            ch.set('open_window_%s' % sensor_id, current_temperature)
            offset = -10.0
        else:
            offset = 0.0
    else:  # window is opened before
        coldest = ch.get('open_window_coldest_%s' % sensor_id)
        # this is coldest value since  it's activated
        coldest = coldest if coldest else 0.0
        # deactivation checking
        if current_temperature - coldest > deactivation_value:
            ch.delete('open_window_%s' % sensor_id)
            ch.delete('open_window_coldest_%s' % sensor_id)
            offset = 0.0
        else:
            offset = -10.0

    return offset


def get_offset_regelung(haus, module, objid):
    return


def get_name():
    return 'Fenster-offen-Erkennung'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/fenster_offen/'>Fenster-offen-Erkennung</a>" % haus.id


def get_global_description_link():
    desc = u"Erkennt offene Fenster anhand des Temperaturabfalls."
    desc_link = "https://support.controme.com/fenster-offen-erkennung/"
    return desc, desc_link


def get_local_settings_link(request, raum):
    offset = get_offset(raum.etage.haus, raum)
    if not offset:
        offset = "deaktiviert"
    else:
        offset = "%.2f" % offset
    return "Fenster-offen-Erkennung", "fenster-offen", offset, 75, "#"


def get_local_settings_link_regelung(haus, modulename, objid):
    return


def get_local_settings_page(request, raum):
    if request.method == "GET":
        pass

    if request.method == "POST":
        pass


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass


def get_global_settings_page_help(request, haus):
    return None


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    if request.method == "GET":

        if action == 'reset':
            # reset offset for open windows
            for etage in haus.etagen.all():
                for raum in Raum.objects.filter(etage_id=etage.id):
                    if ch.get('open_window_%s' % raum.id) is not None:
                        ch.delete('open_window_%s' % raum.id)
            return render_redirect(request, '/m_setup/%s/fenster_offen/' % haus.id)

        context = dict()
        context['haus'] = haus

        # collecting room
        eundr = list()
        for etage in haus.etagen.all():
            e = {etage: []}
            for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                e[etage].append(_raum)
            eundr.append(e)
        context['eundr'] = eundr

        mod_params = haus.get_spec_module_params('fenster_offen')
        context['activation_value'] = mod_params.get('activation_value', 1.0)
        context['deactivation_value'] = mod_params.get('deactivation_value', 1.0)
        context['rooms'] = mod_params.get('rooms', [])
        return render_response(request, "m_settings_fenster_offen.html", context)

    if request.method == "POST":
        
        # preparing data
        mod_params = haus.get_spec_module_params('fenster_offen')
        mod_params['activation_value'] = float(request.POST.get('activation_value', 1.0))
        mod_params['deactivation_value'] = float(request.POST.get('deactivation_value', 1.0))
        mod_params['rooms'] = request.POST.getlist('rooms')
        haus.set_spec_module_params('fenster_offen', mod_params)

        # activate module for rooms
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter(etage_id=etage.id):
                activate(raum, 'fenster_offen')

        return render_redirect(request, '/m_setup/%s/fenster_offen/' % haus.id)


def get_local_settings_link_haus(request, haus):
    pass


def get_local_settings_page_regelung(request, haus, modulename, objid, params):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass
