# coding=utf-8

from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
from django.http import JsonResponse
from heizmanager.models import Haus, Raum, Regelung
import time
import heizmanager.cache_helper as ch
from datetime import datetime, timedelta
import logging


def get_name():
    return 'Vorlauftemperaturkorrektur'


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def offset_calc(raum, deviation):

    anhebung, absenkung = raum.get_vorlauftemperaturkorrektur_params()

    if deviation < 0:
        offset = deviation * anhebung * (-1)
    else:
        offset = deviation * absenkung * (-1)

    return offset


def get_calculated_offset(haus, vtk_key):
    max_offset = float("-inf")
    offset = 0
    vtk_haus_params = haus.get_spec_module_params('vorlauftemperaturkorrektur')
    rooms = vtk_haus_params.get(vtk_key, dict()).get('rooms_list', [])
    for raumid in rooms:
        try:
            raum = Raum.objects.get(id=long(raumid))
        except Raum.DoesNotExist:
            continue

        try:
            deviation = float(vtk_haus_params['deviation'][raum.id])
        except:
            deviation = 0

        try:
            module_offset = offset_calc(raum, deviation)
        except:
            module_offset = 0

        if module_offset > max_offset:
            max_offset = module_offset

        max_absenkung, max_anhebung = get_max_params(haus, vtk_key)
        offset = min(max(-max_absenkung, max_offset), max_anhebung)

    return offset


def get_offset_regelung(haus, modulename, objid):

    if modulename not in {"vorlauftemperaturregelung", "fps"}:
        return 0
    if 'vorlauftemperaturkorrektur' not in haus.get_modules():
        return 0

    haus_params = haus.get_module_parameters()
    vtk_list = sorted(haus_params.get('vorlauftemperaturkorrektur', dict()).iteritems())
    if modulename == "vorlauftemperaturregelung":
        mrs = haus_params.get('vorlauftemperaturregelung', dict()).get(objid, None)
        if mrs is None:
            return 0
    elif modulename == "fps":
        if str(objid) not in vtk_list.keys():
            return 0

    vtk_list = [(k, v) for k, v in vtk_list if k and k.isdigit() and v['vorlauftemperaturregelung_id'] == str(objid)]
    total_offset = 0
    for k, v in vtk_list:
        offset = get_calculated_offset(haus, k)
        total_offset += offset

    ch.set_module_offset_regelung("vorlauftemperaturkorrektur", haus.id, modulename, str(objid), total_offset, ttl=600)

    return total_offset


def get_offset(haus, raum=None):
    return 0.0


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/vorlauftemperaturkorrektur/'>Vorlauftemperaturkorrektur</a>" % haus.id


def get_global_description_link():
    desc = u"Einfluss der Räume auf die Vorlauftemperatur."
    desc_link = "http://support.controme.com/vorlauf-korrektur/"
    return desc, desc_link


def get_max_params(haus, entityid):

    params = haus.get_module_parameters()
    try:
        max_decrease = float(params['vorlauftemperaturkorrektur'][entityid]['max_decrease'])
        max_increase = float(params['vorlauftemperaturkorrektur'][entityid]['max_increase'])
    except:
        max_decrease = 3
        max_increase = 3

    return max_decrease, max_increase


def get_global_settings_page(request, haus):

    params = haus.get_module_parameters()

    if request.method == 'GET':
        
        if 'edit' in request.GET:
            entityid = request.GET.get('edit', None)
            max_decrease, max_increase = get_max_params(haus, entityid)
            # list of vorlauftemperaturregelung 
            vtr_list = sorted(params.get('vorlauftemperaturregelung', dict()).iteritems())
            ret = dict()
            ret['list_vorlauftemperaturregelung'] = vtr_list

            select_all_room = False
            if entityid:
                ret['vtk_id'] = entityid
                ret.update(params.get('vorlauftemperaturkorrektur', dict()).get(entityid, dict()))
                if 'rooms_list' not in ret:
                    select_all_room = True
                    ret['rooms_list'] = []
            else:
                ret['rooms_list'] = []
                select_all_room = True

            all_room_ids = []
            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                    all_room_ids.append(str(_raum.id))
                eundr.append(e)
            if not entityid or select_all_room:
                ret['rooms_list'] = all_room_ids
            elif not select_all_room and all([x in ret['rooms_list'] for x in all_room_ids]):
                select_all_room = True
            ret['eundr'] = eundr
            ret['select_all_room'] = select_all_room
            ret['max_decrease'] = max_decrease
            ret['max_increase'] = max_increase

            ret['haus'] = haus

            return render_response(request, "m_settings_vorlauftemperaturkorrektur_edit.html", ret)

        elif "delete" in request.GET:
            entityid = request.GET.get('delete', None)
            try:
                del params['vorlauftemperaturkorrektur'][entityid]
                haus.set_module_parameters(params)
            except:
                pass
            return render_redirect(request, "/m_setup/%s/vorlauftemperaturkorrektur/" % haus.id)

        else:
            vtk_list = sorted(params.get('vorlauftemperaturkorrektur', dict()).iteritems())
            vtk_list = {k: v for k, v in vtk_list if k and k.isdigit()}

            context = dict()
            context['haus'] = haus
            context['r_m_active'] = params.get('vorlauftemperaturkorrektur_right_menu', True)
            context['list_vorlauftemperaturkorrektur'] = vtk_list
            return render_response(request, "m_settings_vorlauftemperaturkorrektur.html", context)

    elif request.method == 'POST':
        if 'right_menu_ajax' in request.POST:
            params['vorlauftemperaturkorrektur_right_menu'] = bool(int(request.POST['right_menu_ajax']))
            haus.set_module_parameters(params)
            return JsonResponse({'message': 'done'})

        name = request.POST['name']
        max_decrease = request.POST['absenkung']
        max_increase = request.POST['anhebung']
        vtr_id = request.POST.get('vtr_id', None)
        rooms = request.POST.getlist('rooms')
        entityid = request.POST.get('vtk_id', None)
        err=""

        if not vtr_id:
            err = u"bitte Vorlauftemperaturregelung auswählen.\n"
        if not name:
            err = u"bitte Namen eingeben.\n"
        if len(err):
            max_de, max_in = get_max_params(haus, entityid)
            if not max_decrease:
                max_decrease = max_de
            if not max_increase:
                max_increase = max_in
            # list of vorlauftemperaturregelung 
            vtr_list = sorted(params.get('vorlauftemperaturregelung', dict()).iteritems())
            ret = dict()
            ret['list_vorlauftemperaturregelung'] = vtr_list
            ret['error'] = err
            ret['name'] = name
            ret['vorlauftemperaturregelung_id'] = vtr_id
            ret['vtk_id'] = request.GET.get('edit', None)

            select_all_room = False
            if entityid:
                ret.update(params.get('vorlauftemperaturkorrektur', dict()).get(entityid, dict()))
                if 'rooms_list' not in ret:
                    select_all_room = True
                    ret['rooms_list'] = []
            else:
                ret['rooms_list'] = []
                select_all_room = True

            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                    if not entityid or select_all_room:
                        ret['rooms_list'].append(str(_raum.id))
                eundr.append(e)
            ret['eundr'] = eundr
            ret['select_all_room'] = select_all_room
            ret['max_decrease'] = max_decrease
            ret['max_increase'] = max_increase

            ret['haus'] = haus

            return render_response(request, "m_settings_vorlauftemperaturkorrektur_edit.html", ret)

        
        vtk_list = params.get('vorlauftemperaturkorrektur', dict())
        if not entityid:
            ids = [0] + [int(k) for k in vtk_list.keys() if k and k.isdigit()]
            entityid = str(max(ids)+1)

        params.setdefault('vorlauftemperaturkorrektur', dict())
        params['vorlauftemperaturkorrektur'].setdefault(entityid, dict())
        params['vorlauftemperaturkorrektur'][entityid]['name'] = name
        params['vorlauftemperaturkorrektur'][entityid]['max_decrease'] = max_decrease
        params['vorlauftemperaturkorrektur'][entityid]['max_increase'] = max_increase
        params['vorlauftemperaturkorrektur'][entityid]['rooms_list'] = rooms
        params['vorlauftemperaturkorrektur'][entityid]['vorlauftemperaturregelung_id'] = vtr_id
        haus.set_module_parameters(params)

    return render_redirect(request, "/m_setup/%s/vorlauftemperaturkorrektur/" % haus.id)


def get_local_settings_link(request, raum):
    pass


def get_local_settings_page(request, raum):
    pass


def get_local_settings_page_haus(request, haus, action=None, objid=None):

    if request.method == "GET":

        haus_params = haus.get_module_parameters()
        # list_dict = []
        lst = []
        mrs = haus_params.get('vorlauftemperaturkorrektur', dict())
        vtr_list = haus_params.get('vorlauftemperaturregelung', dict())
        vtk_list = []
        for rl in mrs.keys():
            if not rl.isdigit():
                continue
            list_dict_rooms = []
            name = mrs[rl].get('name', "")
            rooms = mrs[rl].get('rooms_list', [])
            vtr_id = mrs[rl].get('vorlauftemperaturregelung_id', 0)
            vtr_name = vtr_list.get(vtr_id, dict()).get('name', "")
            rooms_name = []
            for raumid in rooms:
                try:
                    raum = Raum.objects.get(id=long(raumid))
                except Raum.DoesNotExist:
                    continue
                else:
                    rooms_name.append(raum.name)
                    try:
                        deviation = float(haus_params['vorlauftemperaturkorrektur']['deviation'][raum.id])
                    except Exception:
                        deviation = 0

                    try:
                        module_offset = offset_calc(raum, deviation)
                    except Exception:
                        module_offset = 0

                    list_dict_rooms.append({'raum': raum.name, "deviation": deviation, 'offset': module_offset})
            
            if len(list_dict_rooms):
                max_offset_dict = max(enumerate(list_dict_rooms), key=lambda item: item[1]['offset'])
                max_offset = max_offset_dict[1]['offset']
                room_with_most_positive_offset = max_offset_dict[1]['raum']
                deviation = max_offset_dict[1]['deviation']
            else:
                max_offset = 0
                deviation = 0
                room_with_most_positive_offset = 0

            max_absenkung, max_anhebung = get_max_params(haus, rl)
            offset_limited = min(max(-max_absenkung, max_offset), max_anhebung)

            vtk_list.append({
                'name': name, 
                'id': rl,
                'r_w_m_p_o': room_with_most_positive_offset,
                'deviation':deviation,
                'offset':max_offset,
                'negative': max_absenkung, 
                'positive': max_anhebung,
                'offset_limit':offset_limited,
                'rooms': rooms_name,
                'vorlauftemperaturregelung_id':vtr_id,
                'vorlauftemperaturregelung_name':vtr_name,
            })
        vtk_list = sorted(vtk_list, key=lambda d: d['id'])

        for etage in haus.etagen.all():
            for raum in etage.raeume.all():
                try:
                    deviation = float(haus_params['vorlauftemperaturkorrektur']['deviation'][raum.id])
                except Exception:
                    logging.exception("fehler bei der abweichungsberechnung")
                    deviation = 0

                try:
                    module_offset = offset_calc(raum, deviation)
                except Exception:
                    logging.exception("fehler bei der offsetberechnung")
                    module_offset = 0
                
                # list of vorlauftemperaturkorrektur, that this room was selected in them
                room_vtk_list = []
                room_vtr_list = []
                for rl in mrs.keys():
                    if not rl.isdigit():
                        continue
                    name = mrs[rl].get('name', "")
                    rooms = mrs[rl].get('rooms_list', [])
                    vtr_id = mrs[rl].get('vorlauftemperaturregelung_id', 0)
                    vtr_name = vtr_list.get(vtr_id, dict()).get('name', "")
                    if str(raum.id) in rooms:
                        room_vtk_list.append({'name': name, 'id': rl})
                        room_vtr_list.append({'name': vtr_name, 'id': vtr_id})

                room_vtr_list = {v['id']:v for v in room_vtr_list}.values()
                # list_dict.append({'raum': raum.name, "deviation": deviation, 'offset': module_offset})
                anhebung, absenkung = raum.get_vorlauftemperaturkorrektur_params()
                deviation_text = 'Die Raumtemperatur ist %.2f° zu niedrig. Es gilt der Slider “Anhebung”.' % deviation if deviation >= 0 \
                    else "Die Raumtemperatur ist %.2f zu hoch. Es gilt der Slider “Absenkung”" % deviation
                lst.append(["%s / %s" % (etage.name, raum.name), deviation, module_offset, absenkung, anhebung, room_vtk_list, room_vtr_list, deviation_text])

        # if len(list_dict):
        #     max_offset_dict = max(enumerate(list_dict), key=lambda item: item[1]['offset'])
        #     max_offset = max_offset_dict[1]['offset']
        #     room_with_most_positive_offset = max_offset_dict[1]['raum']
        #     deviation = max_offset_dict[1]['deviation']
        # else:
        #     max_offset = 0
        #     deviation = 0
        #     room_with_most_positive_offset = 0

        # max_absenkung, max_anhebung = get_max_params(haus)
        # offset_limited = min(max(-max_absenkung, max_offset), max_anhebung)

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                e[etage].append(raum)
            eundr.append(e)

        try:
            time_check_now = ch.get('vorlauf_now_time')
            time_check_next = time_check_now + timedelta(minutes=10)
            time_check_now = time_check_now.strftime("%d.%m. %H:%M")
            time_check_next = time_check_next.strftime("%d.%m. %H:%M")
        except:
            time_check_now = ""
            time_check_next = ""

        # return render_response(request, "m_vorlauftemperaturkorrektur.html",
        #                        {"eundr": eundr, "haus": haus, 'list': lst, "r_w_m_p_o": room_with_most_positive_offset,
        #                         "deviation":deviation, "offset": max_offset, "offset_limit": offset_limited,
        #                         "negative": max_absenkung, "positive": max_anhebung, "time_check_now": time_check_now,
        #                         "time_check_next": time_check_next})
        return render_response(request, "m_vorlauftemperaturkorrektur.html",
                               {"eundr": eundr, "haus": haus, 'list': lst, 'list_vtk': vtk_list,
                                "time_check_now": time_check_now,
                                "time_check_next": time_check_next})

    elif request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            if name == "anhebung":
                anhebung = request.POST['number']
                absenkung = -1
            if name == "absenkung":
                absenkung = request.POST['number']
                anhebung = -1
            raum = Raum.objects.get(id=long(request.POST.get('id', 0)))
            set_params(raum, anhebung, absenkung)
            return HttpResponse()

        else:
            absenkung = request.POST.get('absenkung', -1)
            if 'absenkung_checkbox' not in request.POST:
                absenkung = -1
            anhebung = request.POST.get('anhebung', -1)
            if 'anhebung_checkbox' not in request.POST:
                anhebung = -1
            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                set_params(raum, anhebung, absenkung)
            return render_redirect(request, "/m_vorlauftemperaturkorrektur/%s/" % haus.id)


def set_params(hausoderraum, increase, decrease):
    i, d = hausoderraum.get_vorlauftemperaturkorrektur_params()

    try:
        if float(increase) == -1:
            increase = i
    except:
        increase = i

    try:
        if float(decrease) == -1:
            decrease = d
    except:
        decrease = d

    params = hausoderraum.get_module_parameters()
    params.setdefault('vorlauftemperaturkorrektur', dict())
    params['vorlauftemperaturkorrektur']['increase'] = increase
    params['vorlauftemperaturkorrektur']['decrease'] = decrease
    hausoderraum.set_module_parameters(params)


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    if modulename == "vorlauftemperaturregelung":
        return get_name(), 'vorlauftemperaturkorrektur', offset, 75, '/m_vorlauftemperaturkorrektur/%s/' % haus.id


def get_module_variables(haus):
    haus_params = haus.get_module_parameters()
    mrs = haus_params.get('vorlauftemperaturkorrektur', dict())
    module_variables = list()
    for rl in mrs.keys():
        if not rl.isdigit():
            continue
        name = mrs[rl].get('name')
        vtk_name = name if name else 'vtk %s' % rl
        offset = get_calculated_offset(haus, rl)
        module_variables.append((rl, vtk_name, offset))

    return [{'module_name': 'vorlauftemperaturkorrektur',
                            'variables': module_variables,
                            'verbose_module_name': get_name()}]


def get_jsonapi(haus, usr, entityid=None):
    
    ret = {}

    if entityid is None:
        vtk_haus_params = haus.get_spec_module_params('vorlauftemperaturkorrektur')
        for _id, rparams in vtk_haus_params.items():
            if _id.isdigit():
                offset = get_calculated_offset(haus, _id)
                ret[_id] = {'name': rparams.get('name', ''), 'value': offset}
    
    else:
        offset = get_calculated_offset(haus, entityid)
        ret[entityid] = {'value': offset}
    
    return ret
