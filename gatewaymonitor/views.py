# -*- coding: utf-8 -*-

from heizmanager.models import RPi, Haus, Sensor, Gateway, GatewayAusgang, AbstractSensor, Regelung
from rf.models import RFSensor, RFAktor
from heizmanager.render import render_response, render_redirect
import logging
from django.http import HttpResponse
import json
import pytz
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from heizmanager.models import sig_new_output_value, sig_new_temp_value
from django.views.decorators.csrf import csrf_exempt
from heizmanager.mobile.m_temp import get_module_offsets_regelung, get_module_line_list_regelung
from benutzerverwaltung.decorators import has_regelung_access
import operator
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from itertools import chain


logger = logging.getLogger('logmonitor')


def get_name():
    return u"Gateway Monitor"


def managesoutputs():
    return False


def do_ausgang(regelung, ausgangno):
    haus = Haus.objects.all()[0]
    regparams = regelung.get_parameters()
    if not 'resetMarker' in regparams:
        regparams['resetMarker'] = False

    if regparams['manReset']:
        # falls man marker 1 dann marker entfernen und 0 zurückgeben
        logger.warning("0|%s: Manueller Reset wird durchgeführt" % str(regparams['name']))
        regparams['manReset'] = False
        regparams['resetMarker'] = True
        regelung.set_parameters(regparams)
        return HttpResponse("<1>")
    if regparams['resetMarker']:
        # falls bereits zuvor auf 1, dann wieder auf 0 stellen
        regparams['resetMarker'] = False
        regelung.set_parameters(regparams)
        logger.warning("0|%s: Reset beendet" % regparams['name'])
        #regparams = regelung.get_parameters()
        return HttpResponse("<0>")
    # todo schauen, wann gw letztes mal online, falls > 4 min, gib 1 zurück
    # setzte marker falls 1
    gw = None
    for g in Gateway.objects.all():
        if g.name == regparams['mac'].lower():
            gw = g
    if gw == None:
        logger.warning("Gateway Monitor: Gateway existiert nicht")
        return HttpResponse("<0>")

    last = ch.get_gw_ping(gw.name)

    if last is None: #if gw never send anything, do nothing
        #logger.warning("GW not send anything yet, do 0")
        return HttpResponse("<0>")
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)
    if len(last) and (now-last[0]).seconds > 240:
        regparams['resetMarker'] = True
        regelung.set_parameters(regparams)
        logger.warning("0|%s: Automatischer Reset wird durchgeführt" % str(regparams['name']))
        return HttpResponse("<1>")
    return HttpResponse("<0>")


def get_outputs(haus, raum, regelung):
    belegte_ausgaenge = dict()
    ret = sig_get_outputs_from_output.send(sender='gatewaymonitor', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            if regelung.regelung != "gatewaymonitor":
                continue
            belegte_ausgaenge.setdefault(gw, list())
            params = haus.get_module_parameters()
            gwmreg = regelung.get_parameters()
            for name in namen:
                try:
                    n = int(name.strip())
                except:
                    n = name
                belegte_ausgaenge[gw].append((n, ausgang, ("gwmreg_%s" % str(regelung.id), u"Gateway Monitor %s: %s" % (gwmreg['name'],gwmreg['mac'])), True))
    return belegte_ausgaenge


def get_possible_outputs(haus):
    ctrldevices = sig_get_ctrl_devices.send(sender='fps', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)

    for reg in  Regelung.objects.filter(regelung='gatewaymonitor'):
        gwmparam = reg.get_parameters()
        for cd in regler.keys():
            regler[cd].append(["gwmreg_%s" % str(reg.id), u"Gateway Monitor %s: %s" % (gwmparam['name'], gwmparam['mac'])])
    return regler


def get_config(request, macaddr):
    if request.method == 'GET':
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)
        #cachedValue = ch.get("gwm_%s" % macaddr)
        #if cachedValue is not None:
        #    return HttpResponse(json.dumps(cachedValue))

        ret = {}
        # haus = gw.haus
        for ausgang in GatewayAusgang.objects.filter(gateway=gw):
            reg = ausgang.regelung
            if reg.regelung != 'gatewaymonitor':
                continue
            for ausgangno in [int(_o) for _o in reg.gatewayausgang.ausgang.split(',')]:
                try:
                    val = int(do_ausgang(reg, ausgangno).content.translate(None, "<>"))
                except Exception as e:
                    logger.warning("%s|%s" % (reg.raum.id, u"Fehler: Ausgang an %s konnte nicht gesetzt werden, steht auf 0." % macaddr))
                else:
                    ret.update({str(ausgangno): val})
        #ch.set("gwm_%s" % macaddr, ret,600)
        return HttpResponse(json.dumps(ret))
    else:
        return HttpResponse(405)

@csrf_exempt
def set_status(request, macaddr):
    return HttpResponse()


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/gatewaymonitor/'>Gateway Monitor</a>" % str(haus.id)


def get_global_description_link():
    desc = u"Überwacht den Online-Status eines Gateways/Regelgeräts"
    desc_link = "https://support.controme.com/gateway-monitor/"
    return desc, desc_link

def get_global_settings_page(request, haus, action=None, entityid=None):
    if request.method == "GET":
        try:
            if not isinstance(haus, Haus):
                haus = Haus.objects.get(pk=long(haus))
            if request.GET.get('type') == "FireMonitor":
                ret = {}
                gwmreg_id = request.GET.get('val')
                for reg in  Regelung.objects.filter(regelung='gatewaymonitor'):
                    if str(reg.id) == gwmreg_id:
                        gwmparam = reg.get_parameters()
                        gwmparam['manReset'] = True
                        reg.set_parameters(gwmparam);
                        return HttpResponse(json.dumps(ret), content_type='application/json')
                ret['error'] = 'Monitor nicht gefunden.'
                return HttpResponse(json.dumps(ret), content_type='application/json')
            
            if action == 'delete':
                try:
                    reg =Regelung.objects.filter(id=entityid)[0]                      
                    reg.delete()
                    #return HttpResponse(json.dumps(ret), content_type='application/json')
                except ValueError:
                    ret['error'] = 'Monitor nicht gefunden'
                    return HttpResponse(json.dumps(ret), content_type='application/json')
                #return HttpResponse(json.dumps(ret), content_type='application/json')
                
                
                
                
            if action == "edit":
                gwList = list()
                for gw in Gateway.objects.filter(haus=haus):
                    gwList.append({'name': gw.name,'description': gw.description})
                ret = dict(haus=haus)
                if entityid == None: # create new one
                    return render_response(request, "m_settings_gatewaymonitor_edit.html", {'gws': gwList,  'mac': '', 'haus' : haus, 'name' : "",'alive' : True, 'sensor' : True})
                else: # edit existing one
                    # todo get values for entity
                    reg = Regelung.objects.filter(id=entityid)
                    regparams = reg[0].get_parameters()
                    return render_response(request, "m_settings_gatewaymonitor_edit.html", {'gws': gwList, 'mac': regparams['mac'],  'haus' : haus, 'name' : regparams['name'],'gmregid' : entityid, 'alive' : regparams['active'], 'sensor' : regparams['sensor']})
            try:
                gwmdict = dict()
                for reg in Regelung.objects.filter(regelung='gatewaymonitor'):
                    regparams = reg.get_parameters()
                    gwmdict[str(reg.id)] = {'id' : str(reg.id), 'name': regparams['name']}
                return render_response(request, "m_settings_gatewaymonitor.html", {'haus': haus, 'gwmreg': sorted(gwmdict.iteritems()), 'ha' : 'dddd1'})
            except Exception as e:
                return render_redirect(request, 'https://support.controme.com/gateway-monitor/')
        except Exception as e:
            return render_redirect(request, 'https://support.controme.com/gateway-monitor/')
        return render_redirect(request, 'https://support.controme.com/gateway-monitor/')
    if request.method == "POST":
        try:
            if not isinstance(haus, Haus):
                haus = Haus.objects.get(pk=long(haus))                       
            elif request.POST.get('type') == 'add': # add new or update existing
                ret = {}
                reg_id = request.POST.get('gmreg', None)
                if reg_id:
                    mac = request.POST.get('mac')
                    reg =Regelung.objects.filter(id=reg_id)[0]
                    regparams = reg.get_parameters()
                    ngwmreg = reg.get_parameters()
                    ngwmreg['mac'] = mac
                    ngwmreg['name'] = request.POST.get('name')
                    check_values1 = request.POST.getlist('alive1')
                    #check_values2 = request.POST.getlist('alive2')
                    ngwmreg['active'] = '1' in check_values1
                    ngwmreg['sensor'] = False #'2' in check_values2
                    ngwmreg['manReset'] = False
                    ngwmreg['resetMarker'] = False
                    reg.set_parameters(ngwmreg)
                    reg.save()
                    gwmdict = dict()
                    for reg in Regelung.objects.filter(regelung='gatewaymonitor'):
                        regparams = reg.get_parameters()
                        gwmdict[str(reg.id)] = {'id' : str(reg.id), 'name': regparams['name']}
                    return render_response(request, "m_settings_gatewaymonitor.html", {'haus': haus, 'gwmreg': sorted(gwmdict.iteritems()), 'ha' : 'dddd1'})
                    
                    pass
                else:
                    mac = request.POST.get('mac')
                    for reg in Regelung.objects.filter(regelung='gatewaymonitor'):
                        regparams = reg.get_parameters()
                        if regparams['mac'] == mac:
                            ret['error'] = 'Für Gateway existiert bereits ein Monitor'
                            return HttpResponse(json.dumps(ret), content_type='application/json')

                    ngwmreg = dict()
                    ngwmreg['mac'] = mac
                    ngwmreg['name'] = request.POST.get('name')
                    check_values1 = request.POST.getlist('alive1')
                    check_values2 = request.POST.getlist('alive2')
                    ngwmreg['active'] = '1' in check_values1
                    ngwmreg['sensor'] = '2' in check_values2

                    ngwmreg['manReset'] = False
                    ngwmreg['resetMarker'] = False
                    reg = Regelung(regelung='gatewaymonitor', parameter=ngwmreg)
                    reg.save()
                    gwmdict = dict()
                    for reg in Regelung.objects.filter(regelung='gatewaymonitor'):
                        regparams = reg.get_parameters()
                        gwmdict[str(reg.id)] = {'id' : str(reg.id), 'name': regparams['name']}
                    return render_response(request, "m_settings_gatewaymonitor.html", {'haus': haus, 'gwmreg': sorted(gwmdict.iteritems()), 'ha' : 'dddd1'})

                
                
        except Exception as e:
            logging.exception("Exception in Gateway-Monitor")
        return render_redirect(request, 'https://support.controme.com/gateway-monitor/')



def get_local_settings_page_haus(request, haus, action=None, objid=None):
    return render_redirect(request, 'https://support.controme.com/gateway-monitor/')


def get_name():
    return u'Gateway Monitor'

def get_cache_ttl():
    return 3600


def get_params(hausoderraum):
    return 0


def set_params(hausoderraum, active):
    pass


def calculate_always_anew():
    return False


def is_hidden_offset():
    return False


def is_togglable():
    return True

def activate(hausoderraum):
    if not 'gatewaymonitor' in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['gatewaymonitor'])
