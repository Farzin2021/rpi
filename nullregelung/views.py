# -*- coding: utf-8 -*-

from heizmanager.models import RPi, Haus, Sensor, Gateway, GatewayAusgang, AbstractSensor, Regelung
from rf.models import RFSensor, RFAktor
from heizmanager.render import render_response, render_redirect
import logging
from django.http import HttpResponse
import json
import pytz
from datetime import datetime, timedelta
import heizmanager.cache_helper as ch
from heizmanager.models import sig_new_output_value, sig_new_temp_value
from django.views.decorators.csrf import csrf_exempt
from heizmanager.mobile.m_temp import get_module_offsets_regelung, get_module_line_list_regelung
from benutzerverwaltung.decorators import has_regelung_access
import operator
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from itertools import chain


logger = logging.getLogger('logmonitor')


def get_name():
    return u"Nullregelung"


def managesoutputs():
    return False


def do_ausgang(regelung, ausgangno):
    return HttpResponse("<1>")


def get_outputs(haus, raum, regelung):
    belegte_ausgaenge = dict()
    ret = sig_get_outputs_from_output.send(sender='nullregelung', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            if regelung.regelung != "nullregelung":
                continue
            belegte_ausgaenge.setdefault(gw, list())
            for name in namen:
                try:
                    n = int(name.strip())
                except:
                    n = name
                belegte_ausgaenge[gw].append((n, ausgang, ("nullreg_%s" % str(regelung.id), u" --- für Testzwecke: Relais EIN ---"), True))
    return belegte_ausgaenge


def get_possible_outputs(haus):
    ctrldevices = sig_get_ctrl_devices.send(sender='nullreg', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)
    reg = None
    regs = Regelung.objects.filter(regelung='nullregelung')
    available_outs = dict()
    for r in regs:
        params = r.get_parameters()
        if params.get("gw_id", "") != "":
            available_outs[params["gw_id"]] = r
    #jedem gw eine regelung zuordnen. falls noch nicht vorhanden, anlegen

    for cd in regler.keys():
        if cd.id in available_outs:
            regler[cd].append(["nullreg_%s" % str(available_outs[cd.id].id), u" --- für Testzwecke: Relais EIN ---"])
        else:
            reg = Regelung(regelung="nullregelung")
            nd = dict()
            nd["gw_id"] = cd.id # reg.get_parameters() key festlegen für gw_id
            reg.set_parameters(nd)
            reg.save()
            regler[cd].append(["nullreg_%s" % str(reg.id), u" --- für Testzwecke: Relais EIN ---"])
    return regler


def get_config(request, macaddr):    
    if request.method == 'GET':
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)
        cachedValue = ch.get("nr_%s" % macaddr)
        if cachedValue is not None:
            return HttpResponse(json.dumps(cachedValue))
        ret = {}
        # haus = gw.haus
        for ausgang in GatewayAusgang.objects.filter(gateway=gw):
            reg = ausgang.regelung
            if reg.regelung != 'nullregelung':
                continue
            for ausgangno in [int(_o) for _o in reg.gatewayausgang.ausgang.split(',')]:
                try:
                    val = int(do_ausgang(reg, ausgangno).content.translate(None, "<>"))
                except Exception as e:
                    logger.warning("-|%s" % (u"Fehler: Ausgang an %s konnte nicht gesetzt werden, steht auf 1." % macaddr))
                else:
                    ret.update({str(ausgangno): val})

        ch.set("nr_%s" % macaddr, ret,600)
        return HttpResponse(json.dumps(ret))
    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):
    return HttpResponse()


def get_global_settings_link(request, haus):
    pass


def get_global_description_link():
    desc = None
    desc_link = None
    return desc, desc_link

def get_global_settings_page(request, haus, action=None, entityid=None):
   pass


def get_local_settings_page_haus(request, haus, action=None, objid=None):
  pass

def get_name():
    return u'Nullregelung'

def get_cache_ttl():
    return 3600


def get_params(hausoderraum):
    pass


def set_params(hausoderraum, active):
    pass

def activate(hausoderraum):
    pass
