# -*- coding: utf-8 -*-
from heizmanager.render import render_response, render_redirect
from heizmanager.models import AbstractSensor, Raum, Haus, Sensor
from rf.models import RFAktor, RFSensor
from vsensoren.models import VSensor


def get_cache_ttl():
    return 1800


def is_togglable():
    return True


def is_hidden_offset():
    return False


def calculate_always_anew():
    return False


def get_offset(haus, raum=None):
    pass


def get_offset_regelung(haus, module, objid):
    pass


def get_name():
    return 'Kaminofen'


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/kaminofen/'>Kaminofen / zusätzliche Wärmequelle</a>" % haus.id


def get_global_description_link():
    desc = u"Bei aktiver Zusatzheizung wird der Fußbodenheizung-Auskühlschutz aktiviert."
    desc_link = "http://support.controme.com/modul-kaminofen/"
    return desc, desc_link


def get_local_settings_link(request, raum):
    pass


def activate(obj):
    if 'kaminofen' not in obj.get_modules():
        obj.set_modules(obj.get_modules() + ['kaminofen'])


def get_local_settings_page(request, raum):
    if request.method == "GET":
        pass

    if request.method == "POST":
        pass


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass


def get_global_settings_page_help(request, haus):
    return None


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    context['haus'] = haus
    k_params = haus.get_spec_module_params('kaminofen')
    if request.method == "GET":
        if action == 'edit':

            sensors = AbstractSensor.get_all_sensors_info(sort_by=["verbose_raum_name", "name"])
            sensors = [{'sensorid': sensor['sensor_id'], 'name': sensor['sensor_name'] + ' (' + sensor['sensortyp'].upper() + ') ' + sensor['raum_name'] + ((' / ' + sensor['description']) if sensor['description'] else '')} for sensor in sensors]
            context['sensors'] = sensors

            # collecting room
            eundr = list()
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                eundr.append(e)
            context['eundr'] = eundr

            if entityid:
                k_param = k_params.get(entityid)
                context['k_param'] = k_param
                context['k_id'] = entityid
                # S1
                s1 = k_param['raum_sensor']
                s1 = AbstractSensor.get_sensor(s1)
                if s1 is not None:
                    s1 = s1.get_wert()
                    if s1 is not None:
                        s1 = s1[0]
                        context['s1'] = s1
                    else:
                        s1 = None
                # S2
                s2 = k_param['kaminofen_sensor']
                s2 = AbstractSensor.get_sensor(s2)
                if s2 is not None:
                    s2 = s2.get_wert()
                    if s2 is not None:
                        s2 = s2[0]
                        context['s2'] = s2
                    else:
                        s2 = None

                # T1
                t1 = k_param['differenztemperatur']
                if s1 and s2:
                    context['diff'] = "%s / %s" % (t1, s2-s1) if s1 and s2 else '-'

                context['last_check'] = k_param.get('last_check', '')
                context['next_check'] = k_param.get('next_check', '')
                context['active'] = 'Aktiv' if k_param.get('active') else 'AUS'
            return render_response(request, "m_settings_kaminofen_edit.html", context)

        if action == 'delete':
            del k_params[entityid]
            haus.set_spec_module_params('kaminofen', k_params)

        context['k_params'] = k_params.items()
        return render_response(request, "m_settings_kaminofen.html", context)

    if request.method == "POST":

        # preparing data
        data_d = dict()
        data_d['name'] = request.POST.get('name', '')
        data_d['differenztemperatur'] = float(request.POST.get('differenztemperatur', 3))
        data_d['raum_sensor'] = request.POST.get('raum_sensor')
        data_d['kaminofen_sensor'] = request.POST.get('kaminofen_sensor')
        rooms_lst = request.POST.getlist('rooms')
        data_d['rooms'] = rooms_lst

        # get existing data or define new id
        k_params = haus.get_spec_module_params('kaminofen')
        if entityid is None:
            ids = [0] + [int(k) for k in k_params.keys()]
            entityid = str(max(ids) + 1)
            k_params[entityid] = dict()

        # saving data
        k_params[entityid].update(data_d)
        haus.set_spec_module_params('kaminofen', k_params)
        return render_redirect(request, '/m_setup/%s/kaminofen/' % haus.id)


def get_local_settings_link_haus(request, haus):
    pass


def get_local_settings_page_regelung(request, haus, modulename, objid, params):

    if request.method == "GET":
        pass

    elif request.method == "POST":
        pass


def get_local_settings_link_regelung(haus, modulename, objid):
    pass
