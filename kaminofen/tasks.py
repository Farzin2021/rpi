# -*- coding: utf-8 -*-
from celery import shared_task
from heizmanager.models import Haus, AbstractSensor, Raum
import pytz
from datetime import datetime, timedelta


def get_result(s1, s2, t1):

    try:
        return AbstractSensor.get_sensor(s2).get_wert()[0] - AbstractSensor.get_sensor(s1).get_wert()[0] > t1
    except:
        return None


def do_process(haus):
    k_params = haus.get_spec_module_params('kaminofen')

    activate_rooms = list()
    deactivate_rooms = list()
    for k_id, k_param in k_params.items():

        berlin = pytz.timezone("Europe/Berlin")
        now = datetime.now(berlin)
        k_params[k_id]['last_check'] = now.strftime('%Y-%m-%d %H:%M')
        k_params[k_id]['next_check'] = (now + timedelta(minutes=10)).strftime('%Y-%m-%d %H:%M')

        s1 = k_param['raum_sensor']
        s2 = k_param['kaminofen_sensor']
        t1 = float(k_param['differenztemperatur'])
        result = get_result(s1, s2, t1)
        if result is None:
            haus.set_spec_module_params('kaminofen', k_params)
            return

        if result:
            activate_rooms += k_param['rooms']
            k_params[k_id]['active'] = True
        else:
            deactivate_rooms += k_param['rooms']
            k_params[k_id]['active'] = False

    haus.set_spec_module_params('kaminofen', k_params)

    activate_rooms = set(activate_rooms)
    deactivate_rooms = set(deactivate_rooms) - set(activate_rooms)

    # activate in rooms
    for raumid in activate_rooms:
        raum = Raum.objects.get(id=raumid)
        r_k_params = raum.get_spec_module_params('kaminofen')
        hfo_params = raum.get_hfo_params()
        if 'kept_after' not in r_k_params:
            val_to_keep = hfo_params.get('after', .5)
            hfo_params['after'] = 5.0
            r_k_params['kept_after'] = val_to_keep
        raum.set_spec_module_params('kaminofen', r_k_params)
        raum.set_spec_module_params('heizflaechenoptimierung', hfo_params)

    # deactivate in rooms
    for raumid in deactivate_rooms:
        raum = Raum.objects.get(id=raumid)
        r_k_params = raum.get_spec_module_params('kaminofen')
        hfo_params = raum.get_spec_module_params('heizflaechenoptimierung')
        if 'kept_after' in r_k_params:
            hfo_params['after'] = r_k_params['kept_after']
            del r_k_params['kept_after']
        raum.set_spec_module_params('kaminofen', r_k_params)
        raum.set_spec_module_params('heizflaechenoptimierung', hfo_params)


@shared_task()
def periodic_kaminofen_calculation():
    for haus in Haus.objects.all():
        # module activation check
        if 'kaminofen' in haus.get_modules():
            do_process(haus)
