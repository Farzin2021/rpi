from django.test import TestCase, Client
from heizmanager.models import Haus, HausProfil, Etage, Raum, Regelung
from django.contrib.auth.models import User
from django.core.cache import cache as memcache
import logging
from datetime import datetime, timedelta
from heizmanager import pytz
import views as gcal
from heizmanager.mobile.m_temp import get_module_offsets
from raumgruppen.models import Raumgruppe


class GCalTest(TestCase):

    def setUp(self):
        self.usr = User.objects.create_user(username='test@test.de', email='test@test.de', password='test')
        self.usr.save()
        self.haus = Haus(name="Teststr. 1, 12345 Testhausen", eigentuemer=self.usr)
        self.haus.save()
        hausprofil = HausProfil(haus=self.haus, modules='gcal', module_parameters=str({'gcal': {'calendarId': '123'}}))
        hausprofil.save()
        etage = Etage(name='testetage', haus=self.haus, position=0)
        etage.save()
        self.raum1 = Raum(name='testraum1', etage=etage, position=0, solltemp=21.0, module_parameters='{}', modules='gcal')
        self.raum1.save()
        self.raum2 = Raum(name='testraum2', etage=etage, position=0, solltemp=21.0, module_parameters='{}', modules='gcal')
        self.raum2.save()
        self.raum3 = Raum(name='testraum3', etage=etage, position=0, solltemp=21.0, module_parameters='{}', modules='gcal')
        self.raum3.save()
        self.raum4 = Raum(name='testraum4', etage=etage, position=0, solltemp=21.0, module_parameters='{}', modules='gcal')
        self.raum4.save()

        self.reg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.reg.save()
        self.zrreg = Regelung(regelung='zweipunktregelung', parameter=str({'delta': 0.0, 'active': True}))
        self.zrreg.save()
        self.raum1.regelung = self.reg
        self.raum1.save()
        self.raum2.regelung = self.zrreg
        self.raum2.save()

        memcache._cache.flush_all()

        self.client = Client()
        self.client.login(username='test@test.de', password='test')

        gmt = pytz.timezone('GMT')
        self.now = datetime.now(gmt)

    def testGetOffset(self):
        events = {
            '1': ('-1 testetage testraum1', self.now-timedelta(hours=1), self.now+timedelta(hours=1)),
            '2': ('+2', self.now-timedelta(hours=1), self.now+timedelta(hours=1))
        }
        memcache.set('%sevents' % self.haus.pk, events)
        ret = gcal.get_offset(self.haus, self.raum1)
        self.assertEqual(ret, -1.0)
        ret = gcal.get_offset(self.haus)
        self.assertEqual(ret, +2.0)
        self.assertEqual(22.0, self.raum1.solltemp+get_module_offsets(self.raum1))  # zieltemp in zpr

        ret = gcal.get_offset(self.haus, raum=None, return_matches=True)
        logging.warning(ret)

    def testRG(self):
        response = self.client.post('/m_setup/%s/raumgruppen/' % self.haus.pk,
                                    {'rgcreate': 'true', 'rsel1': 'on', 'rsel2': 'on', 'name': 'testrg'})
        self.assertEqual(len(Raumgruppe.objects.all()), 1)
        events = {
            '1': ('-1.5 testrg', self.now-timedelta(hours=1), self.now+timedelta(hours=1)),
        }
        memcache.set('%sevents' % self.haus.pk, events)
        ret = gcal.get_offset(self.haus, self.raum1)
        self.assertEqual(ret, -1.5)
        ret = gcal.get_offset(self.haus, self.raum2)
        self.assertEqual(ret, -1.5)
        ret = gcal.get_offset(self.haus, self.raum3)
        self.assertEqual(ret, 0.0)
        ret = gcal.get_offset(self.haus, self.raum4)
        self.assertEqual(ret, 0.0)

    def testGetEvents(self):
        # TODO: this
        # kann man hier auch checken, ob und wie sich das GCal Format geaendert hat?
        pass