# -*- coding: utf-8 -*-

from rf.models import RFAktor, RFSensor
from django.views.decorators.csrf import csrf_exempt
from heizmanager.render import render_response, render_redirect
from django.http import HttpResponse
import json
import logging
from heizmanager.models import Regelung, Gateway, AbstractSensor, GatewayAusgang, Raum, Haus, Sensor
from parser import parse
import heizmanager.cache_helper as ch
import copy
from datetime import datetime, timedelta, time
from heizmanager.mobile.m_temp import get_module_offsets
from benutzerverwaltung.decorators import has_regelung_access
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
import pytz
import re
import importlib
from vsensoren.models import VSensor
from knx.models import KNXSensor

# todo wie kann ich einen Ausgang auf 1 schalten, wenn ein Kriterium nicht erfüllt ist?
# -> (False and 100) or (not False) == True
# => (True and 100) or (not True) == 100 (oder eben jeder andere Wert einschl. 0)


logger = logging.getLogger('logmonitor')


def is_hidden_offset():
    return False


def get_cache_ttl():
    return 0


def calculate_always_anew():
    return True


def get_name():
    return u'FPS'


def do_ausgang(regelung, ausgangno):

    sget = ch.get('regid%s_%s' % (regelung.id, ausgangno))
    if sget is not None:
        return HttpResponse("<%s>" % sget)

    haus = Haus.objects.all()[0]
    try:
        val = evaluate_fps(haus, regelung)
    except RuntimeError:
        val = regelung.get_parameters()['error_value']

    if val is None:
        return HttpResponse("<%s>" % regelung.get_parameters()['error_value'])

    ch.set("regid%s_%s" % (regelung.id, ausgangno), val, time=50)

    if not str(ausgangno).isdigit() or int(ausgangno) == 0:
        return HttpResponse("<%s>" % val)
    else:
        for ausgang in [int(_o) for _o in regelung.gatewayausgang.ausgang.split(',')]:
            if ausgang == int(ausgangno):
                return HttpResponse("<%s>" % val)
        else:
            logging.error("falsche fps. der ausgang gehoert nicht hierher.")
            return HttpResponse("<%s>" % regelung.get_parameters()['error_value'])


def do_rfausgang(regelung, controllerid):
    ret = do_ausgang(regelung, 0).content
    return int(ret[1]) if ret[1] in ["0", "1"] else 0


def managesoutputs():
    return False


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()

    ret = sig_get_outputs_from_output.send(sender='fps', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:
            if regelung.regelung != "fps":
                continue
            belegte_ausgaenge.setdefault(gw, list())
            regparams = regelung.get_parameters()
            for name in namen:
                try:
                    n = int(name.strip())
                except:
                    n = name
                belegte_ausgaenge[gw].append((n, ausgang, ("fps_%s" % regelung.id, regparams['name']), True))

    return belegte_ausgaenge


def get_possible_outputs(haus):

    # todo kann auch analog ausgaenge ansteuern

    ctrldevices = sig_get_ctrl_devices.send(sender='fps', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)
    regs = Regelung.objects.filter(regelung="fps")
    for reg in regs:
        params = reg.get_parameters()
        if params.get('mode') == 'offset':
            continue
        for cd in regler.keys():
            regler[cd].append(["fps_%s" % reg.id, u"FPS %s: %s" % (reg.id, params['name'])])

    return regler


def get_offset(haus, raum=None):
    if raum is None:
        return 0.0

    offset = 0.0
    for fps in Regelung.objects.filter(regelung='fps'):
        params = fps.get_parameters()
        if params.get('mode') == 'offset' and raum.id in params.get('raeume', []):
            try:
                val = evaluate_fps(haus, fps)
            except RuntimeError:
                val = raum.regelung.get_parameters()['error_value']
            offset += val or 0

    return offset


def get_offset_regelung(haus, module=None, objid=None):
    if haus is None or module is None or objid is None:
        return 0.0

    offset = 0.0
    for fps in Regelung.objects.filter(regelung='fps'):
        params = fps.get_parameters()
        if params.get('mode') == 'offset' and ('dr_%s' % objid in params.get('offset_regs', []) or 'mr_%s' % objid in params.get('offset_regs', [])):
            try:
                val = evaluate_fps(haus, fps)
            except RuntimeError:
                val = params.get('error_value')
            offset += val

    return offset


def get_local_settings_link(request, raum):

    offset = get_offset(raum.etage.haus, raum)
    return "FPS", "%.2f" % offset, 75, "#"


def get_local_settings_link_regelung(haus, modulename, objid):
    offset = get_offset_regelung(haus, modulename, objid)
    return "FPS", "FPS", offset, 75, "#"


def get_modules_variables(haus):

    activated_mods = haus.get_modules()
    variables = []
    mvmods = ['wetter_pro', 'jahreskalender', 'vorlauftemperaturkorrektur'] # ordering is important here!
    for mod in mvmods:
        if mod not in activated_mods:
            continue
        try:
            module = importlib.import_module(mod.strip() + '.views')
        except ImportError:
            continue
        try:
            module_vars = module.get_module_variables(haus)
            variables += module_vars
        except Exception:
            logging.exception("exc")
            continue
    return variables


def get_modvarname2value(haus):
    modules_vars = get_modules_variables(haus)
    ret = []
    for mv in modules_vars:
        for v in mv['variables']:
            ret.append(("%s_%s" % (mv['module_name'], v[0]), v[2]))
    return ret


def get_config(request, macaddr):
    # todo das hier muss fuer das hrgw unterscheiden, ob es selber berechnen
    # todo  kann oder ob das ergebenis immer von hier kommen muss

    # todo fürs erste: immer hier berechnen!
    if request.method == 'GET':

        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)

        ret = {}
        haus = gw.haus
        for reg in Regelung.objects.filter(regelung="fps"):

            # erst mal nur diejenigen FPS mit zugeordnetem Ausgang
            if reg.get_ausgang() and reg.get_ausgang().gateway == gw:
                try:
                    val = evaluate_fps(haus, reg)
                except RuntimeError:
                    val = reg.get_parameters()['error_value']
                if val is None:
                    val = reg.get_parameters()['error_value']
                for ausgang in [int(_o) for _o in reg.gatewayausgang.ausgang.split(',')]:
                    if ausgang in {13, 14}:
                        ret.update({str(ausgang): val})
                    else:
                        ret.update({str(ausgang): 1 if val > 0 else 0})

        ch.set("fps_%s" % macaddr, ret)
        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):
    if request.method == "POST":
        data = json.loads(request.body)
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            pass
        else:
            if 'error' in data:
                ausgaenge = GatewayAusgang.objects.filter(gateway=gw)
                for ausgang in ausgaenge:
                    if ausgang.regelung and ausgang.regelung.regelung == "fps":
                        logger.warning("%s|%s" % (ausgang.regelung.id, data['error']))

    return HttpResponse()


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/fps/'>FPS</a>" % haus.id


def get_global_description_link():
    desc = u"Frei programmierbare Logikverknüpfungen."
    desc_link = "http://support.controme.com/frei-programmierbare-steuerung/"
    return desc, desc_link


def _parse_evaluation_request(formula):

    ret = {}

    if "last_a0" in formula:
        ret['last_a0'] = None
    pattern = re.compile('raum_\d+_[a-z_]+')
    for var in pattern.findall(formula):
        ret[var] = None
    pattern = re.compile('vtr_\d+_ziel')
    for var in pattern.findall(formula):
        ret[var] = None
    if "tags" in formula:
        ret['tags'] = None
    if "sonnenaufgang" in formula:
        ret['sonnenaufgang'] = None
    if "sonnenuntergang" in formula:
        ret['sonnenuntergang'] = None
    if "now" in formula:
        ret['now'] = None
    pattern = re.compile('vtr_\d+_position')
    for var in pattern.findall(formula):
        ret[var] = None
    pattern = re.compile('vtk_\d+')
    for var in pattern.findall(formula):
        ret[var] = None

    return ret


def evaluate_fps(haus, regelung):
    params = regelung.get_parameters()
    formula = params['formula']
    variables = copy.deepcopy(params['variables']['sensors'])
    variables.update(params['variables']['outs'])
    variables.update(params['variables'].get('vars', {}))
    if 'modulevariables' in params['variables']:
        variables.update(params['variables']['modulevariables'])
    elif 'WetterPro_wetter-pro' in params['variables']:  # legacy workaround
        params['variables']['modulevariables'] = dict((name.lower(), 'wetterpro_%s' % _id) for name, _id in params['variables']['WetterPro_wetter-pro'].items())
        regelung.set_parameters(params)
        variables.update(params['variables']['modulevariables'])

    variables.update({'hysterese': float(params.get('hysterese', 0)), 'fpsid': regelung.id})
    variables.update(_parse_evaluation_request(formula))

    varvals = ""
    try:
        val = _evaluate_formula(haus, formula, variables)

        if 'error' not in val.keys():

            if isinstance(val['A0'], float) or isinstance(val['A0'], int):
                try:
                    _varvals = []
                    for k, v in sorted(val.items()):
                        if k in ['A0', 'fpsid', 'hysterese', 'tags', 'a0']:
                            continue
                        _varvals.append("%s = %s" % (k.upper(), ("%.2f" % v) if isinstance(v, float) else v))
                    varvals = ", ".join(_varvals)
                    varvals += "."
                except:
                    pass
                val = val['A0']
                if isinstance(val, float):
                    val = float("%.2f" % val)
            else:
                logger.warning("%s|%s" % (regelung.id, u"FPS %s: Fehler. %s" % (params.get('name', '-'), val['A0'] if 'A0' in val and val['A0'] is not None else "")))
                val = int(params['error_value'])

        else:
            logger.warning("%s|%s" % (regelung.id, u"FPS %s: Fehler. %s" % (params.get('name', '-'), val['error'])))
            val = int(params['error_value'])

    except RuntimeError as e:
        raise e

    except Exception as e:
        logging.exception("error evaluating formula")
        val = None  # params['error_value']

    try:
        einvzg = int(params.get('einvzg', 0))
    except:
        params['einvzg'] = 0
        regelung.set_parameters(params)
        einvzg = 0

    try:
        ausvzg = int(params.get('ausvzg', 0))
    except:
        params['ausvzg'] = 0
        regelung.set_parameters(params)
        ausvzg = 0

    last = ch.get("fpslast_%s" % regelung.id)
    if last is not None and len(last) != 2:
        last = None
    if val > 0 or val < 0:

        if last:

            if last[1] == 0:  # einvzg greift gerade
                # cache neu setzen mit alten werten
                ch.set("fpslast_%s" % regelung.id, last)
            else:
                einvzg = 0

            if datetime.utcnow() > (last[0] + timedelta(seconds=einvzg)):
                logger.warning("%s|%s" % (regelung.id, u"FPS %s: %s. %s" % (params.get('name', '-'), val, varvals)))

            else:
                logger.warning("%s|%s" % (regelung.id, u"FPS %s: 0 (Einschaltverzögerung). %s" % (params.get('name', '-'), varvals)))
                return 0

        else:
            # logger.warning("%s|%s" % (regelung.id, u"FPS %s: 0 (Einschaltverzögerung*)." % (params.get('name', '-'))))
            ch.set("fpslast_%s" % regelung.id, (datetime.utcnow(), val))
            return val

    elif val == 0:

        if last:

            if last[1] != 0:  # ausvzg greift gerade
                # cache neu setzen mit alten werten
                ch.set("fpslast_%s" % regelung.id, last)
            else:
                ausvzg = 0  # weil bei abgelaufener ausvzg ja ein neuer timestamp gesetzt wird

            if datetime.utcnow() > (last[0] + timedelta(seconds=ausvzg)):
                logger.warning("%s|%s" % (regelung.id, u"FPS %s: 0. %s" % (params.get('name', '-'), varvals)))

            else:
                logger.warning("%s|%s" % (regelung.id, u"FPS %s: %s (Ausschaltverzögerung). %s" % (params.get('name', '-'), last[1], varvals)))
                return last[1]

        else:
            # logger.warning("%s|%s" % (regelung.id, u"FPS %s: 1 (Ausschaltverzögerung*)." % (params.get('name', '-'))))
            ch.set("fpslast_%s" % regelung.id, (datetime.utcnow(), 0))
            return 0

    else:
        logger.warning("%s|%s" % (regelung.id, u"FPS %s: %s (Fehler)" % (params.get('name', ''), params.get('error_value'))))

    ch.set('fpslast_%s' % regelung.id, (datetime.utcnow(), val))

    return val


def _get_sun(hparams):
    wetterparams = hparams.get("wetter", {})
    lat, lng = wetterparams.get('lat'), wetterparams.get('lng')
    try:
        if not lat or not lng or (not float(lat) and not float(lng)):
            raise Exception("lat/lng exc")
        from fps.sun import Sun
        return Sun(float(lat), float(lng))
    except Exception:
        logging.exception("_get_sun")
        return None


def _evaluate_formula(haus, formula, variables):

    modules_variables = get_modvarname2value(haus)

    hparams = haus.get_module_parameters()
    for name, objid in variables.items():

        if name not in formula.lower() and name != "hysterese" and name != "fpsid":
            # nichts auswerten, was nicht auch in der formel steht, zb ueberzaehlige sensoren
            del variables[name]
            continue

        if name[0].upper() == 'A' and name[1:].isdigit():

            # im moment nur dr/mr, also status aus cache holen
            if objid.startswith('mr_'):
                hrgw = hparams.get('vorlauftemperaturregelung', dict()).get(objid[3:], dict()).get('hrgw')
                if hrgw is None:
                    return {"error": u"Gelöschter/unzugeordneter Ausgang verwendet"}
                elif hrgw == '0' or hrgw == 0:
                    try:
                        reg = Regelung.objects.get(regelung="vorlauftemperaturregelung", parameter="{'mid': '%s'}" % objid[3:])
                    except Regelung.DoesNotExist:
                        return {"error": u"Vorlauftemperaturregelung %s nicht (mehr) vorhanden." % objid[3:]}
                    try:
                        ret = float(reg.do_ausgang(None).content.translate(None, '<>!')) / 100
                    except Exception as e:
                        logging.exception("exc")
                        ret = None
                    status = {'position': ret}
                else:
                    try:
                        hrgw = Gateway.objects.get(pk=hrgw)
                    except Gateway.DoesNotExist:
                        status = {}
                    else:
                        status = ch.get("mischer_%s" % hrgw.name)
                        if status and isinstance(status, dict):
                            status = status.get(objid[3:], dict())
                        else:
                            status = {}
            elif objid.startswith('dr_'):
                try:
                    reg = Regelung.objects.get(regelung="differenzregelung", parameter="{'dreg': '%s'}" % objid[3:])
                except Regelung.DoesNotExist:
                    return {"error": u"Gelöschter/unzugeordneter Ausgang verwendet"}
                status = ch.get("%s%sstatus" % ('differenzregelung', reg.id))
                if status:
                    status = {'position': status.get('position'), 'error': False}
                else:
                    for ausgang in GatewayAusgang.objects.filter(regelung=reg):
                        if ausgang.gateway.name.startswith(("dc-a6-32", "b8-27-eb", "e4-5f-01")):
                            status = ch.get("cc_%s" % ausgang.gateway.name)
                            try:
                                status = status.get(objid[3:], dict())
                                break
                            except Exception:
                                logging.exception("exc")
                                status = {}

                    else:
                        status = {"error": "Kein Ausgangszustand vorhanden, bitte Ausgang zuordnen."}
            elif objid.startswith('pl_'):
                try:
                    reg = Regelung.objects.get(pk=long(objid.split('_')[1]))
                except Regelung.DoesNotExist:
                    return {"error": u"Gelöschter/unzugeordneter Ausgang verwendet"}
                try:
                    val = int(reg.do_ausgang(0).content.translate(None, '<>!'))
                except:
                    val = reg.get_parameters().get('default', 1)
                status = {'position': val}
            elif objid.startswith('fps_'):
                try:
                    reg = Regelung.objects.get(pk=long(objid.split('_')[1]))
                except Regelung.DoesNotExist:
                    return {'error': u'Gelöschter/unzugeordneter Ausgang verwendet'}
                try:
                    val = float(reg.do_ausgang(0).content.translate(None, '<>!'))
                except RuntimeError as e:
                    raise e
                except Exception as e:
                    logging.exception("_evaluate_formula for %s" % objid)
                    val = reg.get_parameters().get('error_value', 1)
                status = {'position': val}
            elif objid.startswith("sp_"):
                try:
                    reg = Regelung.objects.get(pk=long(objid.split('_')[1]))
                except Regelung.DoesNotExist:
                    return {'error': u"Gelöschter/unzugeordneter Ausgang verwendet"}
                try:
                    val = int(reg.do_ausgang(0).content.translate(None, '<>!'))
                except:
                    val = 0
                status = {'position': val}
            else:
                return {"error": u"Ungültige Variable verwendet"}

            del variables[name]  # damit die lower-case deklarationen verschwinden
            if objid.startswith('fps_'):
                variables[name.upper()] = max(0, min(100, status.get('position'))) if not status.get('error', False) else None
            else:
                variables[name.upper()] = min(1, status.get('position')) if not status.get('error', False) else None  # das min castet mr/dr werte auf 1/0

        elif name[0].upper() == 'S' and name[1:].isdigit():
            sensor = AbstractSensor.get_sensor(objid)
            if sensor is None:
                return {"error": u"Gelöschter/unzugeordneter Sensor verwendet"}
            del variables[name]
            val = sensor.get_wert()
            marker = sensor.get_marker()
            variables[name.upper()] = float("%.2f" % val[0]) if val and marker == 'green' else None

        elif name[0].upper() == 'M' and name[1:].isdigit():
            for var in modules_variables:
                if var[0] == objid:
                    variables[name.upper()] = var[1]
                    del variables[name]
                    break
            else:
                return {'error': 'Modulvariable nicht (mehr) definiert'}

        elif name == '0':
            return {'error': "Variable nicht deklariert"}

        elif name.lower() == "last_a0":
            try:
                errval = int(variables['error_value'])
            except:
                errval = 0

            if variables['fpsid'] == 0:
                variables[name] = errval
            else:
                last = ch.get('fpslast_%s' % variables['fpsid'])
                if last is None:
                    last = errval
                else:
                    last = (float("%.2f" % last[1])) if isinstance(last[1], float) else last[1]
                variables[name] = last

        elif name.lower().startswith('raum_'):
            _var = name.lower().split('_')
            try:
                raum = Raum.objects.get(pk=int(_var[1]))
            except:
                return {"error": u'Unbekannter Raum: %s' % _var[1]}

            cmd = _var[2]
            if cmd == 'ziel':
                variables[name] = raum.solltemp + get_module_offsets(raum, only_clear_offsets=True, exclude='fps')
            elif cmd == 'soll':
                variables[name] = raum.solltemp
            elif cmd == 'offsets':
                variables[name] = get_module_offsets(raum, only_clear_offsets=True, exclude='fps')
            elif cmd == 'offset':
                # dann muss in _var[3] das modul sein
                offsets = get_module_offsets(raum, only_clear_offsets=True, as_dict=True, exclude='fps')
                try:
                    variables[name] = offsets.get(_var[3].lower(), 0.0)
                except IndexError:
                    return {'error': u'Bitte Modul für Offset angeben.'}
            elif cmd == 'outs':
                pass
            elif cmd == 'temp':
                variables[name] = None
                ms = raum.get_mainsensor()
                if ms:
                    last = ms.get_wert()
                    if last and ms.get_marker() == "green":
                        variables[name] = last[0]
            else:
                return {'error': u'Unbekannter Befehl in %s: %s' % (name.lower(), cmd)}

        elif name.lower().startswith('vtr_') and name.lower().endswith(("ziel", "position")):
            try:
                mid = int(name.split('_')[1])
                reg = Regelung.objects.get(parameter="{'mid': '%s'}" % mid, regelung="vorlauftemperaturregelung")
            except:
                return {'error': 'Unbekannte Vorlauftemperaturregelung.'}

            hparams = haus.get_module_parameters()
            hrgwid = hparams.get('vorlauftemperaturregelung', dict()).get(str(mid), dict()).get('hrgw')
            try:
                hrgw = Gateway.objects.get(pk=hrgwid)
            except Gateway.DoesNotExist:
                if name.lower().endswith("ziel"):
                    val = ch.get('mischer__%s' % mid)
                    if val is None:
                        val = reg.do_ausgang(None).content[1:-1]
                    try:
                        variables[name] = float(val)
                    except:
                        variables[name] = 50
                else:
                    variables[name] = None  # position nicht lokal berechnen
                # return {'error': u'Unbekannte Vorlauftemperaturregelungskonfiguration.'}
            else:
                status = ch.get("mischer_%s" % hrgw.name)
                if status:
                    status = status.get(str(mid), dict())
                    if name.lower().endswith("ziel"):
                        variables[name] = status.get('setpoint')
                    elif name.lower().endswith("position"):
                        variables[name] = status.get('position')
                else:
                    variables[name] = None

        elif name.lower().startswith("vtk_"):
            try:
                module = importlib.import_module('vorlauftemperaturkorrektur.views')
                offset = module.get_offset_regelung(haus, "fps", name[4:])
                variables[name] = offset
            except Exception as e:
                logging.exception("exc getting %s" % name)
                variables[name] = None

        elif name[0].upper() == 'V' and not name.lower().startswith(('vtr', 'vtk')) and name[1:].isdigit():
            del variables[name]
            try:
                variables[name.upper()] = float(objid)
            except:
                return {'error': u'Unzulässiger Variablenwert für %s' % name.upper()}

        elif name == 'hysterese':
            try:
                variables['hysterese'] = float(objid)
            except:
                variables['hysterese'] = 0

        elif name == 'fpsid':
            variables['fpsid'] = objid

        elif name == "tags":
            s = _get_sun(hparams)
            if s is None:
                variables['tags'] = None
                return {'error': u'Für "tags" wird eine gültige Adresse benötigt.'}

            berlin = pytz.timezone("Europe/Berlin")
            dtnow = datetime.now(berlin)
            now = time(dtnow.hour, dtnow.minute, dtnow.second)
            if s.sunrise() < now < s.sunset():
                logging.warning("%s %s %s" % (s.sunrise(), now, s.sunset()))
                variables['tags'] = True
            else:
                variables['tags'] = False

        elif name == "sonnenaufgang":
            s = _get_sun(hparams)
            if s is None:
                variables['sonnenaufgang'] = None
                return {'error': u'Für "sonnenaufgang" wird eine gültige Adresse benötigt.'}
            variables['sonnenaufgang'] = s.sunrise().hour * 60 + s.sunrise().minute

        elif name == "sonnenuntergang":
            s = _get_sun(hparams)
            if s is None:
                variables['sonnenuntergang'] = None
                return {'error': u'Für "sonnenuntergang" wird eine gültige Adresse benötigt.'}
            variables['sonnenuntergang'] = s.sunset().hour * 60 + s.sunset().minute

        elif name == "now":
            berlin = pytz.timezone("Europe/Berlin")
            dtnow = datetime.time(datetime.now(berlin))
            variables['now'] = dtnow.hour * 60 + dtnow.minute

        else:
            return {"error": u"Ungültige Variable verwendet"}

    if 'error_value' in variables:
        del variables['error_value']

    if None in variables.values():  # "not None" ergibt 1, deswegen dieser check
        err = []
        for k, v in variables.items():
            if v is None:
                err.append(k.upper())
        val = {'A0': u"Auswertung nicht möglich / Fehlerwert (%s ohne Wert)" % ", ".join(err)}
        variables = dict((k, v if v is not None else '-') for k, v in variables.items())
        variables.update(val)
        return variables

    try:
        ret = parse(formula, variables)
        if isinstance(ret['A0'], bool):
            ret['A0'] = int(ret['A0'])
    except SyntaxError as e:
        if str(e) == "invalid syntax (<unknown>, line 1)":
            return {'error': 'Syntaxfehler'}
        elif str(e) == "unexpected EOF while parsing (<unknown>, line 1)":
            return {'error': 'Syntaxfehler, Klammern geschlossen?'}
        logging.warning("unbekannter SyntaxError: %s" % str(e))
        return {'error': 'Syntaxfehler*'}
    except KeyError as e:
        if str(e) == "'unbekannte variable'":
            return {'error': 'Unbekannte Variable'}
        logging.warning("unbekannter KeyError: %s" % str(e))
        return {'error': u'unzulässiger Schlüssel*'}
    except ValueError as e:
        if str(e).startswith('malformed node or string'):
            return {'error': 'Illegale oder unbekannte Operation in Formel'}
        elif str(e).startswith('Funktion'):
            return {'error': str(e)}
        logging.warning("unbekannter ValueError: %s" % str(e))
        return {'error': u'unzulässiger Wert*'}
    except TypeError as e:
        logging.warning(str(e))
        if str(e).startswith('unsupported operand type'):
            return {'error': u'unzulässiger Operand für Operator %s' % (str(e).split(':')[0].rsplit(' ', 1)[1])}
        logging.warning("unbekannter TypeError: %s" % str(e))
        return {'error': u'unzulässige Operatoren-/Variablenverwendung*'}
    except Exception as e:
        logging.exception(str(e))
        return {'error': 'unbekannter Fehler'}

    if 'A0' not in ret:
        return {'error': 'Leere Formel?'}

    # ret['A0'] = 0 if ret['A0'] <= 0 else 1
    ret.update(variables)
    return ret


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    if request.method == "GET":

        if action == "edit":

            hparams = haus.get_module_parameters()
            drs = hparams.get('differenzregelung', dict())
            mrs = hparams.get('vorlauftemperaturregelung', dict())
            pls = Regelung.objects.filter(regelung="pumpenlogik")
            fps = Regelung.objects.filter(regelung="fps")
            regs = []
            for regid, params in drs.items():
                regs.append({
                    "name": params['name'],
                    "regid": "dr_" + regid,
                    "regtype": 'Differenzregelung'
                })
            for regid, params in mrs.items():
                regs.append({
                    "name": params['name'],
                    "regid": "mr_" + regid,
                    "regtype": 'Vorlauftemperaturregelung'
                })
            for pl in pls:
                if pl.get_ausgang():
                    regs.append({
                        "name": pl.get_parameters()['name'],
                        "regid": "pl_%s" % pl.pk,
                        "regtype": "Raumanforderung"
                    })
            for s in fps:
                if entityid is not None and s.id == int(entityid):
                    continue
                regs.append({
                    "name": s.get_parameters().get('name'),
                    "regid": "fps_%s" % s.id,
                    "regtype": "FPS"
                })
            for sp in Regelung.objects.filter(regelung='solarpuffer'):
                if sp.get_ausgang():
                    regs.append({
                        "name": "Solarpuffer",
                        "regid": "sp_%s" % sp.id,
                        "regtype": ""
                    })

            sensors = AbstractSensor.get_all_sensors_info()
            sensors = [{'sensorid': sensor['sensor_id'], 'name': sensor['sensor_name'] + ' (' + sensor['sensortyp'].upper() + ') ' + sensor['raum_name'] + ((' / ' + sensor['description']) if sensor['description'] else '') } for sensor in sensors]

            module_variables = get_modules_variables(haus)

            selected_variables = {}
            params = {}
            if entityid is not None:
                entityid = int(entityid)
                try:
                    reg = Regelung.objects.get(pk=entityid)
                except Regelung.DoesNotExist:
                    return render_redirect(request, "/m_setup/%s/fps/" % haus.id)
                params = reg.get_parameters()
                selected_variables = reg.get_parameters().get('variables')
                selected_variables.setdefault('modulevariables', dict())
                if 'WetterPro_wetter-pro' in selected_variables:  # legacy workaround
                    selected_variables['modulevariables'].update(dict((name.lower(), 'wetterpro_%s' % _id) for name, _id in selected_variables['WetterPro_wetter-pro'].items()))
                    del selected_variables['WetterPro_wetter-pro']
                    params['variables'] = selected_variables
                    reg.set_parameters(params)

            eundr = []
            for etage in haus.etagen.all():
                e = {etage: []}
                for _raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                    e[etage].append(_raum)
                eundr.append(e)

            mode = params.get("mode", 'output')
            benutzer_text_0 = params.get('benutzer_text_0', '')

            benutzer_text_1 = params.get('benutzer_text_1', '')
            return render_response(request, "m_settings_fps_edit.html",
                                   {"haus": haus, "fpsid": entityid, "regs": json.dumps(regs), "plain_regs": regs,
                                    "sensors": json.dumps(sensors), "module_variables": json.dumps(module_variables),
                                    'selected_variables': json.dumps(selected_variables),
                                    'name': params.get('name', ''), 'error_value': params.get('error_value', ''),
                                    'formula': params.get('formula', ''), 'show_in_menu': params.get('show_in_menu'),
                                    'hysterese': params.get('hysterese', 0),
                                    'show_details': params.get('show_details', True),
                                    'explanation_text': params.get('explanation_text', ''),
                                    'benutzer_text_0': benutzer_text_0, 'benutzer_text_1': benutzer_text_1,
                                    'einvzg': params.get('einvzg', 0), 'ausvzg': params.get('ausvzg', 0),
                                    'eundr': eundr, 'mode': mode, 'raeume': params.get('raeume', []),
                                    'offset_regs': json.dumps(params.get('offset_regs', []))})

        elif action == "evaluate":
            formula = None
            outs = {}
            for key in request.GET:
                if key == "formula":
                    formula = request.GET['formula']
                else:
                    outs[key] = request.GET[key]
            outs['fpsid'] = int(request.GET['fpsid'])
            try:
                outs['hysterese'] = float(request.GET.get('hysterese', 0))
            except:
                outs['hysterese'] = 0

            outs.update(_parse_evaluation_request(formula))

            try:
                ret = _evaluate_formula(haus, formula, outs)
                if 'A0' in ret and isinstance(ret['A0'], float):
                    ret['A0'] = float("%.2f" % ret['A0'])
            except RuntimeError:
                ret = {"error": u"Fehler: FPS enthält sich selbst."}

            return HttpResponse(json.dumps(ret))

        elif action == "delete":
            reg = Regelung.objects.get(pk=long(entityid))
            try:
                reg.gatewayausgang.delete()
            except:
                pass
            reg.delete()
            return render_redirect(request, "/m_setup/%s/fps/" % haus.id)

        else:

            regs = Regelung.objects.filter(regelung="fps")
            ret = []
            for reg in sorted(regs, key=lambda x: x.get_parameters()['name']):
                if reg.get_ausgang():
                    status = ch.get("fps_%s" % reg.gatewayausgang.gateway.name)
                    if status is not None:
                        outs = [a.strip() for a in reg.gatewayausgang.ausgang.split(',')]
                        for k, v in status.items():
                            if k in outs:
                                state = u"zugewiesener Ausgang bei letzter Änderung gesetzt auf: %s" % (("%.2f" % v) if isinstance(v, float) else v)  # todo: anzeigen wenn fehler
                                break
                        else:
                            state = ""
                    else:
                        state = ""
                else:
                    state = ""
                ret.append((reg.pk, reg.get_parameters()['name'], state))

            return render_response(request, "m_settings_fps.html", {"haus": haus, "regs": ret})

    elif request.method == "POST":

        fpsid = request.POST.get('fpsid')
        name = request.POST.get('name')
        error_value = request.POST.get('error_value')
        explanation_text = request.POST.get('explanation_text', '')
        formula = request.POST.get('formula')
        benutzer_text_0 = request.POST.get('benutzer_text_0', '')
        benutzer_text_1 = request.POST.get('benutzer_text_1', '')
        try:
            hysterese = float(request.POST.get('hysterese', 0))
        except:
            hysterese = 0
        try:
            einvzg = int(request.POST.get('einvzg', 0))
        except:
            einvzg = 0
        try:
            show_in_menu = bool(request.POST.get('show_in_menu', 0))
        except:
            show_in_menu = False

        try:
            show_details = bool(request.POST.get('show_details', 0))
        except:
            show_details = True

        if einvzg < 0:
            einvzg = 0
        try:
            ausvzg = int(request.POST.get('ausvzg', 0))
        except:
            ausvzg = 0
        if ausvzg < 0:
            ausvzg = 0
        if not (name and error_value and formula):
            return render_redirect(request, "/m_setup/%s/fps/edit/%s" % (haus.id, ("%s/" % fpsid) if fpsid else ""))

        variables = {'outs': {}, 'sensors': {}, 'vars': {}, 'modulevariables': {}}
        for key in request.POST:
            if key not in {'fpsid', 'name', 'error_value', 'formula', 'hysterese', 'einvzg', 'ausvzg', 'mode'}:
                if key[0] == 's' and not (key[:4] == "show" or key[:2] == "sp"):
                    variables['sensors'][key] = request.POST[key]
                elif key[0] == 'a':
                    variables['outs'][key] = request.POST[key]
                elif key[0] == 'v':
                    variables['vars'][key] = request.POST[key]
                elif key[0] == 'm':
                    variables['modulevariables'][key] = request.POST[key]

        if fpsid:
            try:
                reg = Regelung.objects.get(pk=long(fpsid))
            except Regelung.DoesNotExist:
                reg = Regelung(regelung="fps")
        else:
            reg = Regelung(regelung="fps")

        mode = request.POST.get('mode', 'output')

        if mode == "offset":
            if reg.get_ausgang():
                reg.get_ausgang().delete()
            from rf.models import RFAusgang
            if len(RFAusgang.objects.filter(regelung_id=reg.id)):
                RFAusgang.objects.get(regelung_id=reg.id).delete()

        raeume = []
        regs = []
        for k, v in request.POST.items():
            if k.startswith('raeume_') and request.POST[k] == "on" and not k.endswith('alle'):
                try:
                    raeume.append(int(k.split('_')[1]))
                except:
                    continue

                try:
                    raum = Raum.objects.get(pk=raeume[-1])
                except Raum.DoesNotExist:
                    raeume = raeume[:-1]
                else:
                    activate(raum)
            elif k.startswith("oregs_") and request.POST[k] == "on":
                regs.append(k[6:])

        reg.set_parameters({
            'formula': formula,
            'name': name,
            'error_value': error_value,
            'explanation_text': explanation_text,
            'variables': variables,
            'hysterese': hysterese,
            'einvzg': einvzg,
            'ausvzg': ausvzg,
            'mode': mode,
            'raeume': raeume,
            'show_in_menu': show_in_menu,
            'show_details': show_details,
            'benutzer_text_0': benutzer_text_0,
            'benutzer_text_1': benutzer_text_1,
            'offset_regs': regs
        })
        reg.save()

        # damit braucht man kein ajax zum aufrufen der liste nach neuanlegen einer regelung.
        for reg in Regelung.objects.filter(regelung="fps"):
            Regelung.objects.invalidate(reg)

        return render_redirect(request, "/m_setup/%s/fps/" % haus.id)


@has_regelung_access
def get_local_settings_page_haus(request, haus, action=None, objid=None):

    reg = Regelung.objects.get(pk=int(objid))
    params = reg.get_parameters()

    if request.method == "GET":

        if action == "show" and "evaluate" in request.GET:
            formula = params['formula']
            outs = params['variables']['outs']
            outs.update(params['variables']['sensors'])
            outs.update(params['variables'].get('vars', {}))
            outs.update(params['variables'].get('modulevariables', {}))
            outs['fpsid'] = int(objid)
            try:
                outs['hysterese'] = float(request.GET.get('hysterese', 0))
            except:
                outs['hysterese'] = 0
            outs.update(_parse_evaluation_request(formula))

            ret = _evaluate_formula(haus, formula, outs)
            if 'A0' in ret and isinstance(ret['A0'], float):
                ret['A0'] = float("%.2f" % ret['A0'])

            result = 1 if 'A0' in ret and (isinstance(ret['A0'], float) or
                                           isinstance(ret['A0'], int)) and ret['A0'] > 0 else 0

            result_text = params.get('benutzer_text_%s' % result, '')
            ret.update({'result_text': result_text})
            return HttpResponse(json.dumps(ret))

        elif action == "show":
            context = {"haus": haus}
            context['config'] = params

            # zuordnungen section
            variables = params.get('variables', dict())
            outs = variables.get('outs', dict())
            sensors = variables.get('sensors', dict())
            mod_vars = variables.get('modulevariables', dict())
            hparams = haus.get_module_parameters()
            drs = hparams.get('differenzregelung', dict())
            mrs = hparams.get('vorlauftemperaturregelung', dict())
            pls = Regelung.objects
            fps = Regelung.objects
            reg_params_d = {
                'dr': {'object': drs, 'object_name': 'Differenzregelung'},
                'mr': {'object': mrs, 'object_name': 'Vorlauftemperaturregelung'},
                'pl': {'object': pls, 'object_name': 'Raumanforderung'},
                'fps': {'object': fps, 'object_name': 'FPS'},
            }
            sensor_rows = list()
            out_rows = list()
            mod_rows = list()
            # get description for a1, a2,....
            for var_no, var_id in outs.items():
                if var_id == "0":
                    continue
                obj_name, obj_id = var_id.split('_')[0], var_id.split('_')[1]
                if obj_name in ['dr', 'mr']:
                    var_name = "%s %s" % (reg_params_d[obj_name]['object_name'],
                                          reg_params_d[obj_name]['object'].get(obj_id, {}).get('name'))
                else:
                    try:
                        var_name = "%s %s" % (reg_params_d[obj_name]['object_name'],
                                          reg_params_d[obj_name]['object'].get(id=int(obj_id)).get_parameters().get('name'))
                    except Regelung.DoesNotExist:
                        var_name = ""
                out_rows.append((var_no, var_name))

            # get description for s1, s2,....
            for s_no, s_id in sensors.items():
                if s_id == "0":
                    continue
                sensor = Sensor.get_sensor(s_id)
                var_name = "%s / %s" % (sensor.name, sensor.description)
                sensors = AbstractSensor.get_all_sensors_info(sensors=[sensor])
                sensors = [{'sensorid': sensor['sensor_id'], 'name': sensor['sensor_name'] + ' (' + sensor['sensortyp'].upper() + ') ' + sensor['raum_name'] + ((' / ' + sensor['description']) if sensor['description'] else '')} for sensor in sensors]
                if len(sensors):
                    var_name = sensors[0]['name']

                sensor_rows.append((s_no, var_name))

            # get description for m1, m2,....
            module_variables = get_modules_variables(haus)
            for m_no, mod_name in mod_vars.items():
                if mod_name == "0":
                    continue
                var_id, mod_name = mod_name.split('_')[1], mod_name.split('_')[0]
                mod_lst = [d['variables'] for d in module_variables if d['module_name'] == mod_name]
                mod_lst = mod_lst[0] if len(mod_lst) else None
                if mod_lst:
                    names = [lst[1] for lst in mod_lst if lst[0] == int(var_id)]
                    if len(names):
                        var_name = names[0]
                        mod_rows.append((m_no, var_name))

            context['outs_details'] = out_rows
            context['sensors_details'] = sensor_rows
            context['mods_details'] = mod_rows

            context['objid'] = objid
            last = ch.get('fpslast_%s' % reg.id)
            if last is not None and isinstance(last, tuple):
                berlin = pytz.timezone("Europe/Berlin")
                last = "%.2f um %s" % (last[1], last[0].replace(tzinfo=pytz.utc).astimezone(berlin).strftime("%H:%M %d.%m.%Y"))
            context['last'] = last
            context['show_details'] = params.get('show_details', True)
            return render_response(request, "m_fps_show.html", context)

    elif request.method == "POST":
        try:
            params["variables"]["vars"][request.POST['slidername'].split('_')[0].lower()] = float(request.POST['slidernumber'])
            reg.set_parameters(params)
        except Exception:
            logging.exception("fehler")
        return HttpResponse()


def activate(hausoderraum):
    if 'fps' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['fps'])


def get_jsonapi(haus, usr, entityid=None):
    
    def _get_val(_fps, _params):
        try:
            val = evaluate_fps(haus, _fps)
            if val is None:
                raise Exception
        except:
            val = int(_params.get('error_value', 0))
        return val

    ret = {}

    if entityid is None:
        for fps in Regelung.objects.filter(regelung="fps"):
            params = fps.get_parameters()
            val = _get_val(fps, params)
            ret[fps.id] = {'name': params.get('name' ''), 'value': val}
    
    else:
        try:
            fps = Regelung.objects.get(pk=int(entityid))
            val = _get_val(fps, fps.get_parameters())
            ret[fps.id] = {'value': val}
        except Regelung.DoesNotExist:
            pass

    return ret
