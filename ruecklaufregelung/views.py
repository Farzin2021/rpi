# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Raum, Gateway, GatewayAusgang, AbstractSensor
import heizmanager.cache_helper as ch
import pytz
from datetime import datetime, timedelta
from django.http import HttpResponse
import logging
import math
import wetter.views as wetter
from heizmanager.getandset import _invalidate_get_cache, calc_opening
from heizmanager.mobile.m_temp import LocalPage, get_module_offsets, get_offsets_icon
from heizmanager.models import sig_get_outputs_from_output, sig_get_possible_outputs_from_output, sig_get_ctrl_devices
from vsensoren.models import VSensor
import json
from heizmanager.models import sig_new_output_value
from django.views.decorators.csrf import csrf_exempt
import re
from heizmanager.abstracts.base_mode import BaseMode


logger = logging.getLogger('logmonitor')


def get_name():
    return u'Rücklaufregelung'


def managesoutputs():
    return False


def _get_rlsoll(regparams, at):
    neigung = regparams.get('neigung', 5.5)
    regoffset = regparams.get('offset', 20)
    kruemmung = regparams.get('kruemmung', 2.0)

    # berechnung ist analog zu der im haus settings template: offset+(6-slope)* (Math.sqrt(Math.pow(i,bend)))
    if at > 20:
        at = 20
    if at < -20:
        at = -20
    at = math.fabs(at-20)  # verschiebung analog zum diagram

    rltemp = (6-neigung) * (math.sqrt(at**kruemmung)) + regoffset

    return rltemp


def do_ausgang(regelung, ausgangno):

    rlonly = False

    try:
        raum = regelung.get_raum()
        mainsensor = raum.get_mainsensor()
        if mainsensor is None:  # regelung mit nur einem RLSensor.
            rlonly = True
    except (Raum.DoesNotExist, AttributeError) as e:
        return HttpResponse('<0>')

    hausprofil = regelung.raum.etage.haus.profil.get()
    hparams = hausprofil.get_module_parameters()
    regparams = regelung.get_parameters()

    ignorems = regparams.get('ignorems', False)

    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("rlr: exception getting mode for raum %s" % raum.id)
        mode = None
    if mode is None or mode.get('use_offsets', False):
        offset = get_module_offsets(raum, only_clear_offsets=False)
    else:
        offset = 0.0

    rlr_active = False
    last_rt = None
    if not rlonly and not ignorems:
        last_rt = mainsensor.get_wert()
    rlroffset = 0.0
    last_rl = None
    at = None
    raumsoll = raum.solltemp
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    if 'ruecklaufregelung' in hausprofil.get_modules():

        if 'ruecklaufregelung' in hparams and \
                'use_atsensor' in hparams['ruecklaufregelung'] and \
                hparams['ruecklaufregelung']['use_atsensor'] and \
                'atsensor' in hparams['ruecklaufregelung'] and \
                hparams['ruecklaufregelung']['atsensor']:

            s = AbstractSensor.get_sensor(hparams['ruecklaufregelung']['atsensor'])
            if s:
                last = s.get_wert()
            else:
                last = None

            if last is not None and (now-last[1]).total_seconds() < 1800:  # AT Wert sollte halbwegs aktuell sein
                at = last[0]
        if at is None:  # entweder kein ATSensor konfiguriert oder kein aktueller Wert
            at = wetter.get_forecast(hausprofil.haus, 1)
            if at is not None:
                at = float(at[0][1])

        ssn = regparams['rlsensorssn']
        if isinstance(ssn, dict):
            for sn, outs in ssn.items():
                try:
                    if int(ausgangno) in outs:
                        ssn = sn
                        break
                except ValueError:
                    continue 
            else:
                ssn = ssn.keys()[0] if len(ssn) and ssn.keys()[0] != 'Sensor*None' else None

        rlsensor = AbstractSensor.get_sensor(ssn)
        if rlsensor:
            last_rl = rlsensor.get_wert()

        if rlonly or ignorems:

            if last_rl is not None and at is not None:
                last_get = ch.get('lastget_%s_%s' % (regelung.id, ausgangno))
                if last_get is None or ((last_get[0] == "1" or last_get[0] == '') and (now - last_get[1]).total_seconds() > 600):

                    regparams['offset'] = regparams['offset'] + offset
                    rltemp = _get_rlsoll(regparams, at)
                    if rlonly:
                        raum.solltemp = rltemp
                        logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf Rücklauftemperatur %.2f gesetzt" % raum.solltemp))
                        raum.save()
                    raumsoll = rltemp

                elif last_get[0] == "0" and (now-last_get[1]).total_seconds() > regparams.get('intervall', 1800):
                    ret = '<1>'
                    ch.set('lastget_%s_%s' % (regelung.id, ausgangno), ("1", now, rlr_active))
                    ch.set('lastget_%s_%s' % (regelung.id, 0), ("1", now, rlr_active))
                    logging.warning("rlonly2punkt for out %s: switching to 1 because we were 0 for an hour" % ausgangno)
                    logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf 1, weil Spülintervall abgelaufen." % ausgangno))
                    return HttpResponse(ret)

                else:
                    ret = '<' + last_get[0] + '>'
                    # ch.set('lastget_%s_%s' % (regelung.id, ausgangno), (last_get[0], last_get[1], rlr_active))
                    # ch.set('lastget_%s_%s' % (regelung.id, 0), (last_get[0], last_get[1], rlr_active))
                    logging.warning("rlr returning default for ausgang %s, is %s" % (ausgangno, ret))
                    logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s. Verlängerung der letzten Berechnung" % (ausgangno, last_get[0])))
                    return HttpResponse(ret)
            else:
                logging.warning("  rlonly fuer ausgang %s: else. ssn = %s, rlsensor = %s." % (ausgangno, ssn, str(rlsensor)))
                pass  # normale 2punkt unten.

        else:

            last_get = ch.get('lastget_%s_%s' % (regelung.id, ausgangno))
            if last_get is not None and last_rl is not None and last_rt is not None and at is not None:
                if (now-last_get[1]).total_seconds() > 600:
                    if last_get[0] == '1':
                        rltemp = _get_rlsoll(regparams, at)

                        if last_rl[0] > rltemp:  # drueber. 2punkt mit RLOFFSET
                            rlroffset = regparams['schaerfe']
                            rlr_active = True
                        else:  # drunter. normale 2punkt
                            pass

                    elif last_get[0] == '0':
                        # pass  # normale 2punkt

                        if (now-last_get[1]).total_seconds() > regparams.get('intervall', 1800):

                            # hier nicht nur auf RT, sondern auch auf RL schauen?
                            # if (raum.solltemp+offset-regparams['schaerfe']) > last_rt[0]:
                            #     # zu kalt
                            #     pass
                            # else:
                            #     # bereits zu warm
                            #     pass

                            rltemp = _get_rlsoll(regparams, at)
                            if last_rl[0] > rltemp:  # drueber. 2punkt mit RLOFFSET
                                rlroffset = regparams['schaerfe']
                                rlr_active = True
                            else:  # drunter. normale 2punkt
                                pass

                        else:
                            # ch.set('lastget_%s_%s' % (regelung.id, ausgangno), ('0', last_get[1], last_get[2]))
                            # ch.set('lastget_%s_%s' % (regelung.id, 0), ('0', last_get[1], last_get[2]))
                            logging.warning("rlr for ausgang %s: was 0, intervall still on, returning 0" % ausgangno)
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf 0. Kein Wärmebedarf, Spülintervall noch nicht abgelaufen." % ausgangno))
                            return HttpResponse("<0>")

# hier fehlt auch noch die verwendung von RT beim aufheizen!
# das dann auch auf gae uebertragen

                    else:
                        logging.warning("something went seriously wrong")

                else:
                    # hier gibts jetzt zwei Faelle: einfacher Refresh vom User und Parameteraenderung vom User.
                    if last_rt:
                        rlroffset = 0.0
                        if last_get[2]:
                            rlroffset = regparams['schaerfe']

                        ret = _zweipunkt(last_rt[0], raum.solltemp+offset-rlroffset, 0.0)
                        if ret.content.translate(None, '<>!') == last_get[0]:  # keine aenderung, d.h.
                            logging.warning('1RLR fuer ausgagns %s: %s || rloffset: - | now: %s, last_get[1]: %s' % (ausgangno, ret.content, str(now), str(last_get[1])))
                            # ch.set('lastget_%s%s' % (regelung.id, ausgangno),
                            #        (ret.content.translate(None, '<>!'), now, rlr_active))
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s." % (ausgangno, ret.content.translate(None, '<>!'))))
                            return ret
                        else:
                            pass  # dann rutschen wir automatisch unten in den ersten Fall und berechnen neu

                    else:  # zweiter Fall unten: nix.
                        pass

            else:
                # normale 2punkt
                pass

    else:
        logging.warning("rlr hausweit deaktiviert")

    if raum.solltemp is None:
        raum.solltemp = 0

    if last_rt and not rlonly and not ignorems:
        ret = _zweipunkt(last_rt[0], raum.solltemp+offset-rlroffset, 0.0)
        logging.warning("rlr2punkt fuer ausgang %s: ist %s | soll %s | ret %s"
                        % (ausgangno, last_rt[0], raum.solltemp+offset-rlroffset, ret.content))
        logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s. Raumtemperatur = %.2f, Ziel = %s." % (ausgangno, ret.content.translate(None, '<>!'), last_rt[0], (raum.solltemp+offset-rlroffset))))
        # lastget ohne ausgang, weil hauptsensoren regelung
        ch.set('lastget_%s_%s' % (regelung.id, ausgangno),
               (ret.content.translate(None, '<>!'), now, rlr_active, raum.solltemp+offset-rlroffset))
        ch.set('lastget_%s_%s' % (regelung.id, 0),
               (ret.content.translate(None, '<>!'), now, rlr_active, raum.solltemp+offset-rlroffset))
    elif last_rl and (rlonly or ignorems):
        # offset haben wir oben schon reingerechnet auf params['offset']
        ret = _zweipunkt(last_rl[0], raumsoll-rlroffset, 0.0)
        logging.warning("rlonly2punkt fuer ausgang %s: ist %s | soll %s | ret %s"
                        % (ausgangno, last_rl[0], raumsoll-rlroffset, ret.content))
        logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s. RL-Temperatur = %.2f, Ziel = %s." % (ausgangno, ret.content.translate(None, '<>!'), last_rl[0], (raumsoll-rlroffset))))
        ch.set('lastget_%s_%s' % (regelung.id, ausgangno),
               (ret.content.translate(None, '<>!'), now, rlr_active, raumsoll-rlroffset))
        ch.set('lastget_%s_%s' % (regelung.id, 0),
               (ret.content.translate(None, '<>!'), now, rlr_active, raumsoll-rlroffset))
    else:
        logging.warning("rlr.do_ausgang() fuer ausgang %s: -> ausgaben else, returning 0. last_rt=%s; last_rl=%s; rlonly=%s; ignorems=%s"
                        % (ausgangno, last_rt, last_rl, rlonly, ignorems))
        logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf 0, es fehlen Sensorenwerte." % ausgangno))
        ret = HttpResponse('<0>')

    return ret


def do_rfausgang(regelung, controllerid):
    ret = do_ausgang(regelung, 0).content
    return int(ret[1]) if ret[1] in ["0", "1"] else 0


def _zweipunkt(cnt, soll, margin):
    if cnt >= float(soll) + margin:
        return HttpResponse("<0>")
    elif cnt < float(soll) - margin:
        return HttpResponse("<1>")
    else:
        return HttpResponse("<>")


def get_outputs(haus, raum, regelung):

    belegte_ausgaenge = dict()
    ret = sig_get_outputs_from_output.send(sender='rlr', raum=raum, regelung=regelung)
    for cbfunc, outputs in ret:
        for gw, raum, regelung, ausgang, namen in outputs:

            params = regelung.get_parameters()
            ms = raum.get_mainsensor()
            ignorems = params.get('ignorems', False)
            rlsensorssn = params.get('rlsensorssn', dict())
            has_outs = True if len([1 for vals in rlsensorssn.values() if len(vals)]) else False
            outs = []
            for _outs in rlsensorssn.values():
                outs += _outs

            if ms:
                belegte_ausgaenge.setdefault(gw, list())
                for name in namen:
                    if (ignorems and not len(rlsensorssn)) or name not in outs:
                        belegte_ausgaenge[gw].append((name, ausgang, ms, True))

            if len(rlsensorssn) and has_outs:
                for name in namen:
                    for ssn, rlrouts in rlsensorssn.items():
                        rlsensor = AbstractSensor.get_sensor(ssn)
                        if rlsensor and (int(name) in rlrouts if (isinstance(name, unicode) and ':' not in name and '/' not in name) else name in rlrouts) and len(rlrouts):
                            belegte_ausgaenge.setdefault(gw, list())
                            belegte_ausgaenge[gw].append((name, ausgang, rlsensor, True))

    return belegte_ausgaenge


def get_possible_outputs(haus, raumdict=dict()):

    ret = sig_get_possible_outputs_from_output.send(sender='ruecklaufregelung', haus=haus)
    ctrldevices = sig_get_ctrl_devices.send(sender='rlr', haus=haus)
    ctrldevices = [d for cbf, devs in ctrldevices for d in devs]
    regler = dict((cd, list()) for cd in ctrldevices)

    if not len(raumdict):
        raumdict = {}
        for etage in haus.etagen.all():
            for raum in Raum.objects.filter_for_user(haus.eigentuemer, etage_id=etage.id).values_list("id", "name"):
                raumdict[raum[0]] = u"%s/%s" % (etage.name, raum[1])

    for cbfunc, r2s in ret:
        for raum, sensoren in r2s.items():
            params = raum.get_regelung().get_parameters()
            ignorems = params.get('ignorems', False)
            ms = raum.get_mainsensor()
            rlsensorssn = params.get('rlsensorssn', dict())

            for sensor, ctrldevice in sensoren:
                if ctrldevice not in ctrldevices:
                    continue  # kann scheinbar vorkommen (sensor ohne gateway?)

                if ms and not ignorems and not len(rlsensorssn):
                    regler[ctrldevice].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(ms))])
                    for ctrld in ctrldevices:
                        if ctrldevice != ctrld:
                            regler[ctrld].append([ms.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(ms))])

                for ssn, outs in rlsensorssn.items():
                    rlsensor = AbstractSensor.get_sensor(ssn)
                    if not rlsensor:
                        continue

                    regler[ctrldevice].append([rlsensor.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(rlsensor))])
                    for ctrld in ctrldevices:
                        if ctrldevice != ctrld:
                            regler[ctrld].append([rlsensor.get_identifier(), u"%s %s" % (raumdict[raum.id], unicode(rlsensor))])

    def uniq(lst):
        last = object()
        for item in lst:
            if item == last:
                continue
            yield item
            last = item

    for ctrld, outs in regler.items():
        regler[ctrld] = list(uniq(sorted(outs)))

    return regler


def get_config(request, macaddr):

    if request.method == 'GET':

        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            return HttpResponse(status=405)

        now = datetime.now()
        params = {'rlr': True}
        ret = {}
        for ausgang in GatewayAusgang.objects.filter(gateway=gw):
            reg = ausgang.regelung
            if reg.regelung != 'ruecklaufregelung':
                continue
            for ausgangno in [int(_o) for _o in reg.gatewayausgang.ausgang.split(',')]:
                try:
                    val = do_ausgang(reg, ausgangno).content
                    val = calc_opening(val, reg.get_raum().get_betriebsart(), gw.version, ausgangno, reg.get_raum(), gw.haus.profil.get().get_gateway_invertouts(macaddr.lower()))
                except Exception as e:
                    logging.exception("exc")
                    logger.warning("%s|%s" % (reg.raum.id, u"Fehler: Ausgang an %s konnte nicht gesetzt werden, steht auf 0." % macaddr))
                else:
                    ret.update({str(ausgangno): val})
                    lastget = ch.get("lastget_%s_%s" % (reg.id, ausgangno))
                    sig_new_output_value.send(sender='rlr_get_config', name='%s_%s' % (macaddr, ausgangno), value=val, ziel=lastget[3] if lastget is not None and isinstance(lastget, tuple) and len(lastget) == 4 else None, parameters=params, timestamp=now, modules=None)

        ch.set("rlr_%s" % macaddr, ret)
        return HttpResponse(json.dumps(ret))

    else:
        return HttpResponse(405)


@csrf_exempt
def set_status(request, macaddr):
    if request.method == "POST":
        data = json.loads(request.body)
        try:
            gw = Gateway.objects.get(name=macaddr.lower())
        except Gateway.DoesNotExist:
            pass
        else:
            if 'error' in data:
                ausgaenge = GatewayAusgang.objects.filter(gateway=gw)
                for ausgang in ausgaenge:
                    if ausgang.regelung and ausgang.regelung.regelung == "ruecklaufregelung":
                        logger.warning("%s|%s" % (ausgang.regelung.id, data['error']))

    return HttpResponse()


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/ruecklaufregelung/'>Rücklaufregelung</a>" % haus.id


def get_global_description_link():
    desc = get_name()
    desc_link = "http://support.controme.com/auswahl-und-aktivierung-von-modulen/modul-ruecklaufregelun/"
    return desc, desc_link


def get_global_settings_page(request, haus):
    if request.method == "GET":
        active = False
        if 'ruecklaufregelung' in haus.get_modules():  # damit man global (de-)aktivieren kann
            active = True

        params = haus.get_module_parameters()
        atsensor = False
        use_atsensor = False
        if 'ruecklaufregelung' in params:
            a = params.get('ruecklaufregelung', dict()).get('neigung', 5.5)
            c = params.get('ruecklaufregelung', dict()).get('offset', 20)
            k = params.get('ruecklaufregelung', dict()).get('kruemmung', 2.0)
            schaerfe = params.get('ruecklaufregelung', dict()).get('schaerfe', 0.5)
            if 'atsensor' in params['ruecklaufregelung'] and params['ruecklaufregelung']['atsensor']:
                atsensor = True
            if 'use_atsensor' in params['ruecklaufregelung'] and params['ruecklaufregelung']['use_atsensor']:
                use_atsensor = True
        else:
            a = 5.5
            c = +20
            k = 2.0
            schaerfe = 0.5

        return render_response(request, "m_settings_ruecklaufregelung.html",
                               {'haus': haus, 'active': active, 'a': a, 'c': c, 'k': k, 'schaerfe': schaerfe,
                                'atsensor': atsensor, 'use_atsensor': use_atsensor})

    elif request.method == "POST" and request.user.userprofile.get().role != 'K':

        params = haus.get_module_parameters()
        params.setdefault('ruecklaufregelung', dict())
        params['ruecklaufregelung']['neigung'] = float(request.POST['slope'])
        params['ruecklaufregelung']['offset'] = int(request.POST['offset'])
        params['ruecklaufregelung']['kruemmung'] = float(request.POST['bend'])
        params['ruecklaufregelung']['schaerfe'] = float(request.POST['s'])
        if request.POST['atsensor'] == '0':
            params['ruecklaufregelung']['use_atsensor'] = False
        else:
            params['ruecklaufregelung']['use_atsensor'] = True

        haus.set_module_parameters(params)

        return render_redirect(request, '/m_setup/%s/ruecklaufregelung/' % haus.id)


def get_global_settings_page_help(request, haus):
    return render_response(request, "m_help_ruecklaufregelung.html", {'haus': haus})


def get_local_link(regelung, request, raum):
    pass


def get_local_page(regelung, request, raum):

    if request.method == "GET":

        if 'gets' in request.GET:

            sensoren = raum.get_sensor_overview()

            return render_response(request, 'm_raum_sensoren.html', {'raum': raum, 'sensoren': sensoren})

        else:
            lines = []
            params = regelung.get_parameters()

            ms = raum.get_mainsensor()
            ignorems = params.get('ignorems', False)
            haus = raum.etage.haus
            module_lst = haus.get_modules()

            is_module_activated = False
            if 'ruecklaufregelung' in module_lst:
                is_module_activated = True

            at = None
            try:
                hparams = haus.get_module_parameters()
                if 'ruecklaufregelung' in hparams and 'atsensor' in hparams.get('ruecklaufregelung', dict()) and \
                        hparams['ruecklaufregelung']['atsensor'] and \
                        'use_atsensor' in hparams.get('ruecklaufregelung', dict()) and \
                        hparams['ruecklaufregelung']['use_atsensor']:

                    s = AbstractSensor.get_sensor(hparams['ruecklaufregelung']['atsensor'])
                    if s:
                        last = s.get_wert()
                        at = last[0]
                    else:
                        at = None

                if at is None:
                    at = wetter.get_forecast(raum.etage.haus, 1)
                    at = float(at[0][1])
            except TypeError:
                at = None

            try:
                mode = BaseMode.get_active_mode_raum(raum)
                mode_id = mode.get('activated_mode_id', None)
                raum.refresh_from_db()
            except Exception as e:
                logging.exception("rlr: exception getting mode for raum %s" % raum.id)
                mode = None
                mode_id = None

            mods = list()
            if mode is None or mode.get('use_offsets', False) or mode_id == -1:
                mods = _get_modules(request, raum)
                ch.delete("%s_roffsets_dict" % raum.id)
                clear_offsets = get_module_offsets(raum, only_clear_offsets=True)
            else:
                clear_offsets = 0.0

            if ms and ignorems:

                last = ms.get_wert()
                backup = False
                if last is None and isinstance(ms, VSensor) and ms.backup_sensor_id:
                    last = ms.backup_sensor.get_wert()
                    backup = True
                if last is None:
                    # lines.append(("Raumtemperatur", "-", 112, "/m_raum/%s/?gets" % raum.id))
                    lines.append(("Raumtemperatur", "-", 112, "/m_raum/%s/" % raum.id))
                    # lines.append(("Kein Temperaturwert zur Regelung verf&uuml;gbar.", None, None, None))
                    return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

                if backup:
                    temp = "<i>%.2f</i>" % last[0]
                else:
                    temp = "%.2f" % last[0]
                # lines.append(("Raumtemperatur", temp, 112, "/m_raum/%s/?gets" % raum.id))
                lines.append(("Raumtemperatur", temp, 112, "/m_raum/%s/" % raum.id))

                rlsoll = None
                if at is not None:
                    params['offset'] = params['offset'] + clear_offsets
                    rlsoll = _get_rlsoll(params, at)

                if rlsoll:
                    rlsoll = "%.2f" % rlsoll
                else:
                    rlsoll = "-"

                ssn2sensorandlastrl = dict()
                foundone = False
                for ssn, outs in params['rlsensorssn'].items():
                    sensor, last_rl = _get_sensor_by_rlsensorssn(ssn, regelung, params)
                    if not sensor:
                        continue
                    if last_rl is not None:
                        foundone = True
                    ssn2sensorandlastrl[ssn] = (sensor, last_rl)

                if not foundone:
                    return LocalPage(lines=lines, mods=mods, offset="-")

                cnt = 1  # enumerate geht hier nicht
                ssn2sandl = sorted([v for v in ssn2sensorandlastrl.values()], key=lambda v: v[0].name)
                for sensor, last_rl in ssn2sandl:
                    if not sensor:
                        continue

                    if last_rl:
                        last_rl = "%.2f" % last_rl[0]
                    else:
                        last_rl = '-'

                    if is_module_activated:

                        lines.append((u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description,
                                      "<small>%s</small> / <b>%s</b> " % (last_rl, rlsoll), 112,
                                      "/m_raum/%s/ruecklaufregelung/" % raum.id))

                    else:
                        lines.append((u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description,
                                     "<small class='no-link'>%s</small> " % last_rl, 112, "#"))


                    cnt += 1

                return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

            elif ms and not ignorems:
                # 1. regelung mit hauptsensor -> anzeige von hauptsensor

                last = ms.get_wert()
                if last is None and isinstance(ms, VSensor) and ms.backup_sensor_id:
                    last = ms.backup_sensor.get_wert()
                if last is None:
                    return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

                if at is not None:
                    rlsoll = "%.2f" % _get_rlsoll(params, at)
                else:
                    rlsoll = "-"

                cnt = 1  # enumerate geht hier nicht
                for ssn, outs in params['rlsensorssn'].items():

                    sensor, last_rl = _get_sensor_by_rlsensorssn(ssn, regelung, params)

                    if not sensor:
                        del params['rlsensorssn'][ssn]
                        regelung.set_parameters(params)
                        continue

                    if last_rl:
                        last_rl = "%.2f" % last_rl[0]
                    else:
                        last_rl = '-'

                    if is_module_activated:
                        name = u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description
                        lines.append((name, "<small>%s</small> / <b>%s</b> " % (last_rl, rlsoll),
                                      112, "/m_raum/%s/ruecklaufregelung/" % raum.id))
                    else:
                        name = u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description
                        lines.append((name, "<small class='no-link'>%s</small> " % last_rl, 112, "#"))

                    cnt += 1

                return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

            elif not ms:
                # 3. regelung ohne hauptsensor -> anzeige von rlsensor

                rlsensorssn = params['rlsensorssn']
                foundone = False
                ssn2sensorandlastrl = dict()
                for ssn, outs in rlsensorssn.items():
                    sensor, last_rl = _get_sensor_by_rlsensorssn(ssn, regelung, params)
                    if not sensor:
                        continue
                    if last_rl is not None:
                        foundone = True
                    ssn2sensorandlastrl[ssn] = (sensor, last_rl)

                if not foundone:
                    return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

                if at is not None:

                    # rlsoll nochmal holen aktualisiertem offset
                    params['offset'] = params['offset'] + clear_offsets
                    rlsoll = "%.2f" % _get_rlsoll(params, at)
                else:
                    rlsoll = "-"

                cnt = 1  # enumerate geht hier nicht.
                ssn2sandl = sorted([v for v in ssn2sensorandlastrl.values()], key=lambda v: v[0].name)
                for sensor, last_rl in ssn2sandl:
                    if not sensor:
                        continue

                    if last_rl:
                        last_rl = "%.2f" % last_rl[0]
                    else:
                        last_rl = '-'

                    if is_module_activated:
                        lines.append((u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description,
                                      "<small>%s</small> / <b>%s</b> " % (last_rl, rlsoll), 112,
                                      "/m_raum/%s/ruecklaufregelung/" % raum.id))
                    else:
                        lines.append((u"Rücklauf Nr. %s" % cnt if not sensor.description else u"Rücklauf %s" % sensor.description,
                                        "<small class='no-link'>%s</small>" % last_rl, 112, "#"))


                    cnt += 1

                return LocalPage(lines=lines, mods=mods, offset=clear_offsets)

    elif request.method == "POST":
        params = regelung.get_parameters()

        ms = raum.get_mainsensor()
        ignorems = params.get('ignorems', False)

        try:
            offset = float(request.POST['slidernumber'].replace(',', '.'))
        except ValueError:
            logging.warning("post valueerror")
            return render_redirect(request, "/m_raum/%s/" % raum.id)

        if (ms and ignorems) or not ms:  # rlr 2./3.
            params['offset'] = offset
            regelung.set_parameters(params)

            # und jetzt noch erzwingen, dass die ausgaenge neu berechnet werden von get_state
            try:
                # damit neu berechnet wird:
                for k, v in params['rlsensorssn'].items():
                    ch.delete('lastget_%s_%s' % (raum.regelung_id, 0))
                    for a in v:
                        ch.delete('lastget_%s_%s' % (raum.regelung_id, a))
                        ch.delete('lastout_%s_%s' % (raum.regelung_id, a))
                        _invalidate_get_cache(regelung.get_ausgang().gateway.name, regelung.get_ausgang().ausgang)
                        do_ausgang(regelung, a)

            except AttributeError:
                pass

        else:  # rlr 1.
            raum.solltemp = offset
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt (R1)" % raum.solltemp))
            raum.save()
            ch.delete('lastget_%s_%s' % (raum.regelung_id, 0))
            ausgang = raum.regelung.get_ausgang()
            if ausgang:
                ch.set_gateway_update(ausgang.gateway.name)
                for a in ausgang.ausgang.split(','):
                    ch.delete('lastget_%s_%s' % (raum.regelung_id, a.strip()))
                    ch.delete('lastout_%s_%s' % (raum.regelung_id, a.strip()))
                    
        return render_redirect(request, "/m_raum/%s/" % raum.id)


def get_local_html(regelung, request, raum):

    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("rlr: exception getting mode for raum %s" % raum.id)
        mode = None

    haus = raum.etage.haus
    hmodules = haus.get_modules()
    hparams = haus.get_module_parameters()
    if mode is None:
        offsets, icon = get_offsets_icon(raum)
    else:
        icon = mode['icon']
        if mode.get('use_offsets', False):
            offsets, icon = get_offsets_icon(raum)
        else:
            offsets = 0.0

    max_offset_module_html = ""
    if icon:
        max_offset_module_html = """<span class="module-iconn"><img src="/static/icons/%s.png" alt="icon"/></span>""" % icon

    params = regelung.get_parameters()
    ms = raum.get_mainsensor()
    ignorems = params.get('ignorems', False)

    if ms:
        last_rt = ms.get_wert()
        temp = "%.2f" % last_rt[0] if last_rt and last_rt[0] else "-"
        if ignorems:
            soll = params['offset']
        else:
            soll = raum.solltemp
    else:
        soll = params.get('offset', 0)
        for ssn, outs in params.get('rlsensorssn', dict()).items():
            sensor = AbstractSensor.get_sensor(ssn)
            if sensor:
                last_rl = sensor.get_wert()
                if last_rl and last_rl[0]:
                    break
        else:
            last_rl = None
        temp = "%.2f" % last_rl[0] if last_rl and last_rl[0] else "-"

    if soll is None:  # kann passieren
        soll = 0
    if offsets < 0:
        pom = "-"''
        offset_str = ' <span class="heat5_room_offset offstr blue-str" {color}>%05.2f</span>' % abs(offsets)
    else:
        pom = "+"
        offset_str = ' <span class="heat5_room_offset offstr red-str" {color}>%05.2f</span>' % offsets

    soll_offset_color = ""
    hand_time = "&nbsp"
    ziel_color = "#779A45"
    hand_picture = "<img src='/static/icons/icon-clock.png' width='30' height='30'>"
    mode_id = -99
    if mode is not None:
        hand_time = mode['activated_mode_text']
        mode_id = mode['activated_mode_id']
        if 'heizprogramm' not in hmodules:
            hand_picture = "&nbsp"
        else:
            hand_picture = "<img src='/static/icons/%s.png' width='30' height='30'>" % mode['icon']

        soll = mode.get('soll', 21.0)
        if mode_id == -1:
            soll_offset_color = " style='color: gray;'"
            ziel_color = "#d79a2d"

    else:
        if 'heizprogramm' not in hmodules or not hparams.get('is_switchingmode_enabled_%s' % haus.id, True):
            hand_picture = "&nbsp"

    ziel = float(soll) + float(offsets)

    try:
        if float(temp) > (float(ziel)):
            sign = "<"
        else:
            sign = ">"
    except:
        sign = "-"

    is_hide = ""
    from benutzerverwaltung.decorators import show_pro_visualbar
    if not show_pro_visualbar(request):
        is_hide = "hide"

    bis = ""
    if mode_id == -1:
        bis = "-"

    if len(hand_time) > 10:
        hand_time = hand_time[0:7] + "..."

    html = u'''<a data-role="none" href="/m_raum/{id}/" class="ui-link-inherit">
        <div class="main" data-id={room_id}>
          <div class="left_ui">
              <div class="ui_details">
                <span class="room_icon"><img src="/static/icons/{icon}.png" alt="icon"/></span>
                <span class="room_name">{name}</span></div>
                <input type='hidden' id="room_row_id" value={room_id}>
                {html_slider}
              <div class="ui_details"><div class="beam_place">{html_beam}</div></div>
          </div>
          <div class="right_ui">
            <div class="ui_details temperature">
                <div class="details_left"><span id="{room_id}" value="{soll}" class="soll" {color}>{soll}</span></div>
                <div class="details_sign signed" id="pom_sign">{pom}</div>
                <div class="details_right {color}" id="offset">{offset_str}</div>
                <div class="details_leaf"></div>
            </div>
            <div class="ui_details temperature">
                <div class="details_left"><span class="ziel" id="{room_id}" style="color:{ziel_color};">{ziel}</span></div>
                <div class="details_sign"><f class="sign">{sign}</f></div>
                <div class="details_right"><span class="temp bold underline">{temp}</span></div>
                <div class="details_leaf">{max_offset_module}</span></div>
            </div>
            <div class="ui_details hand_click">
                <div class="details_left hand" id="mode_icon">{hand_picture}</div>
                <div class="details_sign" id="sign_bis">{bis}</div>
                <div class="details_right hand_time" id="mode_name" data-id="{mode_id}">{hand_time}</div>
                <div class="details_leaf"></div>
            </div>
          </div>
        </div>
     '''
    if ziel < 10 or ziel > 35:
        html_slider = '''
            <div class="ui_details {hide}">
              <div class="range"><span>10</span></div>
              <div class="visualbar_slider">
                <div class="visual-bar">
                  <div class="slider-place">
                    <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                      <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                      <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                      {arrow_bar}
                    </div>
                  </div>
                </div>
              </div>
              <div class="range"><span>35</span></div>
            </div>
             '''
    else:
        html_slider = '''
                    <div class="ui_details {hide}">
                      <div class="range"><span>10</span></div>
                      <div class="visualbar_slider">
                        <div class="visual-bar">
                          <div class="slider-place">
                            <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:7px;">
                              <div class="ui-slider-bg ui-btn-active" style="width: {visual_temp}%;"></div>
                              <span id="vbar_soll_s2"  class="soll" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {soll_converted}%;"></span>
                              <span id="vbar_ziel_s2"  class="ziel" role="slider" aria-valuemin="10" aria-valuemax="35"  style="left: {ziel_converted}%; background-color: {ziel_color};"></span>
                              {arrow_bar}
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="range"><span>35</span></div>
                    </div>
                     '''

    html_beam = ""
    ausgang = regelung.get_ausgang()
    value = 0
    color = ''
    betriebsart = raum.get_betriebsart()
    if not int(betriebsart):
        html_beam += '<div class="beam" style="border-color: #019BB0;"></div>'
    else:
        if ausgang is not None:
            gw = ausgang.gateway
            gw_inverted = haus.profil.get().get_gateway_invertouts(gw.name.lower())
            for a in ausgang.ausgang.split(','):
                a = a.strip()

                sget = ch.get('lastout_%s_%s' % (regelung.id, a))
                if sget is None:
                    logging.warning("no lastget for %s" % raum.name)
                    sget = do_ausgang(regelung, a).content
                    try:
                        value = calc_opening(sget, raum.get_betriebsart(), gw.version, a, raum, gw_inverted, nolog=True)
                    except ValueError:
                        logging.exception("%s_%s in raum %s" % (gw.name, a, raum.name))
                        value = 0
                else:
                    value = int(sget[0])

                if not ("6.00" > gw.version >= "5.00"):
                    value = 0 if value == 0 else 100
                
                if gw_inverted:  # re-invert for displaying NO out beams correctly
                    value = 100 - value

                color = "#019BB0" if int(betriebsart) == 1 else "#e61873"
                frame_color = color if 'betriebsarten' in haus.get_modules() else "#969597"  # grey
                html_beam += '''
                                           <div class="visualbar_slider">
                                            <div class="visual-bar">
                                              <div class="slider-place">
                                                <div role="application" class="ui-slider-track   ui-corner-all ui-mini" style="height:5px; border-color: {frame_color};">
                                                  <div class="ui-slider-bg" style="width: {value}%; background-color: {background_color}"></div>
                                                  <input type="hidden" class="beam-width-value" value="{value}">
                                                </div>
                                              </div>
                                            </div>
                                           </div>
                                        '''.format(value=value, frame_color=frame_color, background_color=color)

    visual_temp = 0
    try:
        visual_temp = float(temp)
        # convert to %
        visual_temp = min(((visual_temp - 10) * 100) / 25, 100)
        if visual_temp < 0:
            visual_temp = 0
    except:
        pass

    soll = float(soll)
    ziel = float(ziel)
    offset_converted = (abs(float(offsets)) * 100) / 25

    overflow = True
    if soll <= 10:
        soll_converted = 0

    elif soll >= 35:
        soll_converted = 100
    else:
        soll_converted = (float(soll - 10) * 100) / 25
        overflow = False

    if ziel <= 10:
        ziel_converted = 0
    elif ziel >= 35:
        ziel_converted = 100
    else:
        ziel_converted = (float(ziel - 10) * 100) / 25
        overflow = False

    arrow_bar = ""
    delta = abs(ziel_converted - soll_converted)

    if not overflow:
        if offsets <= -0.1 and ziel < 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{delta}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets <= -0.1 and ziel > 10 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-left" style="left:{ziel_converted}%;width:{delta}%"></div><div class ="arrow-left" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel < 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{delta}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'
        if offsets >= 0.1 and ziel > 35 and mode_id != -1:
            arrow_bar = '<div class="arrow-bar-right" style="left:{soll_converted}%;width:{delta}%"></div><div class ="arrow-right" style="left:{ziel_converted}%;"></div>'

    html = html.format(temp=temp, offset_str=offset_str, soll='%.2f' % soll, sign=sign,
                       ziel="%.2f" % ziel,ziel_color=ziel_color, pom=pom, max_offset_module=max_offset_module_html, id=raum.id,
                       icon=raum.icon, delta=delta,
                       name=raum.name, html_beam=html_beam, html_slider=html_slider,mode_id=mode_id
                       ,hand_time=hand_time, room_id=raum.id, hand_picture=hand_picture,
                       color=soll_offset_color, bis=bis)

    html = html.format(color=soll_offset_color,
                       arrow_bar=arrow_bar, soll_converted=soll_converted,
                       visual_temp=visual_temp, delta=delta,
                       ziel_converted=ziel_converted, hide=is_hide, ziel_color=ziel_color)
    html = html.format(delta=delta, ziel_converted=ziel_converted, soll_converted=soll_converted, offset_converted=offset_converted)

    html += "</a>"

    return html


def _get_sensor_by_rlsensorssn(ssn, regelung, regparams):
    sensor = AbstractSensor.get_sensor(ssn)

    if not sensor:
        return None, None

    last_rl = sensor.get_wert()

    return sensor, last_rl


def _get_modules(request, raum):
    import importlib
    lines = list()
    haus = raum.etage.haus
    mods = haus.get_modules()
    for modulename in sorted(mods):
        module = None
        try:
            module = importlib.import_module(modulename.strip() + '.views')
        except ImportError:
            if modulename.strip() in ['module', 'setup_rpi']:
                module = importlib.import_module('heizmanager.modules.' + modulename)
        try:
            link = module.get_local_settings_link(request, raum)
            if link:
                lines.append(link)
        except (AttributeError, KeyError):
            pass
    api_raum = ch.get("api_roomoffset_%s" % raum.id) or {}
    api_haus = ch.get("api_houseoffset_%s" % haus.id) or {}
    for mod in set(api_raum.keys() + api_haus.keys()):
        offset = api_raum.get(mod, 0.0) + api_haus.get(mod, 0.0)
        lines.append((mod, offset, 75, "#"))
    return lines


def get_local_settings_link(request, raum):
    return ''


def get_local_settings_page(request, raum):
    if request.method == "GET":
        params = raum.regelung.get_parameters()
        hparams = raum.etage.haus.get_module_parameters()
        rlonly = False
        if (raum.get_mainsensor() is None and params['rlsensorssn']) or params.get('ignorems', False):
            rlonly = True
        h_slope = hparams.get('ruecklaufregelung', dict()).get('neigung', 5.5)
        h_offset = hparams.get('ruecklaufregelung', dict()).get('offset', 20)
        h_bend = hparams.get('ruecklaufregelung', dict()).get('kruemmung', 2.0)
        a = params.get('neigung', h_slope)
        c = params.get('offset', h_offset)
        k = params.get('kruemmung', h_bend)
        s = params.get('schaerfe', hparams.get('ruecklaufregelung', dict()).get('schaerfe', 0.5))
        i = (params.get('intervall', 1800) / 60)

        return render_response(request, 'm_raum_ruecklaufregelung.html',
                               {'raum': raum, 'a': a, 'c': c, 'k': k,
                                'schaerfe': s, 'h_slope': h_slope, 'h_offset': h_offset, 'h_bend': h_bend,
                                'rlonly': rlonly, 'intervall': i})

    elif request.method == "POST" and Raum.objects.get_for_user(request.user, pk=raum.id):
        params = raum.regelung.get_parameters()

        params['neigung'] = float(request.POST['slope']) if len(request.POST['slope']) else 0.0
        params['offset'] = float(request.POST['offset']) if len(request.POST['offset']) else 0.0
        params['kruemmung'] = float(request.POST['bend']) if len(request.POST['bend']) else 0.0
        params['schaerfe'] = float(request.POST['s']) if len(request.POST['s']) else 0.0
        params['intervall'] = (int(request.POST['interval'])*60) if len(request.POST['interval']) else 1800

        # wenn ausgeschaltet wurde, raumsoll auf offset setzen
        if 'ignorems' not in request.POST and params.get('ignorems', False):
            raum.solltemp = params['offset']
            logger.warning(u"%s|%s" % (raum.id, u"Solltemperatur auf %.2f gesetzt (R2)" % raum.solltemp))
            raum.save()

        params['ignorems'] = False
        if 'ignorems' in request.POST:  # konsistenz beim speichern
            params['ignorems'] = True

        reg = raum.regelung
        reg.set_parameters(params)
        reg.save()

        # das geht erst hier, weil wir zuerst die regelung speichern muessen!
        if raum.get_mainsensor() is None or params['ignorems']:
            try:
                for k, v in params['rlsensorssn'].items():
                    ch.delete('lastget_%s_%s' % (raum.regelung_id, 0))
                    for a in v:
                        ch.delete('lastget_%s_%s' % (raum.regelung_id, a))
                        ch.delete('lastout_%s_%s' % (raum.regelung_id, a))
                        _invalidate_get_cache(reg.get_ausgang().gateway.name, reg.get_ausgang().ausgang)
                        do_ausgang(reg, a)
            except AttributeError:
                logging.warning('wat2')
                pass

        return render_redirect(request, '/m_raum/%s/' % raum.id)
