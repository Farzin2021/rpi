from heizmanager.models import sig_get_outputs_from_regelung, sig_get_possible_outputs_from_regelung, sig_create_output_regelung, sig_create_regelung, Raum, sig_create_output
from django.dispatch import receiver
import ruecklaufregelung.views as rlr
import heizmanager.cache_helper as ch
import logging


@receiver(sig_get_outputs_from_regelung)
def rlr_get_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raum = kwargs['raum']
    regelung = kwargs['regelung']
    if regelung.regelung == 'ruecklaufregelung':
        return rlr.get_outputs(haus, raum, regelung)
    else:
        return {}


@receiver(sig_get_possible_outputs_from_regelung)
def rlr_get_possible_outputs(sender, **kwargs):
    haus = kwargs['haus']
    raumdict = kwargs.get('raumdict', {})
    return rlr.get_possible_outputs(haus, raumdict)


@receiver(sig_create_output_regelung)
def rlr_create_output(sender, **kwargs):
    regelung = kwargs['regelung']
    if regelung and regelung.regelung == "ruecklaufregelung":
        outs = kwargs['outs']
        sensor = kwargs['sensor']

        params = regelung.get_parameters()
        rlsensorssn = params.get('rlsensorssn', dict())

        if sender == 'delete':
            rlsensorssn = dict((k, []) for k in rlsensorssn.keys())
            params['rlsensorssn'] = rlsensorssn
            regelung.set_parameters(params)

        else:
            if sender == 'create':
                rlsensorssn = dict((k, []) for k in rlsensorssn.keys())
            if sensor.get_identifier() in rlsensorssn:
                _outs = rlsensorssn[sensor.get_identifier()]
                for o in outs:
                    try:
                        _o = int(o)  # gw, zwave
                    except ValueError:
                        _o = o  # enocean
                    _outs.append(_o)
                rlsensorssn[sensor.get_identifier()] = _outs

            params['rlsensorssn'] = rlsensorssn
            regelung.set_parameters(params)


@receiver(sig_create_regelung)
def rlr_create_regelung(sender, **kwargs):
    regelung = kwargs['regelung']
    operation = kwargs['operation']
    sensortyp = kwargs['sensortyp']
    sensor = kwargs['sensor']
    changed_sensor = kwargs['changed_sensor']
    outs = kwargs['outs']

    if operation == 'addsensor':

        if sensortyp == 'at':
            hparams = sensor.haus.get_module_parameters()
            hparams.setdefault('ruecklaufregelung', dict())
            hparams['ruecklaufregelung']['atsensor'] = sensor.get_identifier()
            sensor.haus.set_module_parameters(hparams)
            sensor.mainsensor = True
            sensor.save()
            if sensor.raum is not None:
                r = Raum.objects.get(pk=sensor.raum_id)
                r.set_mainsensor(sensor)

        else:

            if regelung.regelung != 'ruecklaufregelung' and sensortyp != 'rl':
                return

            if sensortyp == 'rl':
                if regelung.regelung != 'ruecklaufregelung':
                    regelung.regelung = "ruecklaufregelung"
                    hparams = sensor.haus.get_module_parameters()
                    params = {'active': True, 'rlsensorssn': {sensor.get_identifier(): []},
                              'neigung': hparams.get('ruecklaufregelung', dict()).get('neigung', 5.5),
                              'offset': hparams.get('ruecklaufregelung', dict()).get('offset', 20),
                              'kruemmung': hparams.get('ruecklaufregelung', dict()).get('kruemmung', 2.0),
                              'schaerfe': hparams.get('ruecklaufregelung', dict()).get('schaerfe', 0.5), 'delta': 0.0}
                    regelung.set_parameters(params)
                    regelung.save()
                else:
                    regparams = regelung.get_parameters()
                    if sensor.get_identifier() not in regparams['rlsensorssn']:
                        regparams['rlsensorssn'][sensor.get_identifier()] = list()
                        regparams['active'] = True
                        regelung.set_parameters(regparams)

            elif sensortyp == 'rt':
                sensor.mainsensor = True
                sensor.save()

                r = Raum.objects.get(pk=sensor.raum_id)
                r.set_mainsensor(sensor)

    elif operation == 'deletesensor':

        hparams = sensor.haus.get_module_parameters()
        if sensor.get_identifier() == hparams.get('ruecklaufregelung', dict()).get('atsensor', -1):
            del hparams['ruecklaufregelung']['atsensor']
            sensor.haus.set_module_parameters(hparams)
            sensor.mainsensor = False
            sensor.save()

        if regelung is None or regelung.regelung != 'ruecklaufregelung':
            return

        params = regelung.get_parameters()

        delouts = []
        if changed_sensor.mainsensor:
            changed_sensor.mainsensor = False
            changed_sensor.save()

            if changed_sensor.raum_id and changed_sensor.raum_id != sensor.raum_id:
                r = Raum.objects.get(pk=changed_sensor.raum_id)
                r.set_mainsensor(None)
                ch.delete("reg_%s" % regelung.id)

        else:
            try:
                delouts = params['rlsensorssn'][sensor.get_identifier()]
                del params['rlsensorssn'][sensor.get_identifier()]
                regelung.set_parameters(params)
                ch.delete("reg_%s" % regelung.id)
            except KeyError:
                pass

        if len(delouts):
            for out in outs:
                if len(out) > 4:
                    intersec = set(out[4]) - set(delouts)
                    if not len(intersec):
                        out[3].delete()
                        for i in out[4]:
                            ch.delete("lastget_%s_%s" % (regelung.id, i))
                            ch.delete("lastget_%s_%s" % (regelung.id, 0))
                    elif len(intersec) < len(out[4]):
                        sig_create_output.send(
                            sender="create",
                            haus=None,
                            ctrldevice=out[0],
                            regelung=regelung,
                            sensor=sensor,
                            outs=[str(i) for i in intersec]
                        )
                        for i in delouts:
                            ch.delete("lastget_%s_%s" % (regelung.id, i))
                            ch.delete("lastget_%s_%s" % (regelung.id, 0))

        if not len(params['rlsensorssn']):  # and not regelung.get_raum().get_mainsensor():
            for out in outs:
                try:
                    out[3].delete()
                except:  # schon geloescht oder anderer fehler
                    pass

            regelung.regelung = "zweipunktregelung"
            regelung.set_parameters(str({'delta': 0.1, 'active': True}))
            regelung.save()
            if len(outs):
                return "Aufgrund der Eingaben wurden bereits zugeordnete Ausg&auml;nge modifiziert. Bitte &uuml;berpr&uuml;fen Sie die Ausgangszuordnung."


