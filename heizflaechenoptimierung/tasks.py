# -*- coding: utf-8 -*-
import logging
import pytz
from datetime import datetime, timedelta
from heizmanager.models import AbstractSensor, Haus, Gateway, sig_new_temp_value, sig_new_output_value
from heizmanager.mobile.m_temp import get_module_offsets
import heizmanager.cache_helper as ch
from heizflaechenoptimierung.views import has_ms_and_rls, activate, line_intersection, get_strategy_mode_values
import wetter.views as wetter
from celery import shared_task
import time
from heizmanager.abstracts.base_mode import BaseMode
from math import fabs
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
from knx.models import KNXGateway
from itertools import chain


logger = logging.getLogger('logmonitor')


def log_parameters(rlziel, rparams, raum, haus, now):
    hfo_dict = {"at_window_start": rparams.get('time_start', 0), "at_window_end": rparams.get('time_end', 5),
                "hf_temp_plustwentyat": rparams.get('heating_element_start', 22),
                "hf_temp_minustwentyat": rparams.get('heating_element_end', 40),
                "activity_threshold_below_target": rparams.get('before', -0.5),
                "activity_threshold_above_target": rparams.get('after', 0.5),
                "hf_temp_correction_per_degree_deviation": rparams.get('correction_heating_element', 3)}

    value = 0
    name_params = 'hfo_params_raum_' + str(raum.id)
    modules = None
    sig_new_output_value.send(sender="hfo", name=name_params, value=value, ziel=rlziel,
                              parameters=hfo_dict, timestamp=now, modules=modules)


def log_onAT(haus, raum, now, rparams, forecast, average):
    if forecast is None or average is None:
        at_window_start = rparams['time_start']
        at_window_end = rparams['time_end']
        at_window_end = min(at_window_end, 72)
        forecast_now = wetter.get_10dayforecast(haus, int(at_window_end))
        forecast = forecast_now[int(at_window_start):int(at_window_end)]
        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
    if now is None:
        berlin = pytz.timezone('Europe/Berlin')
        now = datetime.now(berlin)
    cnttemp = forecast[0][1]
    name_avg = "averagetemp_raum_" + str(raum.id)
    name_cnt = "cnttemp_raum_" + str(raum.id)
    modules = []
    sig_new_temp_value.send(sender="hfo", name=name_avg, value=average, modules=modules, timestamp=now)
    sig_new_temp_value.send(sender="hfo", name=name_cnt, value=cnttemp, modules=modules, timestamp=now)


@shared_task(base=CeleryErrorLoggingTask)
def calc_offset(for_raum=None):

    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    for haus in Haus.objects.all():
        if 'heizflaechenoptimierung' not in haus.get_modules():
            continue

        for etage in haus.etagen.all():
            for raum in etage.raeume.all():

                if for_raum and raum != for_raum:
                    continue

                if raum.regelung.regelung == 'ruecklaufregelung':
                    if 'heizflaechenoptimierung' not in raum.get_modules():
                        activate(raum)
                    if raum.regelung.get_ausgang() and "6.00" > raum.regelung.gatewayausgang.gateway.version >= "5.00":
                        continue  # calc_hftemps will cover this room
                else:
                    continue

                try:
                    betriebs_mode = raum.get_betriebsart()
                    if betriebs_mode == '1' or betriebs_mode == '0':  # if mode is cooling or off deactivate module
                        vals = {'timer': None, 'offset': 0, 'step': 0, 'mode': 0, 'raumziel': None, 'ts': None, 'flushing_interval': None}
                        ch.set("hfo_%s_%s" % (haus.id, raum.id), vals, 3600)  # todo 600
                        if betriebs_mode == '1':
                            logger.warning("%s|%s" % (raum.id, u"Status: Betriebsart auf Modus 'kühlen' eingestellt. Heizflächenoptimierung wird für diesen Raum nicht ausgeführt."))
                        elif betriebs_mode == '0':
                            logger.warning("%s|%s" % (raum.id, u"Status: Betriebsart auf Modus 'aus' eingestellt. Heizflächenoptimierung wird für diesen Raum nicht ausgeführt."))
                        if for_raum:
                            return vals['offset']
                        continue
                except Exception as e:
                    logging.exception("Fehler: Betriebsmodus konnte nicht ermittelt werden %s" % str(e))
                    logger.warning(u"Fehler: Betriebsmodes konnte nicht ermittelt werden")

                rparams = get_strategy_mode_values(raum)

                regparams = raum.regelung.get_parameters()

                solltemp = raum.solltemp
                offset = get_module_offsets(raum, exclude='heizflaechenoptimierung', as_dict=True)
                try:
                    del offset['heizflaechenoptimierung']
                except:
                    pass
                offset = sum(offset.values())

                ms, rls = has_ms_and_rls(raum)

                if ms:
                    temp = raum.get_mainsensor().get_wert()
                    if temp:
                        temp = temp[0]
                else:
                    temp = regparams.get('offset', 21.0)

                if temp is None:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Raumtemperatur verfügbar, Berechnung wird übersprungen."))
                    continue

                number_average = 0.0
                i = 0
                for ssn, outs in regparams.get('rlsensorssn', dict()).items():
                    sensor = AbstractSensor.get_sensor(ssn)
                    if not sensor:
                        continue
                    last_rl = sensor.get_wert()
                    if last_rl:
                        number_average += float(last_rl[0])
                        i += 1
                if i == 0:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Rücklaufsensoren angelegt, Berechnung wird übersprungen."))
                    continue
                rltemp_average = number_average / i

                if not rltemp_average:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Werte für Rücklaufsensoren verfügbar, Berechnung wird übersprungen."))
                    continue

                at_window_start = rparams['time_start']
                at_window_end = rparams['time_end']
                hf_temp_plustwentyat = rparams['heating_element_start']
                hf_temp_minustwentyat = rparams['heating_element_end']
                activity_threshold_below_target = rparams['before']
                activity_threshold_above_target = rparams['after']
                flushing_interval_minustwentyat = rparams['flushing_interval_start']
                flushing_interval_plustwentyat = rparams['flushing_interval_end']
                hf_temp_correction_per_degree_deviation = rparams['correction_heating_element']

                last_vals = ch.get('hfo_%s_%s' % (haus.id, raum.id))
                if last_vals is None:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 0
                    hf_mode = 0
                    flushing_interval = None
                else:  # get active timers
                    hf_timer = last_vals['timer']
                    hf_offset = last_vals['offset']
                    hf_step = last_vals['step']
                    hf_mode = last_vals.get('mode', -1)
                    flushing_interval = last_vals['flushing_interval']

                hf_raum_ziel = solltemp + offset

                try:
                    diff_temp = temp - hf_raum_ziel
                except:
                    # wird komischerweise nicht immer oben mit dem temp is None check gefangen
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Raumtemperatur verfügbar. Berechnung wird übersprungen."))
                    continue
                _hf_offset = None
                if diff_temp <= activity_threshold_below_target:
                    new_mode = 1
                elif 0 >= diff_temp >= activity_threshold_below_target:
                    new_mode = 2
                    #  Offset = (room-target-temperature - current-Room-temperature + 0.1K) * -1 // +*-=-
                    _hf_offset = ((hf_raum_ziel - temp) + 0.1) * -1
                elif 0 < diff_temp <= activity_threshold_above_target:
                    new_mode = 3
                    #  Offset = (room-target-temperature - current-Room-temperature - 0.1K) * -1 // -*-=+
                    _hf_offset = ((hf_raum_ziel - temp) - 0.1) * -1
                elif diff_temp > activity_threshold_above_target:
                    new_mode = 4

                if new_mode != hf_mode:  # if mode changed, remove old timers
                    hf_step = 0
                    hf_offset = 0
                    hf_timer = None
                    flushing_interval = None

                logging.warning("new_mode == %s" % new_mode)

                rlsoll_offset = (hf_raum_ziel - temp) * hf_temp_correction_per_degree_deviation

                hf_calc_ts = now
                rlsoll = _get_rlsoll(raum, rparams, hf_raum_ziel)
                try:
                    rlsoll += rlsoll_offset
                except TypeError:  # rlsoll is None, probably. sollte man vielleicht loggen.
                    continue

                ch.set("rlsoll_%s" % raum.id, rlsoll)

                if new_mode == 1:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 1
                    logger.warning("%s|%s" % (raum.id, u"Modus 1: Schnellstmöglich aufheizen. Genius beheizt den Raum gerade maximal, um schnellstmöglich deine Wunschtemperatur zu erreichen."))

                elif new_mode == 4:
                    hf_timer = None
                    hf_offset = 0
                    hf_step = 8
                    logger.warning("%s|%s" % (raum.id, u"Modus 8: Schnellstmöglich abkühlen. Genius beheizt den Raum gerade nicht, um schnellstmöglich auf deine Wunschtemperatur abzukühlen."))

                else:
                    try:
                        at_window_end = min(at_window_end, 72)
                        forecast_now = wetter.get_10dayforecast(haus, int(at_window_end))
                        forecast = forecast_now[int(at_window_start):int(at_window_end)]
                        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
                        log_onAT(haus, raum, now, rparams, forecast_now, average)
                    except Exception as e:
                        logging.exception("message")
                        average = None

                    if not average:
                        logger.warning("%s|%s" % (raum.id, u"Status: Keine Außentemperaturvorhersage vorhanden. Berechnung wird übersprungen."))
                        continue

                    a = (flushing_interval_plustwentyat, 20)
                    b = (flushing_interval_minustwentyat, -20)
                    c = (10, average)
                    d = (600, average)
                    try:
                        flushing_interval, y = line_intersection((a, b), (c, d))
                    except Exception as e:
                        flushing_interval = None

                    if new_mode == 2:  # overswing protection
                        if hf_mode == 3 or hf_mode == 4:  # if comming from mode 3 or 4 switch to step 3, no timer should be active right now
                            hf_offset = 0
                            hf_step = 3
                            logger.warning("%s|%s" % (raum.id, u"Modus 3.n: Intelligente Aufheizphase. Genius ermittelt die aktuelle Fußbodentemperatur."))
                            hf_timer = now + timedelta(minutes=19)
                        elif isinstance(hf_timer, datetime):
                            if hf_step == 2 and hf_timer < (now + timedelta(seconds=10)):  # flushing timer is abgelaufen
                                hf_timer = now + timedelta(minutes=19)  # set new timer to 20 mins (19 mins?)
                                hf_offset = 0
                                hf_step = 3
                                logger.warning("%s|%s" % (raum.id, u"Modus 3.1: Intelligente Aufheizphase. Genius ermittelt die aktuelle Fußbodentemperatur."))
                            elif hf_step == 3 and hf_timer < (now + timedelta(seconds=9)):  # timer done start from top
                                logger.warning("%s|%s" % (raum.id, u"Modus 3.2: Intelligente Aufheizphase. Genius hat die Ermittlung der Fußbodentemperatur abgeschlossen und startet neuen Zyklus."))
                                time.sleep(1.2)  # give next log entry another time
                                # check RLT > target
                                if rltemp_average > rlsoll:  # bild mode 2
                                    hf_offset = _hf_offset
                                    hf_step = 2
                                    flushing_interval /= 2
                                    hf_timer = now + timedelta(minutes=flushing_interval)
                                    logger.warning("%s|%s" % (raum.id, u"Modus 2: Intelligente Aufheizphase. Die Fußbodentemperatur ist zu hoch. Um zu verhindern, dass es wärmer wird als gewünscht, unterbricht Genius den Heizvorgang bis %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                # start from top
                                else:
                                    hf_timer = None
                                    hf_offset = 0
                                    hf_step = 4
                                    logger.warning("%s|%s" % (raum.id, u"Modus 4: Intelligente Aufheizphase. Genius hat ermittelt, dass die Fussbodentemperatur nicht übermäßig hoch ist. Der Raum wird weiter mit voller Leistung beheizt, damit schnellstmöglich deine Wunschtemperatur erreicht wird."))
                            else:
                                # logger.warning("%s|%s" % (raum.id, u"Modus 2, Schritt %s*. Offset=%s, Timer=%s." % (hf_step, hf_offset, hf_timer.strftime("%H:%M %d.%m.%Y"))))
                                pass
                        else:
                            # check RLT > target
                            if rltemp_average > rlsoll:  # bild mode 2
                                hf_offset = _hf_offset
                                hf_step = 2
                                flushing_interval /= 2
                                hf_timer = now + timedelta(minutes=flushing_interval)
                                logger.warning("%s|%s" % (raum.id, u"Modus 2: Intelligente Aufheizphase. Die Fußbodentemperatur ist zu hoch. Um zu verhindern, dass es wärmer wird als gewünscht, unterbricht Genius den Heizvorgang bis %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                # start from top
                            else:
                                hf_timer = None
                                hf_offset = 0
                                hf_step = 4
                                logger.warning("%s|%s" % (raum.id, u"Modus 4: Intelligente Aufheizphase. Genius hat ermittelt, dass die Fussbodentemperatur nicht übermäßig hoch ist. Der Raum wird weiter mit voller Leistung beheizt, damit schnellstmöglich deine Wunschtemperatur erreicht wird."))
                    if new_mode == 3:  # underswing protection
                        if hf_mode == 1 or hf_mode == 2:  # if comming from mode 1 or 2 switch to step 6, no timer should be active right now
                            hf_offset = 0
                            hf_step = 6
                            if hf_timer is not None:
                                logger.warning("%s|%s" % (raum.id, u"Modus 6.n: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens. Nächste Prüfung um %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                            else:
                                logger.warning("%s|%s" % (raum.id, u"Modus 6.n: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens."))
                            hf_timer = now + timedelta(minutes=flushing_interval)
                        if isinstance(hf_timer, datetime):
                            if hf_timer < (now + timedelta(seconds=10)):
                                # after flushing_interval timer
                                if hf_step == 6:
                                    logger.warning("%s|%s" % (raum.id, u"Modus 6: Intelligente Abkühlphase. Die Fußbodentemperatur war zuletzt warm genug. Genius hat den Fußboden deshalb nicht zusätzlich vorgewärmt. Die Wartezeit ist nun vorüber und es startet eine neue Prüfung."))
                                    # go directly to step 5
                                    hf_offset = _hf_offset
                                    hf_timer = now + timedelta(minutes=19)
                                    hf_step = 5
                                    time.sleep(1.2)  # give next log entry another time
                                    logger.warning("%s|%s" % (raum.id, u"Modus 5: Intelligente Abkühlphase. Genius ermittelt die aktuelle Fußbodentemperatur um herauszufinden, ob der Fußboden etwas vorgewärmt werden sollte."))

                                else:  # current step = 5 or 7
                                    if rltemp_average > rlsoll:
                                        hf_offset = 0
                                        hf_step = 6
                                        hf_timer = now + timedelta(minutes=flushing_interval)
                                        logger.warning("%s|%s" % (raum.id, u"Modus 6: Intelligente Abkühlphase. Die Fussbodentemperatur ist warm genug. Genius startet kein zusätzliches vorwärmen des Fussbodens. Nächste Prüfung um %s." % hf_timer.strftime("%H:%M %d.%m.%Y")))
                                    else:
                                        hf_offset = _hf_offset
                                        hf_step = 7
                                        hf_timer = now + timedelta(minutes=9)
                                        logger.warning("%s|%s" % (raum.id, u"Modus 7: Intelligente Abkühlphase. Der Raum ist noch zu warm aber die Fussbodentemperatur ist recht niedrig. Genius wärmt den Fußboden für dich etwas vor."))
                            else:
                                # logger.warning("%s|%s" % (raum.id, u"Modus 3, Schritt %s*. Offset=%s, Timer=%s." % (hf_step, hf_offset, hf_timer.strftime("%H:%M %d.%m.%Y"))))
                                pass

                        else:  # underswing protection - first init
                            hf_offset = _hf_offset
                            hf_timer = now + timedelta(minutes=19)
                            hf_step = 5
                            logger.warning("%s|%s" % (raum.id, u"Modus 5: Intelligente Abkühlphase. Genius ermittelt die aktuelle Fußbodentemperatur um herauszufinden, ob der Fußboden etwas vorgewärmt werden sollte."))

                if new_mode == 1 or new_mode == 4:
                    try:
                        log_onAT(haus, raum, now, rparams, None, None)
                    except Exception as e:
                        logging.exception("message")
                if rlsoll is not None:
                    # nur loggen, wenn der wert auch berechnet wurde. ansonsten ist rlsoll ja auch irrelevant.
                    log_parameters(rlsoll, rparams, raum, haus, now)

                hf_mode = new_mode
                vals = {'timer': hf_timer, 'offset': hf_offset, 'step': hf_step, 'mode': hf_mode,
                        'raumziel': hf_raum_ziel, 'ts': hf_calc_ts, 'flushing_interval': flushing_interval}
                ch.set("hfo_%s_%s" % (haus.id, raum.id), vals, 3600)  # todo 600

                if for_raum:
                    return hf_offset


@shared_task(base=CeleryErrorLoggingTask)
def calc_hftemps(for_raum=None):

    for haus in Haus.objects.all():

        # if 'heizflaechenoptimierung' not in haus.get_modules():
        #     continue
        hparams = haus.get_module_parameters()
        if hparams.get('aha', {}).get('is_active', False):
            continue

        gateways = list(chain(Gateway.objects.filter(haus=haus), KNXGateway.objects.filter(haus=haus)))
        for gateway in gateways:
            out2raum = {}
            if (isinstance(gateway, Gateway) and "6.00" > gateway.version >= "5.00") or isinstance(gateway, KNXGateway):
                belowbounds = {}
                inbounds = {}
                for ausgang in gateway.ausgaenge.all():
                    raum = ausgang.regelung.get_raum()
                    if raum is None or ausgang.regelung.regelung not in ['ruecklaufregelung', 'zweipunktregelung']:
                        continue

                    if isinstance(gateway, KNXGateway):
                        if not ausgang.is010v():
                            continue

                    ms = raum.get_mainsensor()

                    try:
                        mstemp = ms.get_wert()
                        mstemp = mstemp[0]
                    except (TypeError, AttributeError):
                        logger.warning("%s|%s" % (raum.id, u"Status: Keine Raumtemperatur verfügbar, Berechnung wird übersprungen."))
                        continue

                    _belowbounds, _inbounds = _simple_bounded_control(raum, mstemp, gateway, ausgang)
                    belowbounds.update(_belowbounds)
                    inbounds.update(_inbounds)

                    for out in ausgang.ausgang.split(','):
                        if out.strip().isdigit():
                            out = int(out.strip())
                        out2raum[out] = raum

                params = gateway.get_parameters()
                max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
                max_open = dict((k, int(v)) for k, v in max_open.items())
                rb_min = max(params.get('regelbereich_min', 0), 0)
                rb_max = min(params.get('regelbereich_max', 100), 99)
                unused_capacity = sum([max_open[o] - v for o, v in inbounds.items()])
                diffsum = sum([99 - v for v in belowbounds.values()])
                for out, val in belowbounds.items():
                    if diffsum > 0:
                        new_val = min(99, val + unused_capacity * (99 - val) / diffsum)
                    else:
                        new_val = val
                    new_val = regelbereich(new_val, rb_min, rb_max)
                    ch.set("%s_%s" % (gateway.name, out), "<%s>" % new_val)
                    logger.warning("%s|%s" % (out2raum[out].id, u"Status: Raumtemperatur zu niedrig, Ausgang %s an %s für 20 Minuten maximal geöffnet." % (out, gateway.name)))
                    logger.warning("%s|%s" % (out2raum[out].id, u"Ausgang %s auf %s%%. Schnelles Aufheizen." % (out, new_val)))


def _simple_bounded_control(raum, mstemp, gateway, ausgang):

    macaddr = gateway.name
    params = gateway.get_parameters()
    max_open = params.get('max_opening', dict((i, 100) for i in range(1, 16)))
    rb_min = max(params.get('regelbereich_min', 0), 0)
    rb_max = min(params.get('regelbereich_max', 100), 99)

    roffsets = get_module_offsets(raum, exclude='heizflaechenoptimierung', as_dict=True)
    roffset = sum(roffsets.values())

    regparams = raum.regelung.get_parameters()
    hfo_params = get_strategy_mode_values(raum)

    flushing_interval = timer = ts = step = None
    berlin = pytz.timezone('Europe/Berlin')
    now = datetime.now(berlin)

    if raum.regelung.regelung == 'zweipunktregelung':
        activity_threshold_below_target = 0
        activity_threshold_above_target = 0
    else:
        activity_threshold_below_target = hfo_params.get('before', -0.5)  # 'before' ist negativ
        activity_threshold_above_target = hfo_params.get('after', 0.5)  # 'after' ist ein positiver wert

    try:
        mode = BaseMode.get_active_mode_raum(raum)
        raum.refresh_from_db()
    except Exception as e:
        logging.exception("hfo: exception getting mode for raum %s" % raum.id)
        mode = None
    if mode is None or mode.get('use_offsets', False):
        ziel = raum.solltemp + roffset
    else:
        ziel = raum.solltemp

    if fabs(ziel - hfo_params.get('points_for', ziel)) > 0.25:
        try:
            from ki.ahk import adjust_ondemand
            adjust_ondemand.delay(raum, ziel)
        except Exception:
            logging.exception("exc raumid %s" % raum.id)

    rlsoll = _get_rlsoll(raum, hfo_params, zielit=ziel)
    rlsoll_offset = (ziel - mstemp) * hfo_params.get('correction_heating_element', 3)
    try:
        rlsoll += rlsoll_offset if 'ki' not in raum.get_module() else 0
    except:  # rlsoll kann ja None sein
        pass

    ch.set("rlsoll_%s" % raum.id, rlsoll)

    inbounds = {}
    belowbounds = {}
    setcache = True
    if mstemp <= ziel + activity_threshold_below_target:

        step = 1
        ts = now
        timer = datetime.utcnow() + timedelta(minutes=20)

        last = ch.get("hfo_%s_%s" % (gateway.haus_id, raum.id)) or {}
        raum_timer = ch.get("010v_hfo_timer_raum_%s" % raum.id)
        if raum_timer and last.get('step') == 1:
            # wenns einen timer gibt und der hier gesetzt wurde, dann erneuern wir die gecachten werte...
            ch.set("010v_hfo_timer_raum_%s" % raum.id, raum_timer, time=(raum_timer - datetime.utcnow()).total_seconds())
            ch.set("hfo_%s_%s" % (gateway.haus_id, raum.id), last)
            for ausgangno in ausgang.ausgang.split(','):
                val = regelbereich(int(max_open[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()]), rb_min, rb_max)
                belowbounds[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()] = val
                ch.set("%s_%s" % (macaddr, ausgangno.strip()), "<%s>" % val)
            return belowbounds, inbounds
        else:
            # ... ansonsten sind wir hier reingerutscht und setzen jetzt alles auf 100
            ch.set("010v_hfo_timer_raum_%s" % raum.id, datetime.utcnow() + timedelta(minutes=20), time=20 * 60)
            for ausgangno in ausgang.ausgang.split(','):
                # todo koennte man cachen, sollte aber eh wieder ueberschrieben werden
                val = regelbereich(int(max_open[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()]), rb_min, rb_max)
                belowbounds[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()] = val
                ch.set("%s_%s" % (macaddr, ausgangno.strip()), "<%s>" % val)

    elif mstemp > ziel + activity_threshold_above_target:

        step = 8
        ts = now
        flushing_interval = _get_flushing_interval(raum, hfo_params)
        if flushing_interval is None:
            ch.delete("hfo_%s_%s" % (gateway.haus_id, raum.id))
            for ausgangno in ausgang.ausgang.split(','):
                inbounds[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()] = 0
                ch.set("%s_%s" % (macaddr, ausgangno.strip()), "<0>")
            return belowbounds, inbounds
        timer = datetime.utcnow() + timedelta(minutes=flushing_interval)

        last = ch.get("hfo_%s_%s" % (gateway.haus_id, raum.id)) or {}
        raum_timer = ch.get("010v_hfo_timer_raum_%s" % raum.id)
        if raum_timer and last.get('step') == 8:
            # wenns einen timer gibt und der hier gesetzt wurde, dann erneuern wir die gecachten werte...
            ch.set("010v_hfo_timer_raum_%s" % raum.id, raum_timer, time=(raum_timer - datetime.utcnow()).total_seconds())
            ch.set("hfo_%s_%s" % (gateway.haus_id, raum.id), last)
            for ausgangno in ausgang.ausgang.split(','):
                inbounds[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()] = 0
                ch.set("%s_%s" % (macaddr, ausgangno.strip()), "<0>")
            return belowbounds, inbounds
        else:
            # ... ansonsten sind wir hier reingerutscht und setzen jetzt alles auf 0
            ch.set("010v_hfo_timer_raum_%s" % raum.id, datetime.utcnow() + timedelta(minutes=flushing_interval), time=flushing_interval * 60)
            for ausgangno in ausgang.ausgang.split(','):
                # todo s.o.
                inbounds[int(ausgangno.strip()) if str(ausgangno).strip().isdigit() else ausgangno.strip()] = 0
                ch.set("%s_%s" % (macaddr, ausgangno.strip()), "<0>")
                logger.warning("%s|%s" % (raum.id, u"Status: Raumtemperatur zu hoch, Ausgang %s an %s für %s Minuten geschlossen." % (ausgangno.strip(), macaddr, int(flushing_interval))))
                logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf 0%%. Raumtemperatur = %.2f, Ziel = %.2f." % (ausgangno.strip(), mstemp, ziel)))

    else:
        # innerhalb der schwellen

        # todo: das ggf wieder rein, damit man nicht durch die schwellen durchrauscht
        # raum_timer = ch.get("010v_hfo_timer_raum_%s" % raum.id)
        # if raum_timer is not None:
        #     # timer und werte neu setzen, damit sie nicht einfach aus dem cache fliegen
        #     ch.set("010v_hfo_timer_raum_%s" % raum.id, raum_timer, time=(raum_timer - datetime.utcnow()).total_seconds())
        #     ch.set("hfo_%s_%s" % (gateway.haus_id, raum.id), ch.get("hfo_%s_%s" % (gateway.haus_id, raum.id)))
        #     return

        for ssn, outs in regparams['rlsensorssn'].items():
            rlsensor = AbstractSensor.get_sensor(ssn)
            if rlsensor and len(outs):
                try:
                    last_rl = rlsensor.get_wert()[0]
                except TypeError:
                    logger.warning("%s|%s" % (raum.id, u"Status: Keine Rücklauftemperatur für Sensor %s vorhanden. Berechnung wird übersprungen." % rlsensor.name))
                    continue

                # todo koennte man noch um die abweichung von mstemp korrigieren
                hf_temp_correction_per_degree_deviation = hfo_params['correction_heating_element']

                for out in outs:

                    raum_ausgang_timer = ch.get("010v_hfo_timer_raum_%s_%s" % (raum.id, out))
                    if raum_ausgang_timer is not None:
                        # timer und werte neu setzen, damit sie nicht aus dem cache fliegen
                        ch.set("010v_hfo_timer_raum_%s_%s" % (raum.id, out), raum_ausgang_timer, time=(raum_ausgang_timer - datetime.utcnow()).total_seconds())
                        ch.set("hfo_%s_%s" % (gateway.haus_id, raum.id), ch.get("hfo_%s_%s" % (gateway.haus_id, raum.id)))
                        inbounds[out] = regelbereich(min(int(max_open[out]), 30), rb_min, rb_max)  # s.u.
                        continue

                    if rlsoll is None:
                        _rlsoll = "n/a"
                    else:
                        _rlsoll = "%.2f" % (rlsoll + rlsoll_offset)

                    last_out = ch.get("%s_%s" % (macaddr, out))
                    try:
                        last_out = int(last_out.translate(None, "<>!"))
                    except Exception:
                        logging.exception("exc")
                        try:
                            from logmonitor.views import get_logs
                            logs = get_logs(None, gateway.haus, "hfo", str(raum.id))
                            val = None
                            for l in logs:
                                if len(l) >= 3 and "%" in l[2]:
                                    val = int(l[2].split("%")[0].rsplit(' ', 1)[1])
                                    break
                            else:
                                raise Exception("raise to default behaviour")
                            ch.set("%s_%s" % (macaddr, out), "<%s>" % val)
                            step = 9
                            timer = now
                            ts = now
                            logger.warning("%s|%s" % (raum.id, u"Status: Ausgang %s an %s auf %s%% gesetzt.*" % (out, macaddr, min(int(max_open[out]), int(val)))))
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %s.*" % (out, min(int(max_open[out]), int(val)), last_rl, _rlsoll)))
                            inbounds[out] = regelbereich(min(int(max_open[out]), int(val)), rb_min, rb_max)
                            continue
                        except Exception:
                            logging.exception("exc")
                            step = 9
                            timer = now
                            ts = now
                            last_out = int(max_open[out])/2
                            logger.warning("%s|%s" % (raum.id, u"Status: Ausgang %s an %s auf %s%% gesetzt.**" % (out, macaddr, min(int(max_open[out])/2, int(last_out)))))
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %s.**" % (out, min(int(max_open[out]), int(last_out)), last_rl, _rlsoll)))
                            val = regelbereich(min(int(max_open[out])/2, int(last_out)), rb_min, rb_max)
                            ch.set("%s_%s" % (macaddr, out), "<%s>" % val)
                            inbounds[out] = val
                            # logger.warning("%s|%s" % (raum.id, u"Status: Wert wird beibehalten."))  # todo man koennte natuerlich auch schauen, ob rlsoll > oder < last_rl und dann halt auf 0 oder 100 zuruecksetzen
                            continue  # das ist aggressiv. aber wir fallen dadurch ja "nur" aus den schwellen raus.

                    if last_out == 0:
                        # war vorher aus, deswegen erstmal nur auf 30% fahren fuer 20 minuten
                        step = 5
                        timer = now + timedelta(minutes=20)
                        ts = now
                        logger.warning("%s|%s" % (raum.id, u"Status: Ausgang %s an %s war geschlossen. Für 20 Minuten %s%% öffnen, um Rücklauftemperatur zu ermitteln." % (out, macaddr, min(int(max_open[out]), 30))))
                        logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s%%. Spülen." % (out, min(int(max_open[out]), 30))))
                        ch.set("010v_hfo_timer_raum_%s_%s" % (raum.id, out), datetime.utcnow() + timedelta(minutes=20), time=20 * 60)
                        ch.set("%s_%s" % (macaddr, out), "<30>")  # das nicht in val, damit es nicht angepasst wird
                        inbounds[out] = 30

                    else:
                        if rlsoll is None:
                            pass
                        elif last_rl > rlsoll:
                            last_out -= (last_rl - rlsoll) * 2 * hf_temp_correction_per_degree_deviation  # todo 2 damits nicht zu langsam reagiert
                        else:
                            last_out += (rlsoll - last_rl) * 2 * hf_temp_correction_per_degree_deviation

                        last_out = regelbereich(min(int(max_open[out]), int(last_out)), rb_min, rb_max)

                        if last_out == 0:
                            step = 2
                            ts = now
                            flushing_interval = _get_flushing_interval(raum, hfo_params)
                            if flushing_interval is None:
                                ch.delete("hfo_%s_%s" % (gateway.haus_id, raum.id))
                                setcache = False
                            timer = now + timedelta(minutes=flushing_interval)
                            logger.warning("%s|%s" % (raum.id, u"Status: Rücklauftemperatur hoch genug. Ausgang %s an %s für %s Minuten auf %s%% gesetzt." % (out, macaddr, int(flushing_interval), last_out)))
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %.s." % (out, last_out, last_rl, _rlsoll)))
                            ch.set("010v_hfo_timer_raum_%s_%s" % (raum.id, out), datetime.utcnow() + timedelta(minutes=flushing_interval), time=flushing_interval * 60)
                        else:
                            step = 9
                            ts = now
                            timer = now
                            logger.warning("%s|%s" % (raum.id, u"Status: Ausgang %s an %s auf %s%% gesetzt." % (out, macaddr, last_out)))
                            logger.warning("%s|%s" % (raum.id, u"Ausgang %s auf %s%%. RL-Temperatur = %.2f, RL-Ziel = %s." % (out, last_out, last_rl, _rlsoll)))
                        ch.set("%s_%s" % (macaddr, out), "<%s>" % int(last_out))
                        inbounds[out] = int(last_out)

    if setcache:
        ch.set("hfo_%s_%s" % (gateway.haus_id, raum.id),
               {'step': step,
                'timer': timer,
                'ts': ts,
                'raumziel': ziel,
                'calc_hftemps': True,
                'flushing_interval': int(flushing_interval) if flushing_interval is not None else flushing_interval,
                'offset': 0.0
                })

    log_parameters(rlsoll, hfo_params, raum, None, now)

    return belowbounds, inbounds


def regelbereich(val, min_rb, max_rb):

    if val < 0:
        return 0
    elif 0 <= val < min_rb:
        return min_rb
    elif val > max_rb:
        return min(99, max_rb)
    else:
        return val


def _get_weather_forecast_average(raum, hfo_params):

    # todo die forecasts koennte man auch zwischenspeichern fuer jeden shared_task aufruf
    # todo weil dann muss man die wetter.get_10dayforecast nicht fuer jeden raum/ausgang einzeln neu aufrufen
    # todo weil sich fenster ja meistens wahrscheinlich identisch sind

    at_window_start = hfo_params.get('time_start', 0)
    at_window_end = hfo_params.get('time_end', 5)

    msg = u""
    try:
        at_window_end = min(at_window_end, 72)
        # forecast = wetter.get_10dayforecast(raum.etage.haus, int(at_window_end))[int(at_window_start):int(at_window_end)]
        forecast_now = wetter.get_10dayforecast(raum.etage.haus, int(at_window_end))
        forecast = forecast_now[int(at_window_start):int(at_window_end)]
        average = sum([float(f[1]) for f in forecast]) / float(len(forecast))
        log_onAT(None, raum, None, None, forecast_now, average)  # hier wird ein Fehler geschmissen in "/home/pi/rpi/logger/models.py", line 189, in write_temp_log
    except Exception as e:
        logging.exception("exc")
        msg += u"Status: Keine Außentemperaturvorhersage vorhanden."
        average = None
        haus = raum.etage.haus
        hparams = haus.get_module_parameters()
        s = AbstractSensor.get_sensor(hparams.get('ruecklaufregelung', {}).get('atsensor'))
        if s:
            average = s.get_wert()
            if average is not None:
                msg += u'Verwende stattdessen AT-Sensor.'
                average = average[0]
            else:
                msg += u'Berechnung wird übersprungen.'

    if len(msg):
        logger.warning("%s|%s" % (raum.id, msg))

    return average


def _get_flushing_interval(raum, hfo_params):

    flushing_interval_minustwentyat = hfo_params.get('flushing_interval_start', 60)
    flushing_interval_plustwentyat = hfo_params.get('flushing_interval_end', 300)
    average = _get_weather_forecast_average(raum, hfo_params)

    a = (flushing_interval_plustwentyat, 20)
    b = (flushing_interval_minustwentyat, -20)
    c = (10, average)
    d = (600, average)
    try:
        flushing_interval, y = line_intersection((a, b), (c, d))
    except Exception as e:
        logging.exception("exc")
        flushing_interval = None

    return flushing_interval


def _get_rlsoll(raum, hfo_params, zielit=None):

    average = _get_weather_forecast_average(raum, hfo_params)
    if average is None:
        return None

    if 'ki' in raum.get_modules():

        if hfo_params.get('ki_mode', 'proki') == 'proki':
            points = hfo_params.get('points', [])
            if len(points) == 41:
                intavg = int(average)
                if intavg < -19:
                    return points[0]
                elif intavg > 19:
                    return points[40]
                else:
                    idx = intavg + 20
                    decimal = average - intavg
                    if decimal < 0:
                        sm = points[idx] - decimal*(points[idx-1]-points[idx])
                    elif decimal == 0:
                        sm = points[idx]
                    else:
                        sm = points[idx] + decimal*(points[idx+1]-points[idx])
                    return sm

        elif hfo_params.get('ki_mode') == "on":
            m = hfo_params.get('slope', 0.33)
            itminusat = zielit - average
            rltminusit = m * itminusat
            rlsoll = rltminusit + zielit
            return rlsoll

    hf_temp_plustwentyat = hfo_params.get('heating_element_start', 22)
    hf_temp_minustwentyat = hfo_params.get('heating_element_end', 40)
    a = (hf_temp_plustwentyat, 20)
    b = (hf_temp_minustwentyat, -20)
    c = (20, average)
    d = (80, average)
    try:
        rlsoll, y = line_intersection((a, b), (c, d))
    except Exception as e:
        logging.exception("exc")
        rlsoll = None

    return rlsoll



