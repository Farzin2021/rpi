# coding=utf-8
from datetime import datetime, timedelta

from heizmanager.mobile.m_temp import get_module_offsets
from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus, Raum, AbstractSensor
import pytz
import logging
from django.http import HttpResponse
from django.http import JsonResponse
import heizmanager.cache_helper as ch
import wetter.views as wetter


def get_name():
    return u'Heizflächenoptimierung'


def get_cache_ttl():
    return 600


def is_togglable():
    return True


def is_hidden_offset():
    return True


def calculate_always_anew():
    return False


def get_global_settings_link(request, haus):
    return u"<a href='/m_setup/%s/heizflaechenoptimierung/'>Heizflächenoptimierung</a>" % haus.id


def get_global_description_link():
    desc = u"Bemerkenswerte Komfortsteigerungen bei Fußbodenheizungen."
    desc_link = "http://support.controme.com/heizflaechenoptimierung/"
    return desc, desc_link


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=long(haus))

    context = dict()
    params = haus.get_module_parameters()

    if request.method == "GET":
        context['haus'] = haus
        context['r_m_active'] = params.get('heizflaechenoptimierung_right_menu', True)
        return render_response(request, "m_settings_heizflaechenoptimierung.html", context)

    if 'right_menu_ajax' in request.POST:
        params = haus.get_module_parameters()
        params['heizflaechenoptimierung_right_menu'] = bool(int(request.POST['right_menu_ajax']))
        haus.set_module_parameters(params)
        return JsonResponse({'message': 'done'})

    return render_redirect(request, '/m_setup/%s/heizflaechenoptimierung/' % haus.id)


def get_global_settings_page_help(request, haus):
    pass


def get_offset(haus, raum=None):

    if raum is None:
        return 0.0

    last_vals = ch.get('hfo_%s_%s' % (haus.id, raum.id))
    if last_vals:
        return last_vals.get('offset', 0)

    else:
        from heizflaechenoptimierung.tasks import calc_offset
        offset = calc_offset(raum)
        return offset or 0.0


def activate(hausoderraum):
    if 'heizflaechenoptimierung' not in hausoderraum.get_modules():
        hausoderraum.set_modules(hausoderraum.get_modules() + ['heizflaechenoptimierung'])


def deactivate(hausoderraum):
    modules = hausoderraum.get_modules()
    try:
        modules.remove('heizflaechenoptimierung')
        hausoderraum.set_modules(modules)
    except ValueError:  # nicht in modules
        pass


def line_intersection(line1, line2):
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = det(d, xdiff) / float(div)
    y = det(d, ydiff) / float(div)
    return x, y


def has_ms_and_rls(raum):
    ms = True if raum.get_mainsensor() else False

    rls = False
    if raum.regelung.regelung == "ruecklaufregelung":
        regparams = raum.regelung.get_parameters()
        if len(regparams.get('rlsensorssn', dict())):
            rls = True

    return ms, rls


def get_local_settings_page_haus(request, haus):

    if request.method == "GET":

        hparams = haus.get_module_parameters()
        if 'ki' in haus.get_modules() and hparams.get('wetter_pro', {}).get('is_active', False):
            ki_enabled = True
        else:
            ki_enabled = False

        eundr = []
        for etage in haus.etagen.all():
            e = {etage: []}
            for raum in Raum.objects.filter_for_user(request.user, etage_id=etage.id):
                rparams = get_strategy_mode_values(raum)
                if ki_enabled and raum.get_module_parameters().get('heizflaechenoptimierung', {}).get('ki_mode', 'off') != 'off':  # das kann man ersetzen, wenn die celery prozesse konsequent 'ki' in die raum.modules schreiben
                    ki_status = True
                else:
                    ki_status = False
                e[etage].append((raum, has_ms_and_rls(raum), rparams, ki_status))
            eundr.append(e)

        return render_response(request, "m_heizflaechenoptimierung.html", {"eundr": eundr, "haus": haus})

    elif request.method == "POST":

        if request.POST.get('singleroom', False):
            name = request.POST['name']
            raum = Raum.objects.get(pk=long(request.POST.get('id', 0)))
            rparams = get_strategy_mode_values(raum)
            params = raum.get_module_parameters()
            strategy_mode = params.get('heizflaechenoptimierung', {}).get('strategy', 'custom')
            # switch to custom mode when user change the params
            if strategy_mode != 'custom':
                params.setdefault('heizflaechenoptimierung', raum.get_hfo_params())
                params['heizflaechenoptimierung']['strategy'] = 'custom'

            rparams[name] = float(request.POST['number'])
            params['heizflaechenoptimierung'] = rparams
            raum.set_module_parameters(params)
            return HttpResponse()

        else:
            updparams = {}

            if 'mitt_zeit_active' in request.POST:
                updparams['time_start'] = int(request.POST.get('time_start', 0))
                updparams['time_end'] = min(int(request.POST.get('time_end', 5)), 72)
            if 'heizflaechen_checkbox' in request.POST:
                updparams['heating_element_start'] = float(request.POST.get('heating_element_start', 22))
                updparams['heating_element_end'] = float(request.POST.get('heating_element_end', 40))
            if 'aussen_temp' in request.POST:
                updparams['flushing_interval_start'] = int(request.POST.get('flushing_interval_start', 60))
                updparams['flushing_interval_end'] = int(request.POST.get('flushing_interval_end', 300))
            if 'over-checkbox' in request.POST:
                updparams['before'] = float(request.POST.get('before', -0.5))
            if 'underswing_checkbox' in request.POST:
                updparams['after'] = float(request.POST.get('after', 0.5))
            if 'correction_of_heating' in request.POST:
                updparams['correction_heating_element'] = float(request.POST.get('correction_heating_element', 3))

            raeume = request.POST.getlist('rooms')
            for raum_id in raeume:
                raum = Raum.objects.get(id=long(raum_id))
                params = raum.get_module_parameters()
                params.setdefault('heizflaechenoptimierung',
                                  {'time_start': 0, 'time_end': 5, 'heating_element_start': 22,
                                   'heating_element_end': 40, 'before': -0.5, 'after': 0.5, 'flushing_interval_start': 60,
                                   'flushing_interval_end': 300, 'correction_heating_element': 3})
                params['heizflaechenoptimierung'].update(updparams)
                raum.set_module_parameters(params)

            return render_redirect(request, "/m_heizflaechenoptimierung/%s/" % haus.id)


def get_local_settings_link(request, raum):
    # todo nur in entsprechenden raeumen anzeigen
    if raum.regelung.regelung != 'ruecklaufregelung':
        return ""
    haus = raum.etage.haus
    offset = get_offset(haus, raum)
    if offset != 0.0:
        # wird in new_home.js in 'Normalbetrieb' umgeschrieben und eingefaerbt
        if offset > 0:
            return u"Heizflächenoptimierung", u"heizflaechenoptimierung-red", "Unterschwingschutz", \
                   155, "/m_raum/%s/heizflaechenoptimierung/" % raum.id
        else:
            return u"Heizflächenoptimierung", u"heizflaechenoptimierung-blue", "Überschwingschutz", \
                   155, "/m_raum/%s/heizflaechenoptimierung/" % raum.id
    else:
        return u"Heizflächenoptimierung", u"heizflaechenoptimierung-grey", "Normalbetrieb", \
               155, "/m_raum/%s/heizflaechenoptimierung/" % raum.id


def get_local_settings_page(request, raum):

    mode = ['unset', 'permanently-ON', 'overswing-protection-mode', 'underswing-protection-mode', 'permanently-OFF']

    if request.method == "GET":

        rparams = get_strategy_mode_values(raum)
        at_window_start = rparams['time_start']
        at_window_end = min(72, rparams['time_end'])
        hf_temp_plustwentyat = rparams['heating_element_start']
        hf_temp_minustwentyat = rparams['heating_element_end']
        activity_threshold_below_target = rparams['before']
        activity_threshold_above_target = rparams['after']
        flushing_interval_minustwentyat = rparams['flushing_interval_start']
        flushing_interval_plustwentyat = rparams['flushing_interval_end']
        hf_temp_correction_per_degree_deviation = rparams['correction_heating_element']

        haus = raum.etage.haus
        berlin = pytz.timezone('Europe/Berlin')

        regparams = raum.regelung.get_parameters()
        ms, rls = has_ms_and_rls(raum)
        if ms:
            temp = raum.get_mainsensor().get_wert()
            if temp:
                temp = temp[0]
        else:
            temp = regparams.get('offset', 21.0)

        try:
            forecast = wetter.get_10dayforecast(raum.etage.haus, int(at_window_end))[int(at_window_start):int(at_window_end)]
            average = sum([float(f[1]) for f in forecast])/float((at_window_end-at_window_start) or 1)
        except Exception as e:
            average = forecast = None

        offset = get_module_offsets(raum, exclude='heizflaechenoptimierung', as_dict=True)
        offset = sum(offset.values())
        hf_raum_ziel = raum.solltemp + offset

        try:
            from .tasks import _get_rlsoll
            rlsoll = _get_rlsoll(raum, rparams, hf_raum_ziel)  # uebernommen von unten. ist offensichtlich falsch, weil das nur raum.solltemp+hfo_offset ist und nicht m_temp.get_module_offset(). ist aber wohl noch niemandem aufgefallen.
        except:
            a = (hf_temp_plustwentyat, 20)
            b = (hf_temp_minustwentyat, -20)
            c = (20, average)
            d = (80, average)
            try:
                rlsoll, y = line_intersection((a, b), (c, d))
            except Exception as e:
                rlsoll = None

        last_vals = ch.get('hfo_%s_%s' % (haus.id, raum.id))
        if last_vals is None:
            logging.warning("keine last_vals")
            hf_timer = None
            hf_offset = 0
            hf_step = 0
            hf_raum_ziel = raum.solltemp + get_offset(raum.etage.haus, raum)
            hf_calc_ts = None
            hf_mode = 0
            flushing_interval = None
            calc_hftemps = False
        else:
            hf_timer = last_vals.get('timer')
            hf_offset = last_vals.get('offset')
            hf_step = last_vals.get('step')
            hf_raum_ziel = last_vals.get('raumziel')
            hf_calc_ts = last_vals.get('ts')
            flushing_interval = last_vals.get('flushing_interval')
            calc_hftemps = last_vals.get('calc_hftemps', False)

        if hf_calc_ts:
            last_check = hf_calc_ts.strftime('%a %H:%M')
            next_check = (hf_calc_ts + timedelta(minutes=10)).strftime('%a %H:%M')
        else:
            last_check = "-"
            now = datetime.now(berlin)
            next_check = (now + timedelta(minutes=(10-(now.minute % 10)))).strftime('%a %H:%M')

        try:
            rlsoll_offset = (hf_raum_ziel - float(temp)) * hf_temp_correction_per_degree_deviation
            correct_heating_element_temp = rlsoll + rlsoll_offset
        except Exception as e:
            logging.exception("message")
            rlsoll = rlsoll_offset = correct_heating_element_temp = None

        heizflaechen_next = heizflaechen_since = None
        try:
            if hf_step == 7:
                heizflaechen_since = hf_timer - timedelta(minutes=10)
            if hf_step == 5:
                heizflaechen_since = hf_timer - timedelta(minutes=20)
            if hf_step == 4:
                heizflaechen_next = hf_calc_ts + timedelta(minutes=10)
                heizflaechen_next = heizflaechen_next.strftime("%a %H:%M")
                heizflaechen_since = hf_calc_ts
            if hf_step == 6:
                heizflaechen_since = hf_timer - timedelta(minutes=flushing_interval)
            if hf_step == 2:
                heizflaechen_since = hf_timer - timedelta(minutes=flushing_interval)
            if hf_step == 3:
                heizflaechen_since = hf_timer - timedelta(minutes=20)

            # we don't have since, so we need to fill it something to not crash
            if hf_step == 1 or hf_step == 8 or hf_step == 4:
                heizflaechen_since = hf_calc_ts

            heizflaechen_since_str = heizflaechen_since.strftime('%a %H:%M')
            heizflaechen_timer_str = hf_timer.strftime('%a %H:%M')
        except Exception as e:
            logging.exception("message")
            heizflaechen_since_str = heizflaechen_timer_str = None

        number_average = 0.0
        i = 0
        rls = []
        for ssn, outs in regparams.get('rlsensorssn', dict()).items():
            sensor = AbstractSensor.get_sensor(ssn)
            if not sensor:
                continue
            last_rl = sensor.get_wert()
            if last_rl:
                number_average += float(last_rl[0])
                i += 1
            rls.append((last_rl[0] if last_rl else "-", raum.solltemp, sensor.description or sensor.name))
        if i != 0:
            rltemp_average = "%.2f" % (number_average / i)
        else:
            rltemp_average = None

        if 'ki' in haus.get_modules() and haus.get_module_parameters().get('wetter_pro', {}).get('is_active', False) and raum.get_module_parameters().get('heizflaechenoptimierung', {}).get('ki_mode', 'off') != 'off':
            ki_mode = True
        else:
            ki_mode = False

        page_param = {"raum": raum, "haus": haus, "next_check": next_check, "last_check": last_check, "time_start": at_window_start,
                      "time_end": at_window_end, "heating_element_start": hf_temp_plustwentyat,
                      "heating_element_end": hf_temp_minustwentyat, "before": activity_threshold_below_target,
                      "after": activity_threshold_above_target,
                      "flushing_interval_start": flushing_interval_minustwentyat,
                      "flushing_interval_end": flushing_interval_plustwentyat,
                      "rlsensoren": rls, 'hf_offset': hf_offset, "rltemp_average": rltemp_average or '-',
                      "raumtemperatur": temp or '-', "forecast": forecast, "at_average": ("%.2f" % average) if average is not None else '-',
                      "heating_element_temp": rlsoll,
                      "correction_heating_element": hf_temp_correction_per_degree_deviation,
                      "correction_heating_element_offset": rlsoll_offset,
                      "correct_heating_element_temp": correct_heating_element_temp,
                      "flushing_interval": flushing_interval or '-', "heizflaechen_step": hf_step,
                      "heizflaechen_since": heizflaechen_since_str, "heizflaechen_next": heizflaechen_next,
                      "heizflaechen_timer": heizflaechen_timer_str, "heizflaechen_target_temp": hf_raum_ziel, 'calc_hftemps': calc_hftemps, 'ki_mode': ki_mode}
        return render_response(request, "m_raum_heizflaechenoptimierung.html", page_param)

    elif request.method == "POST":
        
        if request.POST.get('singleroom', False):
            name = request.POST['name']
            obj_id = long(request.POST.get('id', 0))
            number = request.POST['number']
            raum = Raum.objects.get(id=obj_id)
            rparams = get_strategy_mode_values(raum)
            params = raum.get_module_parameters()

            params.setdefault('heizflaechenoptimierung', raum.get_hfo_params())
            strategy_mode = params.get('heizflaechenoptimierung', {}).get('strategy', 'custom')

            # switch to custom mode when user change the params
            if strategy_mode != 'custom':
                rparams['strategy'] = 'custom'
            
            if name == "time_start":
                rparams['time_start'] = int(number)
            if name == "time_end":
                rparams['time_end'] = min(72, int(number))
            if name == "heating_element_start":
                rparams['heating_element_start'] = float(number)
            if name == "heating_element_end":
                rparams['heating_element_end'] = float(number)
            if name == "flushing_interval_start":
                rparams['flushing_interval_start'] = int(number)
            if name == "flushing_interval_end":
                rparams['flushing_interval_end'] = int(number)
            if name == "before":
                rparams['before'] = float(number)
            if name == "after":
                rparams['after'] = float(number)
            if name == "correction_heating_element":
                rparams['correction_heating_element'] = float(number)

            params['heizflaechenoptimierung'] = rparams
            raum.set_module_parameters(params)

        return HttpResponse()


def get_regelstrategie(request, raumid):
    raum = Raum.objects.get_for_user(request.user, pk=long(raumid))
    params = raum.get_module_parameters()

    if 'strategy' in params.get('heizflaechenoptimierung', {}):
        strategy_mode = params.get('heizflaechenoptimierung', {}).get('strategy')
    elif 'strategy' in params:
        strategy_mode = params['strategy']
        del params['strategy']
    else:
        strategy_mode = 'custom'

    haus = raum.etage.haus
    hparams = haus.get_module_parameters()
    if 'ki' in haus.get_modules() and hparams.get('wetter_pro', {}).get('is_active', False):
        ki_enabled = True
    else:
        ki_enabled = False

    ki_mode = params.get('heizflaechenoptimierung', {}).get('ki_mode', 'off')

    if request.method == "GET":
        return render_response(request, "m_raum_heizflaechenoptimierung_regelstrategie.html", {"raum": raum, "mode": strategy_mode, 'ki_mode': ki_mode, 'ki_enabled': ki_enabled})

    elif request.method == "POST":

        if request.POST.get('ftype') == 'strategy_mode':
            name = request.POST['selected_mode']  # the name of mode
            if strategy_mode != name:
                params.setdefault('heizflaechenoptimierung', raum.get_hfo_params())
                params['heizflaechenoptimierung']['strategy'] = name
                raum.set_module_parameters(params)
            return HttpResponse()

        elif request.POST.get('ftype') == 'ki_mode':
            name = request.POST['selected_mode']
            if ki_mode != name:
                params.setdefault('heizflaechenoptimierung', raum.get_hfo_params())
                params['heizflaechenoptimierung']['ki_mode'] = name
                raum.set_module_parameters(params)
            return HttpResponse()



def get_strategy_mode_values(raum):
    hfo_params = {'time_start': 0, 'time_end': 5, 'heating_element_start': 22, 'heating_element_end': 40,
                  'before': -0.5, 'after': 0.5, 'flushing_interval_start': 60, 'flushing_interval_end': 300,
                  'correction_heating_element': 3}
    _hfo_params = raum.get_hfo_params()
    hfo_params.update(_hfo_params)
    mode = hfo_params.get('strategy', 'custom')
    if mode == 'Wohnraum':
        hfo_params.update({'time_start': 0, 'time_end': 5, 'heating_element_start': 22,
                            'heating_element_end': 37, 'before': -0.5, 'after': 0.5, 'flushing_interval_start': 60,
                            'flushing_interval_end': 300, 'correction_heating_element': 3})
    elif mode == 'Bad_und_Wellness':
        hfo_params.update({'time_start': 0, 'time_end': 5, 'heating_element_start': 24,
                            'heating_element_end': 41, 'before': -0.5, 'after': 4, 'flushing_interval_start': 60,
                            'flushing_interval_end': 300, 'correction_heating_element': 2})
    elif mode == 'Schlafraum':
        hfo_params.update({'time_start': 0, 'time_end': 5, 'heating_element_start': 15,
                            'heating_element_end': 24, 'before': -2, 'after': 0, 'flushing_interval_start': 180,
                            'flushing_interval_end': 500, 'correction_heating_element': 2})
    elif mode == 'Nebenraum':
        hfo_params.update({'time_start': 0, 'time_end': 5, 'heating_element_start': 22,
                            'heating_element_end': 37, 'before': -0.5, 'after': 0, 'flushing_interval_start': 120,
                            'flushing_interval_end': 500, 'correction_heating_element': 3})
    return hfo_params