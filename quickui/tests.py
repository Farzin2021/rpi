from heizmanager.tests import BaseModeTest, TaskTest
from datetime import datetime, timedelta
import pytz
from .views import QuickuiMode, QuickuiTask


class QuickUiTest(BaseModeTest, TaskTest):

    def setUp(self):
        super(QuickUiTest, self).setUp('quickui/fixtures/fixture.json')

    def test_SetZieltempSingleRoomUi(self):
        # initial
        temperature_to_set_quickui = 20.5
        duration = 180
        # set quickui through UI
        self.set_hand_mode_single_room_ui(room_id=1,
                                          slidernumber=temperature_to_set_quickui,
                                          duration=duration)
        self.assert_hand_mode_is_activated_in_room(1, temperature_to_set_quickui)
        self.assert_raum_solltemp_equal(1, temperature_to_set_quickui)

    def test_SetZieltempSingleRoomAPI(self):
        room_id, temperature, duration = 1, 20.5, 180
        raum = QuickUiTest.get_raum_by_id(room_id)
        qui_mode = QuickuiMode(temperature, duration)
        qui_mode.set_mode_raum(raum)
        self.assert_hand_mode_is_activated_in_room(room_id, temperature)
        self.assert_hand_mode_is_set_in_db(room_id, temperature, duration)
        self.assert_hand_mode_room_is_set_in_room_ui(room_id, temperature)

    def test_GetModeRaumFunctionality(self):
        room_id, temperature, duration = 1, 20.5, 180
        mode = self.set_hand_mode_single_room_db(1, temperature, duration)
        ex_time = mode['time']
        raum = self.get_raum_by_id(1)
        mode = QuickuiMode.get_mode_raum(raum)
        self.assertIsInstance(mode, dict)
        self.assertIn('soll', mode)
        self.assertEqual(mode['soll'], temperature)
        self.assertEqual(mode['activated_mode_text'], ex_time)

    def test_SetZieltempSingleRoomByRaumMode(self):
        self.set_hand_mode_single_room_db(room_id=1, slidernumber=20.99, duration=180)
        raum = self.get_raum_by_id(1)
        self.assert_hand_mode_is_activated_in_room(raum.id, 20.99)
        self.assert_raum_solltemp_equal(raum.id, 20.99)

    def test_ExpireZieltempSingleRoom(self):
        room_id = 1
        self.set_expired_hand_mode_single_room(room_id=room_id)
        self.assert_hand_mode_is_not_set(room_id)
        self.run_modules_timer()
        self.assert_raum_solltemp_equal(room_id, 21.0)

    def test_ExpireZieltempSingleRoomFunctionality(self):
        room_id = 1
        raum = self.get_raum_by_id(1)
        r_params = raum.get_module_parameters()
        r_params['temp_solltemp'] = 21.0
        raum.set_module_parameters(r_params)
        self.set_expired_hand_mode_single_room(room_id=room_id, slidernumber=24)
        raum.refresh_from_db()
        self.assert_raum_solltemp_equal(1, 24)
        self.run_modules_timer()
        self.assert_raum_solltemp_equal(1, 21.0)

    def test_SetAZieltempWithActiveTemperaturszeneInSingleRoom(self):
        self.set_temperaturszene_in_single_room_ui(room_id=3, mode_id=2)
        self.assert_temperatureszene_is_activated_in_room(3, 2)
        self.set_hand_mode_single_room_ui(3, 21.5, 60)
        self.assert_hand_mode_is_activated_in_room(3, 21.5)
        self.assert_raum_solltemp_equal(3, 21.5)

    def testExpireAZieltempWithActiveTemperaturszeneInSingleRoom(self):
        room_id = 3
        mode_id = 2
        self.set_temperaturszene_in_single_room_ui(room_id=room_id, mode_id=mode_id)
        self.set_expired_hand_mode_single_room(room_id=room_id)
        self.run_modules_timer()
        self.assert_temperatureszene_is_set_in_db(room_id, mode_id)
        self.assert_temperatureszene_is_activated_in_room(room_id, mode_id)
        self.assert_temperaturszene_is_set_in_room_ui(room_id, mode_id)
        self.assert_raum_solltemp_equal(3, 21.5)

    def test_SetExpirationTimeFollowedByZieltempInSingleRoom(self):

        berlin = pytz.timezone('Europe/Berlin')
        calculated_ex_time = self.berlin_now + timedelta(minutes=180)
        response = self.set_hand_mode_single_room_ui(3, 20.5, 180)
        # this is a upcoming ro precious round time of 5
        returned_ex_time = response.get('time')
        returned_ex_time = '%s %s' % (datetime.strftime(self.berlin_now, '%Y-%m-%d'), returned_ex_time)
        returned_ex_time = berlin.localize(datetime.strptime(returned_ex_time, '%Y-%m-%d %H:%M'))
        if calculated_ex_time > returned_ex_time:
            time_diff = calculated_ex_time - returned_ex_time
        else:
            time_diff = returned_ex_time - calculated_ex_time
        self.assertLess(time_diff.seconds, 5 * 60)

    def test_SetDifferentZieltempsInTwoDifferentRooms(self):
        # room 1
        self.set_hand_mode_single_room_ui(room_id=1, slidernumber=20.99, duration=180)
        self.assert_hand_mode_is_activated_in_room(1, 20.99)
        self.assert_raum_solltemp_equal(1, 20.99)
        # room 2
        self.set_hand_mode_single_room_ui(room_id=2, slidernumber=25.0, duration=5)
        self.assert_hand_mode_is_activated_in_room(2, 25.0)
        self.assert_raum_solltemp_equal(2, 25.0)

    def test_ExpireZieltempInSingleRoomWhileZieltempInDifferentRoomLivesOn(self):
        # room 1
        self.set_hand_mode_single_room_ui(room_id=1, slidernumber=20.5, duration=180)
        self.assert_hand_mode_is_activated_in_room(1, 20.5)
        self.assert_raum_solltemp_equal(1, 20.5)
        # expire room 2
        self.set_expired_hand_mode_single_room(2)
        self.assert_hand_mode_is_not_set(room_id=2)
        # 22.0 is saved in fixture.json, db
        self.assert_raum_solltemp_equal(3, 19.4)

    def test_DeleteZieltempInMultipleRoomsThroughUI(self):
        # set ziel temp for room 1 and 2
        self.set_hand_mode_single_room_ui(room_id=1, slidernumber=20.99, duration=180)
        self.set_hand_mode_single_room_ui(room_id=2, slidernumber=22.22, duration=60)
        # assert quick mode is activated in room 1,2
        self.assert_hand_mode_is_activated_in_room(1, 20.99)
        self.assert_hand_mode_is_activated_in_room(2, 22.22)
        # call to delete all hand modes
        response = self.client.post('/quick-change/', {
            'set_all_time': True,
            'slider_time_val': 0,
            'delete_all': True
        })
        # assert response
        self.assertEqual(response.status_code, 200)
        self.assertIn('id_list', response.json())
        self.assertEqual(response.json()['id_list'], [1, 2, 3, 4])
        # asserts hand mode is no longer activated in room 1,2
        self.assert_hand_mode_is_not_set(1)
        self.assert_hand_mode_is_not_set(2)

    def test_SetZieltempInSingleRoomAndCheckWhetherGatewayAusgangInThatRoomChangesState(self):
        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.91, duration=180)
        self.assert_room_state_is_on(3)
        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.80, duration=180)
        self.assert_room_state_is_off(3)

    def test_ExpireZieltempInSingleAndCheckWhetherGatewayAusgangInThatRoomChangesState(self):
        self.set_expired_hand_mode_single_room(4, 26)
        self.assert_room_state_is_off(4)

    def test_ManuallyActivatingATemperaturszeneDoesNotOverrideAnActiveHandMode(self):
        # room 1
        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.99, duration=180)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.set_temperaturszene_in_single_room_ui(3, 2)
        # assert still hand mode is activated
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.assert_raum_solltemp_equal(3, 20.99)

    def test_HeizprogrammSwitchingTemperaturszeneDoesNotOverrideAnActiveHandModeDB(self):

        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.99, duration=180)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.set_heizprogramm_entry_db(mode_id=2)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.run_auto_switch()
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.assert_raum_solltemp_equal(3, 20.99)

    def test_HeizprogrammSwitchingTemperaturszeneDoesNotOverrideAnActiveHandModeUI(self):
        self.set_hand_mode_single_room_ui(room_id=3, slidernumber=20.99, duration=180)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.set_heizprogramm_entry_ui(2, self.berlin_now)
        day_of_week = self.berlin_now.weekday()
        str_now_time = self.berlin_now.strftime("%H:%M")
        self.assert_heizprogramm_is_set_db(2, str_now_time, day_of_week)
        self.run_auto_switch()
        # temperatureszene should be set in db correctly to be activated after hand mode expiration
        self.assert_temperatureszene_is_set_in_db(3, 2)
        self.assert_hand_mode_is_activated_in_room(3, 20.99)
        self.assert_raum_solltemp_equal(3, 20.99)

    def test_GetTaskList(self):
        haus = self.get_haus_by_id(1)
        self.set_hand_mode_single_room_ui(1)
        self.set_hand_mode_single_room_ui(2, 20.5, 20)
        tasks = QuickuiTask.get_module_tasks(haus)
        self.assertEqual(len(tasks), 2)

    def test_ExpireHandModeByModuleTimer(self):
        self.set_expired_hand_mode_single_room(1, 25.0)
        self.run_modules_timer()
        self.assert_hand_mode_is_not_set(1)
