from datetime import datetime
from datetime import timedelta


def get_str_formatted_time(time):
    return time.strftime("%Y-%m-%d %H:%M")


def ret_formatted_time_from_str(str_time):
    return datetime.strptime(str_time, '%Y-%m-%d %H:%M')


def get_nearest_time(tm, pivot=5, nearest=10):
    discard = timedelta(minutes=tm.minute % nearest,
                        seconds=tm.second,
                        microseconds=tm.microsecond)
    tm -= discard
    if discard >= timedelta(minutes=pivot):
        tm += timedelta(minutes=nearest)
    return tm


def fix_temperature_value(temp):
    if isinstance(type(temp), str):
        temp = float(temp.replace(',', '.'))
    return temp
