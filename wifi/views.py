# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Haus
from fabric.operations import local
import re


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/wifi/'>Wi-Fi</a>" % haus.id


def connect_wifi(ssid, password):
    # writing wifi config to related file
    config = local("sudo cat /etc/wpa_supplicant/wpa_supplicant.conf", capture=True)
    if 'country' not in config:
        config = "country=DE\n" + config

    # search for ssid and psk if exists otherwise change them
    if 'ssid' not in config:
        config = config + '\nnetwork={\nssid="%s"\npsk="%s"\n}\n' % (ssid, password)
    else:
        config = re.sub(r'ssid=".*"', 'ssid="%s"' % ssid, config)
        config = re.sub(r'psk=".*"', 'psk="%s"' % password, config)
    command = "printf '%s\n' '{}' | sudo tee /etc/wpa_supplicant/wpa_supplicant.conf".format(config)
    local(command)
    return True


def get_global_settings_page(request, haus, action=None, entityid=None):

    if not isinstance(haus, Haus):
        haus = Haus.objects.get(pk=int(haus))

    if request.method == "GET":
        wifi_data = haus.get_spec_module_params('wifi')
        return render_response(request, 'm_install_wifi.html', {'haus': haus, 'params': wifi_data})

    elif request.method == "POST":
        wifi_data = {
            'ssid': request.POST.get('ssid', ''),
            'password': request.POST.get('password', '')
        }
        haus.set_spec_module_params('wifi', wifi_data)
        # prevent to leave ssid empty in config file
        if wifi_data['ssid'] == '':
            wifi_data['ssid'] = 'notset'
        connect_wifi(wifi_data['ssid'], wifi_data['password'])
        return render_redirect(request, "/m_setup/%s/wifi/" % haus.id)
