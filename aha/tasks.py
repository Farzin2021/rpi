# -*- coding: utf-8 -*-

import logging
from celery import shared_task
from heizmanager.models import Haus, Gateway, AbstractSensor, GatewayAusgang
from datetime import datetime, timedelta
import json
import pytz
from logger.models import SimpleSensorLog
from heizmanager.mobile.m_error import CeleryErrorLoggingTask
import math
try:
    import pandas as pd
except ImportError:
    pd = None
try:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
except ImportError:
    plt = mdates = None


logger = logging.getLogger('logmonitor')


@shared_task(base=CeleryErrorLoggingTask)
def testrun():

    for haus in Haus.objects.all():
        hparams = haus.get_module_parameters()
        if not hparams.get('aha', {}).get('is_active', False):
            continue

        berlin = pytz.timezone("Europe/Berlin")
        start = datetime.strptime(hparams['aha']['start'], "%Y-%m-%d %H:%M")
        start = berlin.localize(start)
        auswertung, fenster, logs = _get_current_testrun(haus.id)

        run_minutes = (datetime.now(berlin) - start).total_seconds() / 60

        if run_minutes > 300:
            if fenster >= 5:
                # fertig
                _save_testrun(haus, logs, auswertung)
            else:
                # wir haben nicht genug fenster, es ist aber auch schon zu viel zeit vergangen. wir brechen ab.
                _save_testrun(haus, logs, auswertung, msg="_mangelhafte-daten")

        else:
            # testlauf geht weiter. nicht genug fenster, aber zeit noch nicht abgelaufen.
            pass


def _get_current_testrun(hausid):

    haus = Haus.objects.get(pk=hausid)
    hparams = haus.get_module_parameters()
    if not hparams.get('aha', {}).get('is_active'):
        return None, None, None

    start = hparams['aha']['start']

    gateways = Gateway.objects.filter(haus=haus)
    logs = _get_logs(gateways, start)
    df = _convert_logs_to_df(logs)
    df = _calc_differences(df)

    time_inc = 0
    try:
        zero = df.loc[0]
    except KeyError:
        # leer
        return {}, None, {}
    mean_deviation = {}  # je positiver die abweichung vom mittel ist, desto weiter liegt das sensormittel ueber dem allg. mittel
    force_end = False
    while (df.iloc[-1].Zeit - zero.Zeit).total_seconds > 1800:
        window = df.loc[(df.Zeit > zero.Zeit + timedelta(minutes=time_inc)) & (df.Zeit < zero.Zeit + timedelta(minutes=time_inc + 22))]

        try:
            if window.iloc[-1].Zeit == df.iloc[-1].Zeit:
                # fensterende ist letztes element von df, fertig nach bearbeitung von diesem fenster
                break
        except IndexError:
            # wenn ein gruppierter Sensor keinen Wert hat hinten im Fenster
            break

        for r in window.groupby("Sensorname").apply(_cnt_diffs):
            if not r:
                break
        else:
            gba = window.groupby('Sensorname').aggregate(['min', 'max', 'mean', 'first', 'last'])
            mom = gba.Wert['mean'].mean()
            for i in range(len(gba)):
                if gba.iloc[i].Wert[2] > 45:
                    # testlauf beenden, weil fensterschnitt bei einem sensor >45K. msg uebergeben.
                    # am besten hier flag setzen und dann am ende _save_testrun aufrufen
                    force_end = True
                mean_deviation.setdefault(gba.iloc[i].name, list())
                mean_deviation[gba.iloc[i].name].append(gba.iloc[i].Wert[2] - mom)

        time_inc += 7

    auswertung = dict((sensor, sum(devs) / len(devs)) for sensor, devs in mean_deviation.items())

    if force_end:
        _save_testrun(haus, logs, auswertung, msg="_abbruch-rltemp-zu-hoch")

    return auswertung, len(mean_deviation.values()[0]) if len(mean_deviation.values()) else 0, logs


def _save_testrun(haus, logs, auswertung, msg=None):

    hparams = haus.get_module_parameters()
    start = datetime.strptime(hparams['aha']['start'], "%Y-%m-%d %H:%M")

    run_data = {'opening_changes': {}, 'msg': msg}
    for gateway in Gateway.objects.filter(haus=haus):
        gwparams = gateway.get_parameters()
        if not gwparams.get('ahaparams', {}).get('is_active'):
            continue

        if msg != "_mangelhafte-daten":
            max_opening = gwparams.get('max_opening', dict((i, 100) for i in range(1, 16)))
            rb_min = gwparams.get('regelbereich_min', 0)
            rb_max = gwparams.get('regelbereich_max', 100)
            run_data['opening_changes'][gateway.name] = {}
            if len(logs) and len(auswertung):
                selected_outs = gwparams['ahaparams']['outs']
                gwausgaenge = GatewayAusgang.objects.filter(gateway=gateway)
                for gwausgang in gwausgaenge:
                    regparams = gwausgang.get_regelung().get_parameters()
                    ausgaenge = [int(a.strip()) for a in gwausgang.ausgang.split(', ') if len(a)]
                    for ausgang in ausgaenge:
                        if ausgang in selected_outs:
                            run_data['opening_changes'][gateway.name][ausgang] = {}
                            for sensorid, outs in regparams.get('rlsensorssn', {}).items():
                                sensor = AbstractSensor.get_sensor(sensorid)
                                if sensor:
                                    run_data['opening_changes'][gateway.name][ausgang]['sensorname'] = sensor.description
                                if ausgang in outs:
                                    abweichung = auswertung.get(sensorid, 0)
                                    run_data['opening_changes'][gateway.name][ausgang]['abweichung'] = abweichung
                                    opening = int(max_opening[ausgang])
                                    run_data['opening_changes'][gateway.name][ausgang]['alt'] = opening
                                    if abweichung > 0.5:
                                        opening -= round(3 * abweichung)
                                    elif abweichung < -1:
                                        opening -= round(2 * abweichung)
                                    opening = min(rb_max, max(rb_min, int(opening)))
                                    run_data['opening_changes'][gateway.name][ausgang]['neu'] = opening
                                    max_opening[ausgang] = str(opening)

            gwparams['max_opening'] = max_opening

        gwparams['ahaparams']['is_active'] = False
        gwparams['ahaparams']['outs'] = []
        gwparams['ahaparams']['sensors'] = []
        gwparams['ahaparams']['start'] = None
        gateway.set_parameters(gwparams)

    diff = 0
    if len(auswertung):
        _auswertung = sorted([(a[0], a[1]) for a in auswertung.items()], key=lambda x: x[1])
        diff = _auswertung[-1][1] - _auswertung[0][1]
        run_data['diff'] = diff
    else:
        run_data['diff'] = None

    fig, ax = plt.subplots(figsize=(18, 6))
    df = _convert_logs_to_df(logs, convert_ids_to_names=True)
    df['Zeit'] = pd.to_datetime(df['Zeit'], format="%Y-%m-%d %H:%M:%S.%f")
    for name, group in df.groupby('Sensorname'):
        # group.plot(x=pd.to_datetime(group['Zeit'], format="%Y-%m-%d %H:%M:%S.%f"), label=name, ax=ax, y='Wert', marker='x')
        group.plot(x='Zeit', label=name, ax=ax, y='Wert', marker='x')
    plt.legend(bbox_to_anchor=(0.5, 1.15), loc="upper center", ncol=4)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
    filename = "{date}-{start}_diff{diff}K{msg}".format(
        date=start.strftime("%Y%m%d"),
        start=start.strftime("%H%M"),
        diff="%.2f" % diff,
        msg=msg if msg else ""
    )
    try:
        plt.savefig("/home/pi/rpi/heizmanager/static/testlaeufe/%s.png" % filename)
    except ValueError:
        pass
    plt.close()

    with open("/home/pi/rpi/heizmanager/static/testlaeufe/%s.json" % filename, "w") as jsonfile:
        json.dump(run_data, jsonfile)

    hparams['aha']['is_active'] = False
    hparams['aha']['start'] = None
    haus.set_module_parameters(hparams)


def _calc_differences(df):
    # nach sensoren gruppieren und dann jeweils die differenz zwischen aufeinanderfolgenden sensorwerten in den dataframe einfuegen
    df.sort_values(by=["Sensorname", "Zeit"])
    df['wert_diff'] = df.groupby("Sensorname")['Wert'].diff().fillna(0)
    return df


def _cnt_diffs(frame):
    # schauen, ob der sensorwert in diesem frame mehr steigt oder faellt
    if (len(frame) >= 2 and frame.iloc[-1].Wert > frame.iloc[0].Wert and len(frame.loc[frame.wert_diff >= 0]) >= len(frame.loc[frame.wert_diff < 0])) or (len(frame.loc[frame.wert_diff >= 0]) >= len(frame.loc[frame.wert_diff < 0]) * 1.5) and sum(frame.loc[frame.wert_diff > 0].wert_diff) > 0 and math.fabs(sum(frame.loc[frame.wert_diff > 0].wert_diff)) > math.fabs(sum(frame.loc[frame.wert_diff < 0].wert_diff)):
        return True
    else:
        return False


def _convert_logs_to_df(logs, convert_ids_to_names=False):
    df = pd.DataFrame(columns=["Sensorname", "Zeit", "Wert"])
    for sensorid, sensorlogs in logs.items():
        if convert_ids_to_names:
            sensor = AbstractSensor.get_sensor(sensorid)
            if sensor:
                sensorid = sensor.description or sensor.name
        df = df.append(pd.DataFrame(data=[(log[0], sensorid, log[1]) for log in sensorlogs], columns=["Zeit", "Sensorname", "Wert"]), ignore_index=True)
    df['Zeit'] = pd.to_datetime(df['Zeit'], unit='ms')
    return df


def _get_logs_as_df(gateways, start):
    logs = _get_logs(gateways, start)
    df = pd.DataFrame(columns=["Sensorname", "Zeit", "Wert"])
    for sensorid, sensorlogs in logs.items():
        df = df.append(pd.DataFrame(data=[(log[0], sensorid, log[1]) for log in sensorlogs], columns=["Zeit", "Sensorname", "Wert"]), ignore_index=True)
    df['Zeit'] = pd.to_datetime(df['Zeit'], unit='ms')
    return df


def _get_logs(gateways, start):
    berlin = pytz.timezone("Europe/Berlin")
    start = (datetime.strptime(start, "%Y-%m-%d %H:%M") + timedelta(minutes=30)).strftime("%Y-%m-%d %H:%M")
    ret = {}
    for gateway in gateways:
        params = gateway.get_parameters().get('ahaparams', {})
        if params.get('is_active'):
            sensors = params['sensors']
            for sensor in sensors:
                try:
                    ret[sensor] = SimpleSensorLog.objects.getlogs(sensor, start.replace(" ", "T"), datetime.now(berlin).strftime("%Y-%m-%dT%H:%M"))
                except Exception as e:
                    logging.exception("exc getting sensordata %s" % sensor)
                    ret[sensor] = []
    return ret
