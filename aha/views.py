# -*- coding: utf-8 -*-

from heizmanager.render import render_response, render_redirect
from heizmanager.models import Gateway, GatewayAusgang, AbstractSensor
from fabric.api import local
import logging
from datetime import datetime
import pytz
from tasks import _get_current_testrun, _save_testrun
import json
from django.core.serializers.json import DjangoJSONEncoder
try:
    import pandas as pd
except ImportError:
    pd = None


def get_name():
    return "Automatischer hydraulischer Abgleich"


def get_global_settings_link(request, haus):
    return "<a href='/m_setup/%s/aha/'>Automatischer hydraulischer Abgleich</a>" % haus.id


def get_global_description_link():
    desc = u"Automatischer hydraulischer Abgleich für Pro Gateways."
    desc_link = ""
    return desc, desc_link


def get_global_settings_page(request, haus):

    gateways = Gateway.objects.filter(version__startswith="5.")
    ret = {'haus': haus}

    if request.method == "GET":

        if 'deletetr' in request.GET:

            filename = request.GET['deletetr']
            try:
                local("rm heizmanager/static/testlaeufe/%s.*" % filename)
            except:
                pass

            return render_redirect(request, "/m_setup/%s/aha/" % haus.id)

        else:

            if not len(gateways):
                ret['err'] = u'Kein Pro Gateway vorhanden, automatischer hydraulischer Abgleich nicht möglich.'
                return render_response(request, "m_settings_aha.html", ret)
            if pd is None:
                ret['err'] = u'Update nicht vollständig ausgeführt, automatischer hydraulischer Abgleich nicht möglich.'
                return render_response(request, "m_settings_aha.html", ret)

            ausgaenge = GatewayAusgang.objects.filter(gateway__in=gateways, regelung__regelung="ruecklaufregelung")
            ret['ausgaenge'] = {}
            ret['is_active'] = haus.get_module_parameters().get('aha', {}).get('is_active', False)
            for gateway in gateways:
                if gateway.is_heizraumgw():
                    continue
                ahaparams = gateway.get_parameters().get('ahaparams', {})
                gwausgaenge = ausgaenge.filter(gateway=gateway)
                outs = []
                for gwausgang in gwausgaenge:
                    regparams = gwausgang.regelung.get_parameters()
                    outs += regparams.get('rlsensorssn', dict).values()
                ret['ausgaenge'][gateway.id] = {
                    'name': gateway.description or gateway.name,
                    'outs': sorted(list(set([o for _outs in outs for o in _outs]))),
                    'params': ahaparams
                }

            if ret['is_active']:
                ret['start'] = haus.get_module_parameters()['aha']['start']
                try:
                    auswertung, fenster, logs = _get_current_testrun(haus.id)
                except Exception:
                    logging.exception("exc")
                    ret['current_logs'] = []
                    ret['auswertung'] = []
                else:
                    _logs = {}
                    for sensorid, slogs in logs.items():
                        sensor = AbstractSensor.get_sensor(sensorid)
                        # sensor = sensorid  # TODO NICHT COMMITTEN
                        if sensor is not None:
                            try:
                                _logs[sensor.description or sensor.name] = slogs
                            except Exception as e:
                                logging.exception("_logs")
                                _logs[sensorid.replace('*', ' ')] = slogs
                    ret['current_logs'] = json.dumps(_logs, cls=DjangoJSONEncoder)

                    _auswertung = []
                    for a in auswertung.items():
                        sens = AbstractSensor.get_sensor(a[0])
                        if sens:
                            _auswertung.append((sens.description or sens.name, a[1]))
                        # else:  # TODO NICHT COMMITTEN!
                        #     _auswertung.append((a[0], a[1]))
                    ret['auswertung'] = sorted(_auswertung, key=lambda x: x[1])

                    if fenster:
                        ret['diff'] = ret['auswertung'][-1][1] - ret['auswertung'][0][1]

            testlaeufe = local("ls -r heizmanager/static/testlaeufe", capture=True)
            if len(testlaeufe):
                ret['testlaeufe'] = []
                for tl in testlaeufe.split('\n'):
                    filename, ending = tl.rsplit('.', 1)[0], tl.rsplit('.', 1)[1]
                    ts = "%s" % datetime.strptime(filename[:13], "%Y%m%d-%H%M").strftime("%Y-%m-%d %H:%M")
                    data = {'filename': filename, 'ts': ts}

                    if ending == "png":
                        data['diff'] = float(filename.split('_')[1][4:-1])
                        data['msg'] = None if len(filename.split('_')) == 2 else filename.split('_')[2]
                        ret['testlaeufe'].append((tl.rsplit('.', 1)[0], data))
                    elif ending == "json":
                        with open("heizmanager/static/testlaeufe/%s" % tl, "r") as f:
                            try:
                                data.update(json.load(f))
                                data['has_json'] = True
                                oc = data['opening_changes']
                                for gw, outs in oc.items():
                                    data['opening_changes'][gw] = sorted([(k, v) for k, v in outs.items()], key=lambda x: int(x[0]))
                            except:
                                pass
                            if len(ret['testlaeufe']) and ret['testlaeufe'][-1][0] == tl.rsplit('.', 1)[0]:  # png schon in ret, ueberschreiben
                                ret['testlaeufe'][-1] = (tl.rsplit('.', 1)[0], data)
                            else:
                                ret['testlaeufe'].append((tl.rsplit('.', 1)[0], data))

                    if ret['testlaeufe'][-1][1]['msg'] is not None and (ret['testlaeufe'][-1][1]['msg'].endswith(('abbruch-rltemp-zu-hoch', 'mangelhafte-daten')) or (ret['testlaeufe'][-1][1]['msg'].endswith('manuell-beendet') and ret['testlaeufe'][-1][1].get('diff') is None)):
                        ret['testlaeufe'][-1][1]['success'] = False
                        ret['testlaeufe'][-1][1]['show_diff'] = False
                    elif ret['testlaeufe'][-1][1].get('diff', 3) > 2:
                        ret['testlaeufe'][-1][1]['success'] = False
                        ret['testlaeufe'][-1][1]['show_diff'] = True
                    else:
                        ret['testlaeufe'][-1][1]['success'] = True
                        ret['testlaeufe'][-1][1]['show_diff'] = True
                    if ret['testlaeufe'][-1][1]['msg'] is not None:
                        ret['testlaeufe'][-1][1]['msg'] = ret['testlaeufe'][-1][1]['msg'].replace('_', ' ').replace('-', ' ')
                    logging.warning(ret['testlaeufe'][-1])
            else:
                ret['testlaeufe'] = None

            return render_response(request, "m_settings_aha.html", ret)

    elif request.method == "POST":

        if request.POST.get('is_active') == "true":
            # war active, testlauf beenden

            try:
                auswertung, fenster, logs = _get_current_testrun(haus.id)
                _save_testrun(haus, logs, auswertung, msg="_manuell-beendet")
            except Exception as e:
                logging.exception("exc saving testrun")
                for gateway in Gateway.objects.filter(haus=haus):
                    gwparams = gateway.get_parameters()
                    gwparams.setdefault('ahaparams', {})
                    gwparams['ahaparams']['is_active'] = False
                    gwparams['ahaparams']['outs'] = []
                    gwparams['ahaparams']['sensors'] = []
                    gateway.set_parameters(gwparams)
                    hparams = haus.get_module_parameters()
                    hparams['aha']['is_active'] = False
                    hparams['aha']['start'] = None
                    haus.set_module_parameters(hparams)

        elif request.POST.get('is_active') == "false":
            # war inactive, testlauf starten

            gatewayausgaenge = GatewayAusgang.objects.filter(gateway__in=gateways)
            relevant_rlsensors = dict((gw.id, []) for gw in gateways)
            gw2outs = dict((gw.id, []) for gw in gateways)
            for k, v in request.POST.items():
                if k.startswith("out_"):
                    gwid = k.split('_')[1]
                    out = k.split('_')[2]

                    gateway = gateways.get(pk=int(gwid))
                    gw2outs[gateway.id].append(int(out))
                    ausgaenge = gatewayausgaenge.filter(gateway=gateway)
                    for ausgang in ausgaenge:
                        if out in ausgang.ausgang.split(', '):
                            regelung = ausgang.get_regelung()
                            regparams = regelung.get_parameters()
                            for sensorid, outs in regparams['rlsensorssn'].items():
                                if int(out) in outs:
                                    relevant_rlsensors[gateway.id].append(sensorid)

            if not any(relevant_rlsensors.values()):
                # wenn keine ausgaenge ausgewaehlt wurden, einfach zurueck
                return render_redirect(request, "/m_setup/%s/aha/" % haus.id)

            berlin = pytz.timezone("Europe/Berlin")
            hparams = haus.get_module_parameters()
            hparams.setdefault('aha', {})
            hparams['aha']['is_active'] = True
            hparams['aha']['start'] = datetime.now(berlin).strftime("%Y-%m-%d %H:%M")
            haus.set_module_parameters(hparams)

            for gwid, ids2names in relevant_rlsensors.items():
                gateway = gateways.get(pk=gwid)
                gwparams = gateway.get_parameters()
                params = {
                    'is_active': True,
                    'outs': gw2outs[gateway.id],
                    'sensors': relevant_rlsensors[gateway.id],
                    'start': datetime.now(berlin).strftime("%Y-%m-%d %H:%M")
                }
                gwparams['ahaparams'] = params
                gateway.set_parameters(gwparams)

        else:
            pass

        return render_redirect(request, "/m_setup/%s/aha/" % haus.id)